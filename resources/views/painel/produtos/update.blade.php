@extends("painel.templates.app")
@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Alterar Produtos
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{getenv("PAINEL")}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{getenv("PAINEL")}}/produtos"> Páginas</a></li>
        <li class="active">Alterar</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="with-border"></div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="{{getenv("PAINEL")}}/produtos/update" method="post">
                    <div class="box-body">
                        <div class="col-sm-12">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="id" value="{{$item->ProCodigo}}">


                            <div class="form-group">
                                <label for="titulo">Título</label>
                                <input type="text" class="form-control" id="titulo" name="titulo" placeholder="Título"
                                       value="{{$item->ProTitulo}}"
                                       required maxlength="150">
                            </div>
                            <div class="form-group">
                                <label for="valor">Valor Oferta</label>
                                <input type="text" class="form-control" id="valor" name="valor" placeholder="Valor"
                                       value="{{$item->ProValor}}">
                            </div>
                            <div class="form-group">
                                <label for="departamento">Departamento</label>
                                <select class="form-control" id="departamento" name="departamento" target-id="#categoria" required>
                                    @if(@$departamentos !== null)
                                        @foreach($departamentos as $departamento)
                                            <option value="{{$departamento->DepCodigo}}" {{($item->DepCodigo == $departamento->DepCodigo)?"selected":""}}>{{$departamento->DepTitulo}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="categoria">Categoria</label>
                                <select class="form-control" id="categoria" name="categoria" required>
                                    @if(@$categorias !== null)
                                        @foreach(@$categorias as $categoria)
                                            <option value="{{$categoria->CatCodigo}}" {{($item->CatCodigo == $categoria->CatCodigo)?"selected":""}}>{{$categoria->CatTitulo}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="departamento">Marca</label>
                                <select class="form-control" id="marca" name="marca" required>
                                    @if(@$marcas !== null)
                                        @foreach($marcas as $marca)
                                            <option value="{{$marca->MarCodigo}}" {{($item->ProMarCodigo == $marca->MarCodigo)?"selected":""}}>{{$marca->MarTitulo}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="conteudo">Conteúdo</label>
                                <textarea class="form-control editor" id="conteudo" name="conteudo"
                                          placeholder="Conteúdo" rows="10">{{$item->ProConteudo}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="liberado">Liberado</label>
                                <br>

                                <div class="radio-inline">
                                    <label><input type="radio" name="liberado" id="liberado"
                                                  value="1" {{($item->ProLiberado == 1)?"checked":""}}>Sim</label>
                                </div>
                                <div class="radio-inline">
                                    <label><input type="radio" name="liberado" id="liberado"
                                                  value="0" {{($item->ProLiberado == 0)?"checked":""}}>Não</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Salvar</button>
                        </div>
                    </div>
                </form>

            </div>
            <!-- /.box -->
        </div>
    </div>
    <!-- /.row -->
</section><!-- /.content -->

@endsection