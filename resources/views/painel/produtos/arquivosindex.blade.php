@extends("painel.templates.app")
@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Editar Arquivos dos Produtos
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{getenv("PAINEL")}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{getenv("PAINEL")}}/produtos"> Arquivos dos Produtos</a></li>
        <li class="active">Editar</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <a href="{{getenv("PAINEL")}}/produtos/arquivos/{{$id}}/create"
               class="btn btn-primary btn-lg margin-bottom"><i
                        class="fa fa-plus"></i> Adicionar</a>
        </div>
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="with-border"></div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-sm-12">
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                        @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif
                    </div>
                    <div class="col-sm-12 table-responsive" style="min-height: 300px;">


                        <table class="table table-bordered table-hover" style="width: 100%">
                            <thead>
                            <tr>
                                <th class="col-sm-1 text-center">#</th>
                                <th class="col-sm-1 text-center">#</th>
                                <th class="col-sm-1 text-center">Codigo</th>
                                <th class="col-sm-1 text-center">Arquivo</th>
                                <th class="col-sm-7">Titulo</th>
                                <th class="col-sm-1 text-center">#</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($itens as $item)
                                <tr>
                                    <td class="text-center" style="vertical-align: middle">
                                        <a href="{{getenv("PAINEL")}}/produtos/arquivos/{{$item->ArqProCodigo}}/update/{{$item->ArqCodigo}}"
                                           class="btn btn-primary btn-sm">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </td>
                                    <td class="text-center" style="vertical-align: middle">
                                        <a href="#"
                                           class="btn btn-primary btn-sm btn-upload-modal"
                                           data-href="{{getenv("PAINEL")}}/produtos/arquivos/{{$item->ArqProCodigo}}/upload/{{$item->ArqCodigo}}">
                                            <i class="fa fa-cloud-upload"></i>
                                        </a>
                                    </td>
                                    <td class="text-center" style="vertical-align: middle">{{$item->ArqCodigo}}</td>
                                    <td class="text-center" style="vertical-align: middle"><a
                                                href="{{url("upload/produtosarquivos/".$item->ArqArquivo)}}"
                                                target="_blank">{{$item->ArqArquivo}}</a></td>
                                    <td style="vertical-align: middle">{{$item->ArqTitulo}}</td>
                                    <td class="text-center" style="vertical-align: middle">
                                        <a href="{{getenv("PAINEL")}}/produtos/arquivos/{{$item->ArqProCodigo}}/destroy/{{$item->ArqCodigo}}"
                                           class="btn btn-danger btn-sm btn-destroy"><i class="fa fa-close"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            @if($itens->count() < 1)
                                <tr>
                                    <td class="text-center" colspan="6">
                                        Nenhum Registro Encontrado
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>

                    </div>
                    <div class="col-sm-12 text-center">
                        {!! $itens->render() !!}
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                </div>

            </div>
            <!-- /.box -->
        </div>
    </div>
    <!-- /.row -->
</section><!-- /.content -->

@endsection