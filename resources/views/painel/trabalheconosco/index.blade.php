@extends("painel.templates.app")
@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Trabalhe Conosco
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{getenv("PAINEL")}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{getenv("PAINEL")}}/trabalheconosco"> Trabalhe Conosco</a></li>
        <li class="active">Visualizar</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="with-border"></div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-sm-4 col-sm-offset-8">
                        <form action="{{getenv("PAINEL")}}/trabalheconosco" method="POST">
                            <div class="input-group">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="text" name="busca" id="busca" class="form-control"
                                       placeholder="Buscar por..." value="{{$busca}}">
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default" type="button">Buscar</button>
                                </div>
                            </div><!-- /input-group -->
                            <div class="clearfix">&nbsp;</div>
                        </form>
                    </div>
                    <div class="col-sm-12">
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                        @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif
                    </div>
                    <div class="col-sm-12 table-responsive" style="min-height: 300px;">


                        <table class="table table-bordered table-hover" style="width: 100%">
                            <thead>
                            <tr>
                                <th class="col-sm-1 text-center">#</th>
                                <th class="col-sm-1 text-center">Data</th>
                                <th class="col-xs-5">Nome</th>
                                <th class="col-xs-4">Vaga</th>
                                <th class="col-sm-1 text-center">#</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($itens as $item)
                                <tr>
                                    <td class="text-center" style="vertical-align: middle">
                                        @if(@$item->id > 0)
                                            <a href="{{getenv("PAINEL")}}/vagas/{{$item->vaga_id}}/candidatos/{{$item->id}}/view"
                                               class="btn btn-primary btn-sm">
                                                <i class="fa fa-commenting"></i>
                                            </a>
                                        @else
                                            <a href="{{getenv("PAINEL")}}/trabalheconosco/view/{{$item->ConCodigo}}"
                                               class="btn btn-primary btn-sm">
                                                <i class="fa fa-commenting"></i>
                                            </a>
                                        @endif
                                    </td>
                                    <td style="vertical-align: middle;" align="center">{{date("d/m/Y H:i:s", strtotime($item->created_at))}}</td>
                                    <td style="vertical-align: middle">{{$item->ConNome}}{{$item->nome}}</td>
                                    <td style="vertical-align: middle">{{((@$item->vaga_id) > 0) ? @$item->vaga->titulo:"Sem especificação de vaga"}}</td>
                                    <td class="text-center" style="vertical-align: middle">
                                        @if(@$item->id > 0)
                                            <a href="{{getenv("PAINEL")}}/candidatos/{{$item->id}}/destroy"
                                               class="btn btn-danger btn-sm btn-destroy"><i class="fa fa-close"></i></a>
                                        @else
                                            <a href="{{getenv("PAINEL")}}/trabalheconosco/destroy/{{$item->ConCodigo}}"
                                               class="btn btn-danger btn-sm btn-destroy"><i class="fa fa-close"></i></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            @if($itens->count() < 1)
                                <tr>
                                    <td class="text-center" colspan="8">
                                        Nenhum Registro Encontrado
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="col-sm-12 text-center">
                        {!! $itens->appends(Input::all())->render() !!}
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                </div>

            </div>
            <!-- /.box -->
        </div>
    </div>
    <!-- /.row -->
</section><!-- /.content -->

@endsection