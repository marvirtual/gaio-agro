@extends("painel.templates.app")
@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Alterar Unidades
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{getenv("PAINEL")}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{getenv("PAINEL")}}/unidades"> Unidades</a></li>
        <li class="active">Alterar</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="with-border"></div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="{{getenv("PAINEL")}}/unidades/update" method="post">
                    <div class="box-body">
                        <div class="col-sm-12">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="id" value="{{$item->UniCodigo}}">

                            <div class="form-group">
                                <label for="titulo">Município</label>
                                <input type="text" class="form-control" id="municipio" name="municipio" placeholder="Município"
                                       value="{{$item->UniMunicipio}}" maxlength="50" required>
                            </div>
                            <div class="form-group">
                                <label for="titulo">Endereço</label>
                                <input type="text" class="form-control" id="endereco" name="endereco" placeholder="Endereço"
                                       value="{{$item->UniEndereco}}" required>
                            </div>
                            <div class="form-group">
                                <label for="titulo">Telefone</label>
                                <input type="text" class="form-control maskFone" id="telefone" name="telefone" placeholder="Telefone"
                                       value="{{$item->UniTelefone}}" maxlength="30" required>
                            </div>
                            <div class="form-group">
                                <label for="mapa">Mapa</label>
                                <input type="text" class="form-control" id="mapa" name="mapa"
                                       placeholder="Mapa" value="{{$item->UniMapa}}">
                            </div>
                            {{-- <div class="form-group">
                                <label for="titulo">Fábrica de Farelo</label>
                                <input type="text" class="form-control" id="fabrica" name="fabrica" placeholder="Fábrica de Farelo de Soja"
                                       value="{{$item->UniFabrica}}">
                            </div> --}}
                            <div class="form-group">
                                <label for="titulo">Gaio Solar</label>
                                <input type="text" class="form-control" id="gaiosolar" name="gaiosolar" placeholder="Gaio Solar"
                                       value="{{$item->UniGaioSolar}}">
                            </div>
                            <div class="form-group">
                                <label for="legenda">Tipo</label>
                                <br>

                                <div class="radio-inline">
                                    <label><input type="radio" name="tipo" id="tipo"
                                                  value="1" {{($item->UniTipo == 1)?"checked":""}}>Loja</label>
                                </div>
                                <div class="radio-inline">
                                    <label><input type="radio" name="tipo" id="tipo"
                                                  value="0" {{($item->UniTipo == 0)?"checked":""}}>Cerealista</label>
                                </div>
                                <div class="radio-inline">
                                    <label><input type="radio" name="tipo" id="tipo" 
                                                  value="2" {{($item->UniTipo == 2)?"checked":""}}>Gaio Solar</label>
                                </div>
                                <div class="radio-inline">
                                    <label><input type="radio" name="tipo" id="tipo"
                                                  value="3" {{($item->UniTipo == 3)?"checked":""}}>Fábrica de Farelo de Soja</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Salvar</button>
                        </div>
                    </div>
                </form>

            </div>
            <!-- /.box -->
        </div>
    </div>
    <!-- /.row -->
</section><!-- /.content -->

@endsection