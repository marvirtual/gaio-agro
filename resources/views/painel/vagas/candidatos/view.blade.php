@extends("painel.templates.app")
@section('content')
<section class="content-header">
    <h1>
        Candidatura
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{getenv("PAINEL")}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{getenv("PAINEL")}}/vagas-de-emprego"> Vagas de Emprego</a></li>
        <li class="active">Visualizar</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="with-border"></div>
                <div class="box-body">
                    <div class="col-sm-12">
                        {!! $item->conteudo !!}
                        <b>Download do currículo: </b>


                        @if(file_exists(public_path().'/upload/vagas/curriculo_' . $item->id .'.pdf'))
                            <a href="{{url('/upload/vagas/curriculo_' . $item->id .'.pdf')}}" target="_blank"
                               download
                               class="btn btn-light">
                                <i class="fa fa-download"></i>
                            </a>
                        @else
                            Currículo não enviado.
                        @endif

                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="col-sm-12">
                            <a class="btn btn-primary" href="{{@$retorno}}" ><< Voltar</a>
                    </div>
                </div>

            </div>
            <!-- /.box -->
        </div>
    </div>
    <!-- /.row -->
</section><!-- /.content -->

@endsection