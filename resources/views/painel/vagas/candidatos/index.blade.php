@extends("painel.templates.app")
@section('content')
<section class="content-header">
    <h1>
        Candidaturas
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{getenv("PAINEL")}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{getenv("PAINEL")}}/vagas-de-emprego"> Vagas de Emprego</a></li>
        <li class="active">Visualizar</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="with-border"></div>
                <div class="box-body">
                    <div class="col-sm-8">
                        <p style="font-size: 28px; color: #000;">
                            {{$vaga->titulo}}
                        </p>
                        <p style="font-size: 18px; color: #000;">
                            Publicado em: {{date('d/m/Y', strtotime($vaga->created_at))}}
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <form action="{{getenv("PAINEL")}}/vagas/{{$vaga->id}}/candidatos" method="POST">
                            <div class="input-group">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="text" name="busca" id="busca" class="form-control"
                                       placeholder="Buscar por..." value="">
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default" type="button">Buscar</button>
                                </div>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                        </form>
                    </div>
                    <div class="col-sm-12">
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                        @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif
                    </div>
                    <div class="col-sm-12 table-responsive" style="min-height: 300px;">
                        <table class="table table-bordered table-hover" style="width: 100%">
                            <thead>
                            <tr>
                                <th class="col-sm-1 text-center">#</th>
                                <th class="col-sm-1 text-center">Data</th>
                                <th class="col-xs-2">Nome</th>
                                <th class="col-xs-2">Celular</th>
                                <th class="col-xs-3" colspan="2">Data Entrevista</th>
                                <th class="col-xs-3" colspan="2">Data Contratação</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count(@$itens) > 0)
                                @foreach($itens as $key => $item)
                                    <tr>
                                        <td class="text-center" style="vertical-align: middle">
                                            <a href="{{getenv("PAINEL")}}/vagas/{{$vaga->id}}/candidatos/{{$item->id}}"
                                               class="btn btn-primary btn-sm">
                                                <i class="fa fa-commenting"></i>
                                            </a>
                                        </td>
                                        <td class="text-center" style="vertical-align: middle">
                                            {{date('d/m/Y H:i:s', strtotime($item->created_at))}}
                                        </td>
                                        <td style="vertical-align: middle">{{$item->nome}}</td>
                                        <td style="vertical-align: middle">{{$item->celular}}</td>
                                        <td style="vertical-align: middle">
                                            <button data-toggle="modal" data-target="#modalentrevista{{$key}}"
                                               class="btn btn-primary btn-sm" style="margin-right: 15px;">
                                                <i class="fa fa-edit"></i>
                                            </button>

                                            @if(strlen($item->dataentrevista) > 0)
                                                {{date('d/m/Y H:i:s', strtotime($item->dataentrevista))}}
                                            @endif

                                            <div class="modal fade" id="modalentrevista{{$key}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title" id="myModalLabel">Entrevista</h4>
                                                        </div>
                                                        <form action="{{getenv("PAINEL")}}/vagas/{{$vaga->id}}/candidatos/{{$item->id}}/entrevista" method="post">
                                                            <input type="hidden" name="_token" value="{{csrf_token()}}">

                                                            <div class="modal-body">
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="data">Data</label>
                                                                            <input type="text" class="form-control data-formulario" id="data" name="data" size="10" maxlength="10"
                                                                                   placeholder="Data" value="{{((strlen($item->dataentrevista > 0) ? date('d/m/Y', strtotime($item->dataentrevista)):""))}}" required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="hora">Hora</label>
                                                                            <input type="text" class="form-control hora-formulario" id="hora" name="hora" size="8" maxlength="8"
                                                                                   placeholder="Hora" value="{{((strlen($item->dataentrevista > 0) ? date('H:i:s', strtotime($item->dataentrevista)):""))}}" required>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="modal-footer">
                                                                <button type="submit" class="btn btn-primary">Salvar</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td style="vertical-align: middle"></td>
                                        <td style="vertical-align: middle">
                                            <button data-toggle="modal" data-target="#modalcontratacao{{$key}}"
                                                    class="btn btn-primary btn-sm" style="margin-right: 15px;">
                                                <i class="fa fa-edit"></i>
                                            </button>
                                            @if(strlen($item->datacontratacao) > 0)
                                                {{date('d/m/Y H:i:s', strtotime($item->datacontratacao))}}
                                            @endif

                                            <div class="modal fade" id="modalcontratacao{{$key}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title" id="myModalLabel">Contratação</h4>
                                                        </div>
                                                        <form action="{{getenv("PAINEL")}}/vagas/{{$vaga->id}}/candidatos/{{$item->id}}/contratacao" method="post">
                                                            <input type="hidden" name="_token" value="{{csrf_token()}}">

                                                            <div class="modal-body">
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="data">Data</label>
                                                                            <input type="text" class="form-control data-formulario" id="data" name="data" size="10" maxlength="10"
                                                                                   placeholder="Data" value="{{((strlen($item->datacontratacao > 0) ? date('d/m/Y', strtotime($item->datacontratacao)):""))}}" required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="hora">Hora</label>
                                                                            <input type="text" class="form-control hora-formulario" id="hora" name="hora" size="8" maxlength="8"
                                                                                   placeholder="Hora" value="{{((strlen($item->datacontratacao > 0) ? date('H:i:s', strtotime($item->datacontratacao)):""))}}" required>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="modal-footer">
                                                                <button type="submit" class="btn btn-primary">Salvar</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td style="vertical-align: middle"></td>
                                    </tr>
                                @endforeach
                            @endif
                            @if($itens->count() < 1)
                                <tr>
                                    <td class="text-center" colspan="8">
                                        Nenhum Registro Encontrado
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="col-sm-12 text-center">
                        {!! $itens->render() !!}
                    </div>
                </div>
                <div class="box-footer">
                </div>
            </div>
        </div>
    </div>
</section>
@endsection