@extends("painel.templates.app")
@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Alterar Vaga de Emprego
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{getenv("PAINEL")}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{getenv("PAINEL")}}/vagas"> Vagas de Emprego</a></li>
        <li class="active">Alterar</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="with-border"></div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="{{getenv("PAINEL")}}/vagas/update" method="post">
                    <div class="box-body">
                        <div class="col-sm-12">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="id" value="{{$item->id}}">

                            <div class="form-group">
                                <label for="titulo">Título da Vaga</label>
                                <input type="text" class="form-control" id="titulo" name="titulo" placeholder="Título"
                                       required maxlength="150" value="{{$item->titulo}}">
                            </div>
                            <div class="form-group">
                                <label for="atribuicoes">Atribuições e Responsabilidades</label>
                                <textarea class="form-control editor" name="atribuicoes" id="atribuicoes" placeholder="Atribuições e Responsabilidades"
                                          rows="10">{{$item->atribuicoes}}</textarea>
                            </div>

                            <div class="form-group">
                                <label for="requisitos">Requisitos</label>
                                <textarea class="form-control editor" name="requisitos" id="requisitos" placeholder="Requisitos"
                                          rows="10">{{$item->requisitos}}</textarea>
                            </div>

                            <div class="form-group">
                                <label for="cidade">Cidade</label>
                                <select class="form-control" name="unidade" id="unidade">
                                    @foreach($unidades as $unidade)
                                        <option value="{{$unidade->UniMunicipio}}" {{(($unidade->UniCodigo == $item->unidade) ? "selected":"")}}>{{$unidade->UniMunicipio}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="legenda">Liberado</label>
                                <br>

                                <div class="radio-inline">
                                    <label><input type="radio" name="liberado" id="liberado"
                                                  value="1" {{($item->liberado == 1)?"checked":""}}>Sim</label>
                                </div>
                                <div class="radio-inline">
                                    <label><input type="radio" name="liberado" id="liberado"
                                                  value="0" {{($item->liberado == 0)?"checked":""}}>Não</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Salvar</button>
                        </div>
                    </div>
                </form>

            </div>
            <!-- /.box -->
        </div>
    </div>
    <!-- /.row -->
</section><!-- /.content -->

@endsection