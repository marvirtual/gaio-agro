@extends("painel.templates.app")
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{($orcamento->OrcEnviado == 1) ? "ORÇAMENTO ENVIADO" : "Orçamento Pendente"}}
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{getenv("PAINEL")}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{getenv("PAINEL")}}/orcamentos"> Orçamentos</a></li>
            <li class="active">Visualizar</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="with-border"></div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" action="{{getenv("PAINEL")}}/orcamentos/enviar" method="post">
                        <div class="box-body">
                            <div class="col-md-4" style="font-size: 17px;">
                                <strong>Nome:</strong> {{$orcamento->OrcNome}}<br>
                                <strong>Município:</strong> {{$orcamento->OrcMunicipio}}<br>
                                <strong>Estado:</strong> {{$orcamento->OrcEstado}}<br>
                                <strong>Endereço:</strong> {{$orcamento->OrcEndereco}}<br>
                                <strong>Telefone:</strong> {{$orcamento->OrcTelefone}}<br>
                                <strong>Email:</strong> {{$orcamento->OrcEmail}}<br>
                                <strong>Observações</strong><br>
                                <p class="texto-orcamento">{!!$orcamento->OrcObservacoes!!}</p>
                            </div>
                            <div class="col-md-8">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="id" value="{{$orcamento->OrcCodigo}}">

                                <div class="form-group">
                                    <label for="informacoes">Informações</label>
                            <textarea class="form-control editor" name="informacoes" id="informacoes"
                                      placeholder="Informações"
                                      rows="5">{{$orcamento->OrcInformacoes}}</textarea>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <table class="table table-striped table-hover" style="width: 100%">
                                    <thead>
                                    <tr>
                                        <th class="col-sm-1 text-center">#</th>
                                        <th class="col-sm-8">Produto</th>
                                        <th class="col-sm-1 text-center">Qtde</th>
                                        <th class="col-xs-3 col-sm-2 col-lg-1 text-center">Valor Unit.</th>
                                        <th class="col-xs-3 col-sm-2 col-lg-1 text-center">Valor Total</th>
                                        <th class="col-sm-1 text-center">Disp.</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($itens as $item)
                                        <tr>
                                            <td style="vertical-align: middle;"><img
                                                        src="{{asset("/upload/produtos/dest_".$item->ProCodigo.".jpg")}}"
                                                        width="100%"/></td>
                                            <td style="vertical-align: middle;">
                                                @if($item->IteDisponivel == 0)
                                                    {{$item->ProTitulo}}
                                                @else
                                                    <s>{{$item->ProTitulo}}</s>
                                                @endif
                                            </td>
                                            <td class="text-center"
                                                style="vertical-align: middle;">{{$item->IteQtde}}</td>
                                            <td class="text-center"
                                                style="vertical-align: middle;">
                                                @if($item->IteDisponivel == 0)
                                                    <input type="text" class="form-control valoritem"
                                                           width="20" maxlength="8"
                                                           value="{{$item->IteValorUnitario}}"
                                                           name="valoritem" id="valoritem"
                                                           data-id="{{$item->IteCodigo}}">
                                                @else
                                                    {{$item->IteValorUnitario}}
                                                @endif

                                            </td>
                                            <td class="text-center"
                                                style="vertical-align: middle;"><span
                                                        data-id="{{$item->IteCodigo}}">{{$item->IteValorTotal}}</span>
                                            </td>
                                            <td class="text-center" style="vertical-align: middle;">
                                                @if($item->IteDisponivel == 0)
                                                    <a href="{{getenv("PAINEL")}}/orcamentos/destroy-item/{{$item->IteCodigo}}"
                                                       class="btn btn-success btn-sm btn-destroy">Sim</a>
                                                @else
                                                    <a href="{{getenv("PAINEL")}}/orcamentos/destroy-item/{{$item->IteCodigo}}"
                                                       class="btn btn-danger btn-sm btn-destroy">Não</a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2 text-center">
                                    @if(File_exists(public_path("/upload/orcamentos/" . @$orcamento->anexo->ArqArquivo)))
                                        <a class="btn btn-md btn-success" target="_blank" href="{{asset("upload/orcamentos/" . @$orcamento->anexo->ArqArquivo)}}"> <i class="fa fa-download"></i>  Baixar Conta de Luz COPEL  </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <div class="col-sm-12">

                                <button type="submit" class="btn btn-lg btn-primary"><i
                                            class="fa fa-save"></i> ENVIAR ORÇAMENTO</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.row -->
    </section><!-- /.content -->

@endsection