@extends("painel.templates.app")
@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Adicionar Notícias
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{getenv("PAINEL")}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{getenv("PAINEL")}}/noticias"> Notícias</a></li>
        <li class="active">Adicionar</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header"></div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="{{getenv("PAINEL")}}/noticias/create" method="post">
                    <div class="box-body">
                        <div class="col-sm-12">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">

                            <div class="form-group">
                                <label for="data">Data</label>
                                <input type="text" class="form-control" id="data" name="data" size="10" maxlength="10"
                                       placeholder="Data"
                                       data-inputmask="'alias': 'date'"
                                       value="{{date("d/m/Y")}}" required>
                            </div>
                            <div class="form-group">
                                <label for="titulo">Título</label>
                                <input type="text" class="form-control" id="titulo" name="titulo" placeholder="Título"
                                       required maxlength="150">
                            </div>
                            <div class="form-group">
                                <label for="resumo">Resumo</label>
                                <input type="text" class="form-control" id="resumo" name="resumo" placeholder="Resumo" maxlength="200">
                            </div>
                            <div class="form-group">
                                <label for="conteudo">Conteúdo</label>
                            <textarea class="form-control editor" name="conteudo" id="conteudo" placeholder="Conteúdo"
                                      rows="10"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="legenda">Legenda Foto</label>
                                <input type="text" class="form-control" id="legenda" name="legenda"
                                       placeholder="Legenda" maxlength="200">
                            </div>
                            <div class="form-group">
                                <label for="fonte">Fonte</label>
                                <input type="text" class="form-control" id="fonte" name="fonte" placeholder="Fonte" maxlength="100">
                            </div>
                            <div class="form-group">
                                <label for="video">Vídeo (YouTube: Inserir somente o código do vídeo. Ex.: http://youtu.be/LbYgFi_lbic inserir apenas LbYgFi_lbic)</label>
                                <input type="text" class="form-control" id="video" name="video" placeholder="Vídeo">
                            </div>
                            <div class="form-group">
                                <label for="legenda">Liberado</label>
                                <br>

                                <div class="radio-inline">
                                    <label><input type="radio" name="liberado" id="liberado" value="1"
                                                  checked>Sim</label>
                                </div>
                                <div class="radio-inline">
                                    <label><input type="radio" name="liberado" id="liberado" value="0">Não</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Salvar</button>
                        </div>
                    </div>
                </form>

            </div>
            <!-- /.box -->
        </div>
    </div>
    <!-- /.row -->
</section><!-- /.content -->

@endsection