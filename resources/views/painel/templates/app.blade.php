<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>VIRTUAL CONTROL 5.0</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{asset("bootstrap/css/bootstrap.min.css")}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset("plugins/datatables/dataTables.bootstrap.css")}}">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{asset("plugins/iCheck/all.css")}}">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="{{asset("plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css")}}">
    <!-- Morris charts -->
    <link rel="stylesheet" href="{{asset("plugins/morris/morris.css")}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset("dist/css/AdminLTE.min.css")}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{asset("dist/css/skins/_all-skins.min.css")}}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-yellow sidebar-mini">
<div class="wrapper">

    @include("painel.includes.header")
            <!-- Left side column. contains the logo and sidebar -->
    @include("painel.includes.sidebar")

            <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        @yield('content')
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer no-print">
        <div class="pull-right hidden-xs">
            <b>Versão</b> 5.0.0
        </div>
        <strong>Copyright &copy; 2016-{{date("Y")}} <a href="http://marvirtual.com.br" target="_blank">Grupo Mar Virtual
                Ltda</a>.</strong> Todos os Direitos Reservados.
    </footer>
    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-light">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">

        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- Home tab content -->
            <div class="tab-pane" id="control-sidebar-home-tab">


            </div>
            <!-- /.tab-pane -->
            <!-- Stats tab content -->
            <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
            <!-- /.tab-pane -->

        </div>
    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- Modal Upload -->
<div class="modal" id="uploadModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row sec-primary">
                    <div class="col-sm-12 text-center" style="background-color: #F1F1F1;padding:15px;">
                        <form id="uploadModalForm" action="" method="post" enctype="multipart/form-data">
                            <div class="form-group text-center">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">

                                    <h2>Arquivo</h2>
                                    <input type="file" id="file" name="file" placeholder="Arquivo"
                                           required>
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <div class="col-sm-8 col-sm-offset-2" style="padding:20px;">
                                    <button type="submit" class="btn btn-primary">Upload</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="row sec-secondary">
                    <div class="col-sm-12 text-center">
                        <i class="fa fa-spinner fa-pulse fa-3x"></i><br>
                        <h3>Aguarde Carregando Arquivo</h3>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.Modal Upload -->
<!-- Modal Upload Multiplo-->
<div class="modal" id="uploadMultiploModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row sec-primary">
                    <div class="col-sm-12 text-center" style="background-color: #F1F1F1;padding:15px;">
                        <form id="uploadModalForm" action="" method="post" enctype="multipart/form-data">
                            <div class="form-group text-center">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">

                                    <h2>Arquivos</h2>
                                    <input type="file" id="files" name="files[]" placeholder="Arquivo"
                                           multiple required>
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <div class="col-sm-8 col-sm-offset-2" style="padding:20px;">
                                    <button type="submit" class="btn btn-primary">Upload</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row sec-secondary">
                    <div class="col-sm-12 text-center">
                        <i class="fa fa-spinner fa-pulse fa-3x"></i><br>
                        <h3>Aguarde Carregando Arquivos</h3>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.Modal Upload Multiplo-->

<!-- jQuery 2.1.4 -->
<script src="{{asset("plugins/jQuery/jQuery-2.1.4.min.js")}}"></script>
<!-- AJAX -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="{{asset("bootstrap/js/bootstrap.min.js")}}"></script>
<!-- DataTables -->
<script src="{{asset("plugins/datatables/jquery.dataTables.min.js")}}"></script>
<script src="{{asset("plugins/datatables/dataTables.bootstrap.min.js")}}"></script>
<!-- SlimScroll -->
<script src="{{asset("plugins/slimScroll/jquery.slimscroll.min.js")}}"></script>
<!-- FastClick -->
<script src="{{asset("plugins/fastclick/fastclick.min.js")}}"></script>
<!-- Input Mask -->
<script src="{{asset("plugins/input-mask/jquery.inputmask.js")}}"></script>
<script src="{{asset("plugins/input-mask/jquery.inputmask.extensions.js")}}"></script>
<script src="{{asset("plugins/input-mask/jquery.inputmask.date.extensions.js")}}"></script>
<script src="{{asset("plugins/input-mask/jquery.maskmoney.js")}}"></script>

<!-- iCheck 1.0.1 -->
<script src="{{asset("plugins/iCheck/icheck.min.js")}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{asset("plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js")}}"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="{{asset("plugins/morris/morris.min.js")}}"></script>
<!-- AdminLTE App -->
<script src="{{asset("dist/js/app.min.js")}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset("dist/js/demo.js")}}"></script>
<script src="{{asset("js/scripts.js")}}"></script>

<!-- page script -->
<script>
    $(function () {

        //          ORDEM DOS BANNERS
        $('.ordem').change(function () {
            var id_ban = $(this).attr('data-id');
            var href = $(this).attr('data-href');
            var ordem = $(this).val();

            $.ajax({
                type: "GET",
                url: href,
                data: {
                    ordem: ordem
                }
            }).done(function (data) {

                if (data == 1) {
                   $('.span' + id_ban).removeClass('hidden').html('Registro atualizado com sucesso!')
                    setTimeout(function () {
                        $('.span' + id_ban).addClass('hidden')
                    }, 3000);

                } else {
                    $('.span' + id_ban).removeClass('hidden').html('Erro ao atualizar registro!')
                    setTimeout(function () {
                        $('.span' + id_ban).addClass('hidden')
                    }, 3000);

                }
            });
        });

        // FUNÇÃO QUE ATUALIZA OS VALORES NO ORÇAMENTO
        $('.valoritem').change(function () {
            var id = $(this).attr('data-id');
            var valor = $(this).val();

            $.ajax({
                type: "GET",
                url: "{{getenv("PAINEL")}}/orcamentos/valor-orcamento",
                data: {
                    id: id,
                    valor: valor
                },
                success: function (data) {
                    $("span[data-id='" + id + "']").text(data);
                }
            });
        });

        $("#departamento").change(function () {
            var tgt_id = $(this).attr("target-id");
                var atual = $(this).attr("id-sub");
                $(tgt_id).children("option").remove();
                $(tgt_id).append('<option value="">Carregando...</option>');
                $.getJSON("{{getenv("PAINEL")}}/produtoscategorias/getbydepid/" + $(this).val(), function (data) {
                    $(tgt_id).children("option").remove();
//                $(tgt_id).append('<option value="0">Geral</option>');
                    $.each(data, function (i, item) {
                        $(tgt_id).append('<option value="' + item.CatCodigo + '" ' + ((atual == item.CatCodigo) ? 'selected' : '') + '>' + item.CatTitulo + '</option>');
                    });
            });
        });

        if ($('#example2').size() > 0) {
            /*$('#example2').DataTable({
             "paging": false
             //            "ordering": false
             });*/
        }
        if ($('[data-toggle="popover"]').size() > 0) {
            $('[data-toggle="popover"]').popover();
        }
        $(":input").inputmask();
        if ($("textarea.editor").size() > 0) {
            $("textarea.editor").wysihtml5();
        }
        if ($("#valor").size() > 0) {
            $("#valor").maskMoney({thousands:'', decimal:'.'});
            $(".valor").maskMoney({thousands:'', decimal:'.'});
        }

        if ($("#valoritem").size() > 0) {
            $("#valoritem").maskMoney({thousands:'', decimal:'.'});
            $(".valoritem").maskMoney({thousands:'', decimal:'.'});
        }

        $('.maskFone').inputmask('(99) 9999-9999[9]');

        @if(@$estatisticas !== null)
        if ($("#line-chart").size() > 0) {
            // LINE CHART
            var line = new Morris.Line({
                element: 'line-chart',
                resize: true,
                data: [
                    @foreach($estatisticas as $item)
                        {y: '{{date("Y-m-d",strtotime($item->EstData))}}', item1: '{{$item->total}}}'},
                    @endforeach
                ],
                xkey: 'y',
                ykeys: ['item1'],
                labels: ['PageViews'],
                lineColors: ['#3c8dbc'],
                hideHover: 'auto'
            });
        }
        @endif

    });
</script>
</body>
</html>