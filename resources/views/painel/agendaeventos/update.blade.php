@extends("painel.templates.app")
@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Alterar Eventos
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{getenv("PAINEL")}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{getenv("PAINEL")}}/agendaeventos"> Agenda de Eventos</a></li>
        <li class="active">Alterar</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="with-border"></div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="{{getenv("PAINEL")}}/agendaeventos/update" method="post">
                    <div class="box-body">
                        <div class="col-sm-12">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="id" value="{{$item->AgeCodigo}}">

                            <div class="form-group">
                                <label for="datainicio">Data Início</label>
                                <input type="text" class="form-control" id="datainicio" name="datainicio" size="10" maxlength="10"
                                       placeholder="Data Início"
                                       data-inputmask="'alias': 'date'"
                                       value="{{date("d/m/Y",strtotime($item->AgeDataInicio))}}" required>
                            </div>
                            <div class="form-group">
                                <label for="datafim">Data Fim</label>
                                <input type="text" class="form-control" id="datafim" name="datafim" size="10" maxlength="10"
                                       placeholder="Data Fim"
                                       data-inputmask="'alias': 'date'"
                                       value="{{date("d/m/Y",strtotime($item->AgeDataFim))}}" required>
                            </div>
                            <div class="form-group">
                                <label for="titulo">Título</label>
                                <input type="text" class="form-control" id="titulo" name="titulo" placeholder="Título"
                                       value="{{$item->AgeTitulo}}"
                                       required maxlength="150">
                            </div>
                            <div class="form-group">
                                <label for="conteudo">Conteúdo</label>
                            <textarea class="form-control editor" name="conteudo" id="conteudo" placeholder="Conteúdo"
                                      rows="10">{{$item->AgeConteudo}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="legenda">Liberado</label>
                                <br>

                                <div class="radio-inline">
                                    <label><input type="radio" name="liberado" id="liberado"
                                                  value="1" {{($item->AgeLiberado == 1)?"checked":""}}>Sim</label>
                                </div>
                                <div class="radio-inline">
                                    <label><input type="radio" name="liberado" id="liberado"
                                                  value="0" {{($item->AgeLiberado == 0)?"checked":""}}>Não</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Salvar</button>
                        </div>
                    </div>
                </form>

            </div>
            <!-- /.box -->
        </div>
    </div>
    <!-- /.row -->
</section><!-- /.content -->

@endsection