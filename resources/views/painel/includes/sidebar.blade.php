<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <a href="#" class=" btn-upload-modal"
                   data-href="{{getenv("PAINEL")}}/usuarios/upload/{{Auth::user()->UserCodigo}}">
                    <img src="{{ asset('/upload/usuarios/avatar_'.Auth::user()->UserCodigo.".jpg")}}" class="img-circle"
                         style="width: 40px;height: 40px;"/>
                </a>
            </div>
            <div class="pull-left info">
                <p>{{Auth::user()->UserNome}}</p>
                <i class="fa fa-user text-success"></i> {{Auth::user()->UserId}}
            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MENU NAVEGAÇÃO</li>
            <li class="active">
                <a href="{{getenv("PAINEL")}}">
                    <i class="fa fa-home"></i>
                    <span>Página Inicial</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gear"></i>
                    <span>Vagas de emprego</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{getenv("PAINEL")}}/vagas"><i class="fa fa-circle-o"></i>Listar</a></li>
                    <li><a href="{{getenv("PAINEL")}}/vagas/create"><i class="fa fa-circle-o"></i>Adicionar</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gear"></i>
                    <span>Notícias</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{getenv("PAINEL")}}/noticias"><i class="fa fa-circle-o"></i>Listar</a></li>
                    <li><a href="{{getenv("PAINEL")}}/noticias/create"><i class="fa fa-circle-o"></i>Adicionar</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gear"></i>
                    <span>Galerias</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{getenv("PAINEL")}}/galerias"><i class="fa fa-circle-o"></i>Listar</a></li>
                    <li><a href="{{getenv("PAINEL")}}/galerias/create"><i class="fa fa-circle-o"></i>Adicionar</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gear"></i>
                    <span>Páginas</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{getenv("PAINEL")}}/paginas"><i class="fa fa-circle-o"></i>Listar</a></li>
                    @if(Auth::user()->UserNivel == 1)
                        <li><a href="{{getenv("PAINEL")}}/paginas/create"><i class="fa fa-circle-o"></i>Adicionar</a>
                        </li>
                    @endif
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gear"></i>
                    <span>Unidades</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{getenv("PAINEL")}}/unidades"><i class="fa fa-circle-o"></i>Listar</a></li>
                    <li><a href="{{getenv("PAINEL")}}/unidades/create"><i class="fa fa-circle-o"></i>Adicionar</a>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gear"></i>
                    <span>Cotações</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{getenv("PAINEL")}}/cotacoesagricolas"><i class="fa fa-circle-o"></i>Listar</a></li>
                    <li><a href="{{getenv("PAINEL")}}/cotacoesagricolas/create"><i class="fa fa-circle-o"></i>Adicionar</a>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gear"></i>
                    <span>Parceiros</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{getenv("PAINEL")}}/parceiros"><i class="fa fa-circle-o"></i>Listar</a></li>
                    <li><a href="{{getenv("PAINEL")}}/parceiros/create"><i class="fa fa-circle-o"></i>Adicionar</a>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gear"></i>
                    <span>Produtos/Departamentos</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{getenv("PAINEL")}}/produtosdepartamentos"><i class="fa fa-circle-o"></i>Listar</a>
                    </li>
                    <li><a href="{{getenv("PAINEL")}}/produtosdepartamentos/create"><i class="fa fa-circle-o"></i>Adicionar</a>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gear"></i>
                    <span>Produtos/Categorias</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{getenv("PAINEL")}}/produtoscategorias"><i class="fa fa-circle-o"></i>Listar</a></li>
                    <li><a href="{{getenv("PAINEL")}}/produtoscategorias/create"><i class="fa fa-circle-o"></i>Adicionar</a>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gear"></i>
                    <span>Produtos/Marcas</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{getenv("PAINEL")}}/produtosmarcas"><i class="fa fa-circle-o"></i>Listar</a></li>
                    <li><a href="{{getenv("PAINEL")}}/produtosmarcas/create"><i class="fa fa-circle-o"></i>Adicionar</a>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gear"></i>
                    <span>Produtos</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{getenv("PAINEL")}}/produtos"><i class="fa fa-circle-o"></i>Listar</a></li>
                    <li><a href="{{getenv("PAINEL")}}/produtos/create"><i class="fa fa-circle-o"></i>Adicionar</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gear"></i>
                    <span>Banners</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{getenv("PAINEL")}}/banners"><i class="fa fa-circle-o"></i>Listar</a></li>
                    <li><a href="{{getenv("PAINEL")}}/banners/create"><i class="fa fa-circle-o"></i>Adicionar</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gear"></i>
                    <span>Programas da Rádio</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{getenv("PAINEL")}}/programasradio"><i class="fa fa-circle-o"></i>Listar</a></li>
                    <li><a href="{{getenv("PAINEL")}}/programasradio/create"><i class="fa fa-circle-o"></i>Adicionar</a>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="{{getenv("PAINEL")}}/orcamentos">
                    <i class="fa fa-gear"></i>
                    <span>Orçamentos</span>
                </a>
            </li>
            <li class="treeview">
                <a href="{{getenv("PAINEL")}}/contatos">
                    <i class="fa fa-gear"></i>
                    <span>Contatos</span>
                </a>
            </li>
            <li class="treeview">
                <a href="{{getenv("PAINEL")}}/trabalheconosco">
                    <i class="fa fa-gear"></i>
                    <span>Trabalhe Conosco</span>
                </a>
            </li>
            <li class="header">ADMINISTRAÇÃO</li>
            @if(Auth::user()->UserNivel <= 2)
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-users"></i>
                        <span>Usuários</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{getenv("PAINEL")}}/usuarios"><i class="fa fa-circle-o"></i>Listar</a></li>
                        <li><a href="{{getenv("PAINEL")}}/usuarios/create"><i class="fa fa-circle-o"></i>Adicionar</a>
                        </li>
                    </ul>
                </li>
            @endif
            <li>
                <a href="{{getenv("PAINEL")}}/usuarios/update/{{Auth::user()->UserCodigo}}">
                    <i class="fa fa-lock"></i>
                    <span>Mudar Senha</span>
                </a>
            </li>
            <li>
                <a href="{{getenv("PAINEL")}}/logout">
                    <i class="fa fa-sign-out"></i>
                    <span>Sair</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>