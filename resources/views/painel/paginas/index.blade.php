@extends("painel.templates.app")
@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Editar Páginas
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{getenv("PAINEL")}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{getenv("PAINEL")}}/paginas"> Páginas</a></li>
        <li class="active">Editar</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        @if(Auth::user()->UserNivel == 1)
            <div class="col-sm-12">
                <a href="{{getenv("PAINEL")}}/paginas/create" class="btn btn-primary btn-lg margin-bottom"><i
                            class="fa fa-plus"></i> Adicionar</a>
            </div>
        @endif
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="with-border"></div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-sm-4 col-sm-offset-8">
                        <form action="{{getenv("PAINEL")}}/paginas" method="POST">
                            <div class="input-group">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="text" name="busca" id="busca" class="form-control"
                                       placeholder="Buscar por..." value="">
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default" type="button">Buscar</button>
                                </div>
                            </div><!-- /input-group -->
                            <div class="clearfix">&nbsp;</div>
                        </form>
                    </div>
                    <div class="col-sm-12">
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                        @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif
                    </div>
                    <div class="col-sm-12 table-responsive" style="min-height: 300px;">
                        <table class="table table-bordered table-hover" style="width: 100%">
                            <thead>
                            <tr>
                                <th class="col-sm-1 text-center">#</th>
                                <th class="col-sm-1 text-center">#</th>
                                <th class="col-sm-1 text-center"><i class="fa fa-plus"></i> Fotos</th>
                                <th class="col-sm-1 text-center">Codigo</th>
                                <th class="col-sm-1 text-center">Imagem</th>
                                <th class="col-xs-6">Titulo</th>
                                <th class="col-sm-1 text-center">#</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($itens as $item)
                                <tr>
                                    <td class="text-center" style="vertical-align: middle">
                                        <a href="{{getenv("PAINEL")}}/paginas/update/{{$item->PagCodigo}}"
                                           class="btn btn-primary btn-sm">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </td>
                                    <td class="text-center" style="vertical-align: middle">
                                        <a href="#"
                                           class="btn btn-primary btn-sm btn-upload-modal"
                                           data-href="{{getenv("PAINEL")}}/paginas/upload/{{$item->PagCodigo}}">
                                            <i class="fa fa-camera"></i>
                                        </a>

                                    </td>
                                    <td class="text-center" style="vertical-align: middle">
                                        <a href="{{getenv("PAINEL")}}/paginas/{{$item->PagCodigo}}"
                                           class="btn btn-primary btn-sm">
                                            <i class="fa fa-camera"></i>
                                        </a>

                                    </td>
                                    <td class="text-center" style="vertical-align: middle">{{$item->PagCodigo}}</td>
                                    <td class="text-center" style="vertical-align: middle">
                                        <a href="javascript:void(0);" title="<center>Imagem Atual</center>"
                                           data-toggle="popover"
                                           data-placement="bottom"
                                           data-html="true" data-trigger="focus"
                                           data-content='<img src="{{asset("/upload/paginas/dest_".$item->PagCodigo.".jpg?cache=".date("YmdHis"))}}" class=img-responsive>'>
                                            <i class="fa fa-image fa-2x"></i>
                                        </a>
                                    </td>
                                    <td style="vertical-align: middle">{{$item->PagTitulo}}</td>

                                    <td class="text-center" style="vertical-align: middle">
                                        <a href="{{getenv("PAINEL")}}/paginas/destroy/{{$item->PagCodigo}}"
                                           class="btn btn-danger btn-sm btn-destroy"><i class="fa fa-close"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            @if($itens->count() < 1)
                                <tr>
                                    <td class="text-center" colspan="7">
                                        Nenhum Registro Encontrado
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>


                    </div>
                    <div class="col-sm-12 text-center">
                        {!! $itens->render() !!}
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                </div>

            </div>
            <!-- /.box -->
        </div>
    </div>
    <!-- /.row -->
</section><!-- /.content -->

@endsection