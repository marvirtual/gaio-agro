@extends("painel.templates.app")
@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Adicionar Usuários
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{getenv("PAINEL")}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{getenv("PAINEL")}}/usuarios"> Usuários</a></li>
        <li class="active">Adicionar</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header"></div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="{{getenv("PAINEL")}}/usuarios/create" method="post">
                    <div class="box-body">
                        <div class="col-sm-12">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">

                            <div class="form-group">
                                <label for="nome">Nome</label>
                                <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="usuario">Usuário[Id]</label>
                                <input type="text" class="form-control" id="usuario" name="usuario"
                                       placeholder="Usuário"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="senha">Senha</label>
                                <input type="text" class="form-control" id="senha" name="senha" placeholder="Senha"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="nivel">Nível</label>
                                <br>

                                @if(Auth::user()->UserNivel == 1)
                                    <div class="radio-inline">
                                        <label><input type="radio" name="nivel" id="nivel" value="1">Web Master</label>
                                    </div>
                                @endif
                                @if(Auth::user()->UserNivel <= 2)
                                    <div class="radio-inline">
                                        <label><input type="radio" name="nivel" id="nivel" value="2">Administrador</label>
                                    </div>
                                    <div class="radio-inline">
                                        <label><input type="radio" name="nivel" id="nivel" value="3" checked>Normal</label>
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="legenda">Liberado</label>
                                <br>

                                <div class="radio-inline">
                                    <label><input type="radio" name="liberado" id="liberado" value="1"
                                                  checked>Sim</label>
                                </div>
                                <div class="radio-inline">
                                    <label><input type="radio" name="liberado" id="liberado" value="0">Não</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Salvar</button>
                        </div>
                    </div>
                </form>

            </div>
            <!-- /.box -->
        </div>
    </div>
    <!-- /.row -->
</section><!-- /.content -->

@endsection