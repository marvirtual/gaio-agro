@extends("site.templates.app")
@section("title","Vagas de Emprego")
@section("content")
    <section class="section-conteudo">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="titulo-paginas">
                        Vagas Disponíveis
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                <div class="box-vaga" style="position: relative; margin-bottom: 15px; padding: 15px;">
                    <h2 style="margin-top: 0px;">{{$vaga->titulo}}</h2>
                    <p style="margin-bottom: 0px;">Cidade: {{$vaga->unidade}}</p>
                    <p style="margin-bottom: 0px;">Publicada em: {{date("d/m/Y", strtotime($vaga->created_at))}}</p>

                    <div class="atribuicoes" style="margin: 15px 0px;">
                        <p><b>Atribuições e Responsabilidades:</b></p>
                        {!! $vaga->atribuicoes !!}
                    </div>

                    <div class="atribuicoes">
                        <p><b>Requisitos:</b></p>
                        {!! $vaga->requisitos !!}
                    </div>

                    <a href="{{url("/vaga/".$vaga->id)}}" class="btn" style="background-color: #374E64 !important; color: #fff !important;">Candidatar-me agora</a>
                </div>
                </div>
                @include("site.includes.menu")
            </div>
        </div>
    </section>
@endsection