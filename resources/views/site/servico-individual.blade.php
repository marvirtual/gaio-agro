@extends("site.templates.app")
@section("title",$servico->SerTitulo)

@section("content")
    <a name="posiciona" id="posiciona"></a>
    <!--INICIO SERVIÇO-->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 col-lg-6 col-lg-offset-3">
                    <div class="row" style="margin-top: 15px;">
                        <div class="col-xs-6 col-xs-offset-3"
                             style="border-top: 2px solid #333333; padding-bottom: 20px;"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <p class="font25b text-uppercase">{{$servico->SerTitulo}}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="padding-bottom: 15px;margin-top:15px;">
                <div class="col-sm-4">
                    <div class="row light-gallery">
                        @if($destaque == "1")
                            <div class="col-sm-12"
                                 data-src="{{asset("upload/servicos/dest_" . $servico->SerCodigo . ".jpg")}}"
                                 data-sub-html="{{$servico->SerTitulo}}">
                                <img src="{{asset("upload/servicos/dest_" . $servico->SerCodigo . ".jpg")}}"
                                     class="center-block img-responsive img-thumbnail" style="padding:5px;">
                            </div>
                        @endif
                        @foreach($fotos as $foto)
                            <div class="col-sm-4 col-xs-4" style="padding-top:5px;padding-bottom:5px;"
                                 data-src="{{asset("upload/servicos/g_" . $foto->FotCodigo . ".jpg")}}"
                                 data-sub-html="{{$foto->FotLegenda}}">
                                <img src="{{asset("upload/servicos/p_" . $foto->FotCodigo . ".jpg")}}"
                                     class="center-block img-thumbnail img-responsive">
                            </div>
                        @endforeach

                    </div>
                </div>
                <div class="col-sm-8 font15c">
                    {!! $servico->SerConteudo !!}
                </div>
            </div>
        </div>
    </section>
@endsection