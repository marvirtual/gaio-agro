<!DOCTYPE HTML>
<html>
<head>
    <link rel="icon" type="image/png" href="{{asset("imgs/favicon.png")}}"/>
    <meta http-equiv=pragma content=no-cache>
    <meta http-equiv=expires content="Mon, 06 Jan 1990 00:00:01 GMT">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="pt-br">
    <META name="author" content="Desenvolvido por Grupo Mar Virtual - www.marvirtual.com.br">
    <meta name="robots" content="index, follow">
    <META name="title" content="Gaio Agronegócios">
    <meta name="description" content="A Gaio Agronegócios atua na área do Agronegócio">
    <meta name="keywords"
          content="Agronegócios, Gaio, Sudoeste, Paraná, Gaio Agronegócios, Gaio Dois Vizinhos, Gaio Agro">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300italic,300,100italic,100,400italic,700,700italic,900,900italic'
          rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
          rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic'
          rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,300,300italic,400italic,500,500italic,700,700italic'
          rel='stylesheet' type='text/css'>
    <link href="{{asset("bootstrap/css/bootstrap.min.css")}}" rel="stylesheet">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset("css/style.css")}}?c={{date('YmdHis')}}" type="text/css">
    <link rel="stylesheet" href="{{asset("css/navbar.css")}}" type="text/css">
    <link rel="stylesheet" href="{{asset("css/lightGallery.css")}}" type="text/css">
    <link rel="stylesheet" href="{{asset("css/owl.carousel.css")}}" type="text/css">
    <link rel="stylesheet" href="{{asset("css/owl.transitions.css")}}" type="text/css">
    <link rel="stylesheet" href="{{asset("css/owl.theme.css")}}" type="text/css">
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <title>Gaio Agronegócios - @yield('title')</title>
    @yield('metatags')

<!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-70375606-8"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-70375606-8');
    </script>
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '204414234716180');
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=204414234716180&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->
</head>
<body>
@include("site.includes.top")
@include("site.includes.banner")
@yield('content')
@include("site.includes.copy")

<!-- jQuery 2.1.4 -->
<script src="{{asset("plugins/jQuery/jQuery-2.1.4.min.js")}}"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.js"></script>
<script src="{{asset("bootstrap/js/bootstrap.min.js")}}"></script>
<script src="{{asset("js/lightGallery.js")}}"></script>
<script src="{{asset("js/lg-fullscreen.js")}}"></script>
<script src="{{asset("js/lg-pager.js")}}"></script>
<script src="{{asset("js/lg-thumbnail.js")}}"></script>
<script src="{{asset("js/owl.carousel.min.js")}}"></script>
<script src="{{asset("js/owl.carousel.js")}}"></script>
<script src="{{asset("plugins/input-mask/jquery.inputmask.js")}}"></script>
<script src="{{asset("plugins/input-mask/jquery.inputmask.extensions.js")}}"></script>
<script src="{{asset("plugins/input-mask/jquery.inputmask.date.extensions.js")}}"></script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>

<script type="text/javascript">
    {{--FUNÇÃO PARA SELEÇÃO DE UNIDADES--}}
    $(function () {

        $("#form-enviar-btn").submit(function(){
            $("#btn-enviar-form").attr('disabled','disabled');
        });

        $(".btn-popup-cidade").click(function () {

            $("#modal-cidade").modal({
                "backdrop": true,
                "keyboard": true
            });
        });

        // MODAL CONTATO ASSOCIADOS
        $(".btn-mapa-modal").on("click", function () {
            $("#mapaModal .modal-body .sec-secondary").hide();
            $("#mapaModal").modal("show");
            $("#localizacaoModalForm").html($(this).attr("data-localizacao"));
            $("#tituloModalForm").html($(this).attr("data-title"));
        });
    });

    $(function () {
        setTimeout(function () {
            alturafixa();
        }, 1000);


        // var owl1 = $("#owl-home1");
        // owl1.owlCarousel({
        //     itemsCustom: [
        //         [0, 1],
        //         [450, 1],
        //         [600, 2],
        //         [700, 3],
        //         [1000, 5],
        //         [1200, 6],
        //         [1400, 6],
        //         [1600, 6]
        //     ],
        //     navigation: false,
        //     //Pagination
        //     pagination: false,
        //     paginationNumbers: false,
        //     autoPlay: 3000
        // });
        // // Custom Navigation Events
        // $(".owl-next").click(function () {
        //     owl1.trigger('owl.next');
        // });
        //
        // $(".owl-prev").click(function () {
        //     owl1.trigger('owl.prev');
        // });

        if ($("#owl-par").size() > 0) {
            var owl_par = $("#owl-par");
            owl_par.owlCarousel({
                itemsCustom: [
                    [0, 1],
                    [450, 2],
                    [600, 2],
                    [700, 3],
                    [1000, 4],
                    [1200, 5],
                    [1400, 5],
                    [1600, 5]
                ],
                navigation: false,
                //Pagination
                pagination: false,
                paginationNumbers: false,
                autoPlay: 3000
            });

            // Custom Navigation Events
            $(".owl-next-par").click(function () {
                owl_par.trigger('owl.next');
            });

            $(".owl-prev-par").click(function () {
                owl_par.trigger('owl.prev');
            });
        }

        if ($('div[class="carousel"]').size() > 0) {
            $('div[class="carousel"]').carousel();
        }
        if ($(".light-gallery").size() > 0) {
            $(".light-gallery").lightGallery();
        }
        if ($('.posiciona-contato').size() > 0) {
            $('.posiciona-contato').click(function () {
                $('html, body').animate({
                    scrollTop: $("#contato").offset().top
                }, 1000);
            });
        }
        if ($('#posiciona').size > 0) {
            $('html, body').animate({
                scrollTop: $("#posiciona").offset().top
            }, 100);
        }

        //Input MASK
        $('.telefone').inputmask('(99) 9999-9999[9]');
        $('.datanascimento').inputmask('99/99/9999');


        // FUNÇÃO QUE ATUALIZA A QUANTIDADE NO CARRINHO
        $('.qtde').change(function () {
            var id = $(this).attr('data-id');
            var valor = $(this).val();

            $.ajax({
                type: "GET",
                url: "/qtde-orcamento",
                data: {
                    id: id,
                    qtde: valor
                },
                success: function (data) {
                    $("span[data-id='" + id + "']").show();
                    setTimeout(function () {
                        $("span[data-id='" + id + "']").hide()
                    }, 5000);
                }
            });
        });
    });
    function alturafixa() {
        if ($(".altura-fixa-auto").size() > 0) {
            altura = 0;
            $(".altura-fixa-auto").each(function () {
                if ($(this).height() > altura) {
                    altura = $(this).height() + 5;
                }
            });
            $(".altura-fixa-auto").css({"min-height": altura + "px"});
        }

        if ($(".altura-fixa-auto2").size() > 0) {
            altura = 0;
            $(".altura-fixa-auto2").each(function () {
                if ($(this).height() > altura) {
                    altura = $(this).height() + 10;
                }
            });
            $(".altura-fixa-auto2").css({"min-height": altura + "px"});
        }
    }
    alturafixa();
</script>

</body>
</html>