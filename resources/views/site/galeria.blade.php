@extends("site.templates.app")
@section("title","Galerias - ".@$galeria->GalTitulo)
@section("metatags")
    <meta property="og:locale" content="pt_BR"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="{{@$galeria->GalTitulo}}"/>
    <meta property="og:description" content="Gaio Agronegócios"/>
    <meta property="og:url" content="http://gaioagro.com.br/galeria/id/{{@$galeria->GalCodigo}}"/>
    <meta property="og:site_name" content="Gaio Agronegócios"/>
    <meta property="article:publisher" content="https://facebook.com"/>
    <meta property="og:image" content="{{asset("upload/galerias/dest_".@$galeria->GalCodigo.".jpg")}}"/>
@endsection
@section("content")
    <?php
    setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
    ?>
    <section class="section-conteudo">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="titulo-paginas">
                        Galerias de Fotos
                    </div>
                </div>
            </div>
            {{-- EXIBE AS GALERIAS --}}
            <div class="row">
                <div class="col-xs-12">
                    <div style="margin-bottom: 20px;">
                        <div class="font12a">{{utf8_encode(strftime('%A, %d de %B de %Y', strtotime(@$galeria->GalData)))}}</div>
                        <div class="font20a"><b>{{@$galeria->GalTitulo}}</b></div>
                        <div class="font14a">{!!strip_tags(@$galeria->GalResumo)!!}...</div>
                    </div>
                    {{-- EXIBE AS IMAGENS E INFORMAÇÕES --}}
                    <div class="row light-gallery">
                        @if(sizeof(@$itens) > 0)
                            @foreach(@$itens as $item)
                                <div class="col-xs-6 col-sm-2 altura-fixa-auto"
                                     data-src="{{asset("upload/galerias/g_" . @$item->FotCodigo . ".jpg")}}"
                                     data-sub-html="{{"<b>".@$item->FotLegenda."</b>"}}">
                                    {{--EXIBE FOTOS--}}
                                    <img src="{{asset("upload/galerias/p_" . @$item->FotCodigo . ".jpg")}}"
                                         class="center-block img-responsive img-thumbnail"
                                         style="padding:5px; margin-bottom: 5px; max-height: 140px !important;">
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <div class="clearfix">&nbsp;</div>
                    <a class="btn btn-primary btn-xs"
                       href="http://www.facebook.com/sharer.php?u=http://gaioagro.com.br/galeria/id/{{@$galeria->GalCodigo."?".date("YmdHis")}}"
                       title="Compartilhar via Facebook" target="_blank"><span><i class="fa fa-facebook-square"></i> Facebook</span></a>

                    <a class="btn btn-info btn-xs"
                       href="http://twitter.com/share?url=http://gaioagro.com.br/galeria/id/{{@$galeria->GalCodigo."?".date("YmdHis")}}&amp;text=Gaio Agronegócios - {{$galeria->GalTitulo}}"
                       title="Compartilhar no Twitter" target="_blank"><span><i
                                    class="fa fa-twitter"></i> Twitter</span></a>
                    <a class="btn btn-success btn-xs"
                       href="https://api.whatsapp.com/send?text=http://gaioagro.com.br/galeria/id/{{@$galeria->GalCodigo."?".date("YmdHis")}}"
                       title="Compartilhar no WhatsApp" target="_blank"><span><i class="fa fa-whatsapp"></i><span> WhatsApp</span></span></a>
                </div>
            </div>
        </div>

    </section>
@endsection