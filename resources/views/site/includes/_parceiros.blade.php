<section class="section-conteudo">
    <div class="container">
        <div class="row">
            <div class="col-xs-2 col-sm-1">
                <a href="javascript:;" class="owl-prev">
                    <img src="imgs/left-icon.jpg" alt="" class="img-responsive center-block">
                </a>
            </div>
            <div class="col-xs-8 col-sm-10">
                <div id="owl-home1" class="owl-carousel owl-theme">
                    @if(sizeof(@$parceiros) > 0)
                        @foreach(@$parceiros as $parceiro)
                            <div class="item">
                                <table width="100%">
                                    <tr>
                                        <td height="100" style="vertical-align: middle;" align="center">
                                            <a href="{{($parceiro->ParLink == "") ? "javascript:;" : $parceiro->ParLink}}"
                                               title="{{$parceiro->ParTitulo}}" target="_blank">
                                                <img src="{{asset("upload/parceiros/".$parceiro->ParCodigo.".jpg")}}"
                                                     class="img-parceiros" style="max-height: 100px; max-width: 140px;">
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="col-xs-2 col-sm-1">
                <a href="javascript:;" class="owl-next">
                    <img src="imgs/right-icon.jpg" alt="" class="img-responsive center-block">
                </a>
            </div>
        </div>
    </div>
</section>