<?php
if (strlen($noticia->NotLinkFoto) > 10) {
    $url_foto = $noticia->NotLinkFoto;
} else {
    $url_foto = asset("upload/noticias/p_" . $noticia->NotCodigo . ".jpg");
}
?>

<a class="visible-xs" href="/noticia/id/{{$noticia->NotCodigo}}">
    <div class="row altura-fixa-auto2">
        <div class="col-xs-5">
            @if($noticia->NotVideo == "")
                <div style="width:100%; background-repeat:no-repeat !important; padding-top: 60%; background:url({{$url_foto}}); background-size:cover;"></div>
            @else
                <div style="width:100%; background-repeat:no-repeat !important; padding-top: 60%; background:url('http://img.youtube.com/vi/{{$noticia->NotVideo}}/mqdefault.jpg'); background-size:cover;"></div>
            @endif
        </div>

        <div class="col-xs-7">
            <div class="font12a"
                 style="padding: 2px; background: #F58934; color: #FFFFFF; margin-bottom: 5px; float: left;">{{ utf8_encode(strftime('%d de %B de %Y', strtotime($noticia->NotData)))}}</div>
            <div class="clearfix"></div>
            <div class="font14a">{{strip_tags($noticia->NotTitulo)}}</div>
        </div>
    </div>
</a>