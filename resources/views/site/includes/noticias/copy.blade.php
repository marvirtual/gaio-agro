<!--INICIO RODAPE-->
<section class="section-rodape">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 separador hidden-xs">
                <img src="{{asset('imgs/logomarca.png')}}" class="img-responsive center-block img-logo-copy">
            </div>
            <div class="col-md-3 col-sm-6 separador">
                <ul class="menu-copy">
                    <li><a href="/">HOME</a></li>
                    <li><a href="/classificados">CLASSIFICADOS</a></li>
                    <li><a href="/p/politica-de-privacidade">POLÍTICA DE PRIVACIDADE</a></li>
                    <li><a href="/contato">CONTATO</a></li>
                </ul>
            </div>
            {{--<div class="col-md-3 col-sm-6 separador">--}}
                {{--<h4>Formas de Pagamento</h4>--}}
            {{--</div>--}}
            <div class="col-md-3 col-sm-6 separador">
                <h4>Contato</h4>
                <p>
                    Dois Vizinhos/PR <br>
                    Rua Guilherme Guzzo <br>
                    115 - Centro <br>
                    (46) 3536-8600 <br>
                    97.385.876/0001-35
                </p>
                <a href=""><img src="{{asset('imgs/facebook.png')}}"></a>
                <a href=""><img src="{{asset('imgs/insta.png')}}"></a>
            </div>
        </div>
    </div>
</section>
<section style="background-color: #E6E6E6; padding: 15px;">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 font12a">2017 - Todos os direitos reservados | Desenvolvido por <a href="http://www.marvirtual.com.br"
                                                                                                    title="Desenvolvido por Grupo Mar Virtual"
                                                                                                    alt="Desenvolvido por Grupo Mar Virtual"
                                                                                                    class="font12a">Grupo Mar Virtual</a></div>
        </div>
    </div>
</section>
<!--FIM RODAPE-->