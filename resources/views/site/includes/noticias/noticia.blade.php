<?php
    if(strlen($noticia->NotLinkFoto) > 10){
        $url_foto = asset("imgs/sem-foto.jpg");
    }else{
        $url_foto = asset("upload/noticias/p_".$noticia->NotCodigo.".jpg");
    }
?>

<a class="hidden-xs" href="/noticia/id/{{$noticia->NotCodigo}}">
    <div class="col-sm-3 col-xs-12 altura-fixa-auto">
        @if($noticia->NotVideo == "")
            <div
                    style="width:100%; height:100%; padding-top: 55%; background:url({{$url_foto}}); background-size:100%;"></div>
        @else
            <div style="width:100%; height:100%; padding-top: 55%; background:url('http://img.youtube.com/vi/{{$noticia->NotVideo}}/mqdefault.jpg'); background-size:100%;"></div>
        @endif
        <div class="font12a"
             style="padding: 2px; background: #F58934; color: #FFFFFF; margin-bottom: 5px; float: left;">{{ utf8_encode(strftime('%d de %B de %Y', strtotime($noticia->NotData)))}}</div>
        <div class="clearfix"></div>
        <div class="font14a">{{strip_tags($noticia->NotTitulo)}}</div>
    </div>
</a>