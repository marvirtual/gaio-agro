<!--INICIO CONTATO-->
<section class="section-copy">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="mapa-site">
                            <h4>AGRONEGÓCIO</h4>
                            <ul>
                                @if(@$blocos["departamentos"] !== null)
                                    @foreach($blocos["departamentos"] as $departamento)
                                        <li>
                                            <a href="/produtos?dep={{$departamento->DepCodigo}}">{{$departamento->DepTitulo}}</a>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                        <div class="mapa-site">
                            <h4>CEREAIS</h4>
                            <ul>
                                @if(@$blocos["cerealistas"] !== null)
                                    @foreach($blocos["cerealistas"] as $cerealista)
                                        <li>
                                            <a href="/cerealista/id/{{$cerealista->UniCodigo}}">{{$cerealista->UniMunicipio}}</a>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="mapa-site">
                            <h4>ENERGIAS RENOVÁVEIS</h4>
                            <ul>
                                <li>Energia Fotovoltáica</li>
                                <li>Aquecedor Solar</li>
                            </ul>
                        </div>
                        <div class="mapa-site">
                            <h4>INSTITUCIONAL</h4>
                            <ul>
                                <li><a href="/institucional">Quem Somos</a></li>
                                <li><a href="/area-atuacao">Área de Atuação</a></li>
                                <li><a href="/unidades">Unidades de Atendimento</a></li>
                                <li><a href="/galerias">Galerias de Fotos</a></li>
                                <li><a href="/programas-radio">Programas de Rádio</a></li>
                                <li><a href="/noticias">Notícias</a></li>
                                <li><a href="/trabalhe-conosco">Trabalhe Conosco</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <a class="btn btn-lg" href="https://www.facebook.com/gaio.agronegocios" style="color: #FFFFFF;"
                           target="_blank"><i
                                    class="fa fa-facebook"></i></a>
                        <a class="btn btn-lg" href="#" style="color: #FFFFFF;"><i
                                    class="fa fa-twitter"></i></a>
                        <a class="btn btn-lg" href="https://www.instagram.com/gaioagro/" style="color: #FFFFFF;"><i
                                    class="fa fa-instagram"></i></a>
                        <a class="btn btn-lg" href="#" style="color: #FFFFFF;"><i
                                    class="fa fa-youtube-play"></i></a>
                    </div>
                    <div class="col-xs-12 text-center font12b">&COPY; {{date("Y")}} Gaio Agronegócios - Todos os
                        direitos reservados
                    </div>
                </div>
            </div>
            <div class="col-sm-4" id="contato">
                <div class="titulo-paginas" style="color: #FFFFFF !important;">
                    CONTATO / SAC
                </div>
                <div style="font-size: 18px; color: #FFFFFF;">{{Session('municipio')}} <a href="/unidades"
                                                                                          style="color: #FFFFFF; font-size: 12px;">(veja
                        outras)</a></div>
                <div style="font-size: 15px; color: #FFFFFF;">{{Session('endereco')}}</div>
                <div style="font-size: 15px; color: #FFFFFF; font-weight: bold;">{{Session('telefone')}}</div>

                <!-- INICIO FORMULÁRIO-->
                <form action="/contato" method="POST" class="form-horizontal" style="margin-top: 20px;">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input type="text" class="form-control form1" id="nome" name="nome" placeholder="Nome">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input type="email" class="form-control form1" name="email" id="email"
                                   placeholder="E-mail">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input type="text" class="form-control form1 telefone" name="telefone" id="telefone"
                                   placeholder="Telefone">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <textarea class="form-control form1" name="mensagem" id="mensagem"
                                      placeholder="Mensagem (Campo Obrigatório)"
                                      required rows="5"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12 text-center">
                            <button type="submit" class="btn btn-lg"
                                    style="font-weight: bold;">
                                ENVIAR
                            </button>
                        </div>
                    </div>
                </form>
                <!-- FIM FORMULÁRIO-->
            </div>
        </div>
    </div>
</section>
<!--FIM CONTATO-->

