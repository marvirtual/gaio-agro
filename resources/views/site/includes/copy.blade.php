<!--INICIO RODAPE-->
<section class="section-rodape">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 separador">
                <a href="{{url('/')}}">
                    <img src="{{asset('imgs/logomarca.png')}}" class="img-responsive center-block img-logo-copy">
                </a>
            </div>
            <div class="col-sm-3 separador">
                <div class="menu-copy">
                    <h4>AGRONEGÓCIO</h4>
                    <ul>
                        @if(@$blocos["departamentos"] !== null)
                            @foreach($blocos["departamentos"] as $departamento)
                                <li>
                                    <a href="/produtos?dep={{$departamento->DepCodigo}}">{{$departamento->DepTitulo}}</a>
                                </li>
                            @endforeach
                        @endif
                    </ul>
                </div>
                <div class="menu-copy">
                    <h4>CEREAIS</h4>
                    <ul>
                        @if(@$blocos["cerealistas"] !== null)
                            @foreach($blocos["cerealistas"] as $cerealista)
                                <li>
                                    <a href="/cerealista/id/{{$cerealista->UniCodigo}}">{{$cerealista->UniMunicipio}}</a>
                                </li>
                            @endforeach
                        @endif
                    </ul>
                </div>
            </div>
            <div class="col-sm-3 separador">
                <div class="menu-copy">
                    <h4>ENERGIAS RENOVÁVEIS</h4>
                    <ul>
                        <li>Energia Fotovoltáica</li>
                        <li>Aquecedor Solar</li>
                    </ul>
                </div>
                <div class="menu-copy">
                    <h4>INSTITUCIONAL</h4>
                    <ul>
                        <li><a href="{{url('/institucional')}}">Quem Somos</a></li>
                        <li><a href="{{url('/area-atuacao')}}">Área de Atuação</a></li>
                        <li><a href="{{url('/unidades')}}">Unidades de Atendimento</a></li>
                        <li><a href="{{url('/galerias')}}">Galerias de Fotos</a></li>
                        <li><a href="{{url('/programas-radio')}}">Programas de Rádio</a></li>
                        <li><a href="{{url('/noticias')}}">Notícias</a></li>
                        <li><a href="{{url('/trabalhe-conosco')}}">Trabalhe Conosco</a></li>
                    </ul>
                </div>
            </div>
            {{--<div class="col-md-3 col-sm-6 separador">--}}
            {{--<h4>Formas de Pagamento</h4>--}}
            {{--</div>--}}
            <div class="col-md-3 col-sm-6 separador text-center">
                <h4>Contato</h4>
                <p>

                    {{Session::get('municipio')}} {{Session::get('tipo') == 0 ? '(CEREALISTA)' : ''}} <br>
                    {{Session::get('endereco')}} <br>
                    {{Session::get('telefone')}}
                    97.385.876/0001-35

                </p>
                <a href="https://www.facebook.com/gaio.agronegocios"><img src="{{asset('imgs/facebook.png')}}"></a>
                <a href="https://www.instagram.com/gaioagro/"><img src="{{asset('imgs/insta.png')}}"></a>
            </div>
        </div>
    </div>
</section>
<section style="background-color: #E6E6E6; padding: 15px;">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 font12a">2017 - Todos os direitos reservados | Desenvolvido por <a
                        href="http://www.marvirtual.com.br"
                        title="Desenvolvido por Grupo Mar Virtual"
                        alt="Desenvolvido por Grupo Mar Virtual"
                        class="font12a">Grupo Mar Virtual</a></div>
        </div>
    </div>
    <div class="botoes-flutuantes-copy-scroll-paginas">
        <a href="https://api.whatsapp.com/send?phone=554635368600" target="_blank" class="link-sem-estilo btn-flutuante-whatsapp-copy" title="Clique para enviar uma mensagem via WhatsApp.">
            <span class="hidden-xs" style="color: #fff !important;">Enviar Mensagem</span><i class="fa fa-whatsapp icone-whatsapp-copy"></i>
        </a>
    </div>
</section>
<!--FIM RODAPE-->