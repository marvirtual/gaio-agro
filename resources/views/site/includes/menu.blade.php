<div class="col-sm-3">
    <div id="menu-produtos">
        <ul>
            @if(count(@$blocos['menus']) > 0)
                @foreach(@$blocos["menus"] as $menu)
                    <li><a href="/produtos/{{@$menu->DepCodigo}}"><i
                                    class="fa fa-angle-right "></i>&nbsp;{{@$menu->DepTitulo}}</a></li>
                    <ul>
                        @if(@$menu->submenus !== null)
                            @foreach(@$menu->submenus as $submenu)
                                <li>
                                    <a href="/produtos/{{@$menu->DepCodigo}}/{{@$submenu->CatCodigo}}">{{@$submenu->CatTitulo}}</a>
                                </li>
                            @endforeach
                        @endif
                    </ul>
                    </li>
                @endforeach
            @endif
        </ul>
    </div>
</div>