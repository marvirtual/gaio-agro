<div class="hidden-xs col-sm-1">
    <a href="javascript:;" class="owl-prev-par">
        <img src="{{asset("imgs/left-icon.png?c=".date('YmdHis'))}}">
    </a>
</div>
<div class="col-xs-12 col-sm-10">
    <div id="owl-par" class="owl-carousel owl-theme">
        @if(sizeof(@$parceiros) > 0)
            @foreach(@$parceiros as $parceiro)
                <div class="item">
                    <table width="100%">
                        <tr>
                            <td height="120" style="vertical-align: middle;" align="center;">
                                <a href="{{($parceiro->ParLink == "") ? "javascript:;" : $parceiro->ParLink}}"
                                   title="{{$parceiro->ParTitulo}}" target="_blank">
                                    <img src="{{asset("upload/parceiros/".$parceiro->ParCodigo.".jpg")}}"
                                         class="img-parceiros" style="max-height: 100px; max-width: 140px;">
                                </a>
                            </td>
                        </tr>
                    </table>
                </div>
            @endforeach
        @endif
    </div>
</div>
<div class="hidden-xs col-sm-1">
    <a href="javascript:;" class="owl-next-par">
        <img src="{{asset("imgs/right-icon.png?c=".date('YmdHis'))}}">
    </a>
</div>
