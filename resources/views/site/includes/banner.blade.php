<!-- INÍCIO BANNERS -->
@if(@$banners !== null)
    <section>
        <div class="container-fluid">
            <div class="row">
                <div id="carousel-example-generic" class="carousel slide carousel-fade" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        @foreach ($banners as $banner)
                            <div class="item {{ ($banners[0]->BanCodigo == $banner->BanCodigo) ? "active":"" }}">
                                <a href="{{($banner->BanLink != "") ? $banner->BanLink : "javascript:;"}}">
                                    <img src="{{asset("upload/banners/".$banner->BanCodigo.".jpg")}}" class="banners"
                                         style="width: 100%;height: 500px;">
                                </a>
                            </div>
                        @endforeach
                    </div>
                @if(count(@$banners) > 1)
                    <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <?php $x = 0;?>
                            @foreach ($banners as $banner)
                                <li data-target="#carousel-example-generic" data-slide-to="{{$x}}"
                                    class="{{ ($banners[0]->BanCodigo == $banner->BanCodigo) ? "active":"" }}"></li>
                                <?php $x++;?>
                            @endforeach
                        </ol>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic" role="button"
                           data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Anterior</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" role="button"
                           data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Próximo</span>
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endif
<!-- FIM BANNERS -->