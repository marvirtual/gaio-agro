{{--SELEÇÃO DE UNIDADES--}}
<div class="modal fade" id="modal-cidade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <form action="/seleciona-unidade" method="post">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <h3>UNIDADES DE ATENDIMENTO</h3>
                            <select class="form-control input-lg" name="id" id="id"
                                    style="background-color: #F3F3F3;border-radius:0px;border:0px;">
                                @if(sizeof(@$blocos['unidades']) > 0)
                                    @foreach(@$blocos['unidades'] as $unidadestop)
                                        <option value="{{$unidadestop->UniCodigo}}" {{($unidadestop->UniCodigo == Session('unidadecodigo')) ? "selected" : ""}}>
                                            {{$unidadestop->UniMunicipio}} {{$unidadestop->UniTipo == 0 ? '(CEREALISTA)' : ''}} - {{$unidadestop->UniTelefone}}
                                        </option>
                                    @endforeach
                                @endif
                            </select>
                            <br>
                            <button class="btn btn-success btn-lg" type="submit">Selecionar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{--INICIO NAVBAR--}}
<header>
    <section>
        <div class="container">
            <div class="row">
                {{--LOGOMARCA SM MD LG--}}
                <div class="col-lg-5 col-sm-4 hidden-xs">
                    <a href="/home">
                        <img src="{{asset("imgs/logomarca.png")}}"
                            style="margin-top:20px; margin-bottom: 20px;">
                    </a>
                </div>
                {{-- <div class="col-lg-3 col-sm-4">
                    <div class="text-center hidden-xs">
                        <a href="http://loja.gaioagro.com.br/" target="_blank" class="btn link-loja-gaio">
                            <i class="fa fa-shopping-basket"
                                style="padding-top: 2px; margin-right: 5px; float: left;"></i>
                            Acesse nossa loja
                        </a>
                    </div>

                    <div class="text-right visible-xs">
                        <a href="http://loja.gaioagro.com.br/" target="_blank" class="btn link-loja-gaio">
                            <i class="fa fa-shopping-basket"
                                style="padding-top: 2px; margin-right: 5px; float: left;"></i>
                            Acesse nossa loja
                        </a>
                    </div>
                </div> --}}
                <div class="col-lg-4 col-sm-4 local-top ">
                    <div class="row">
                        @if(Session::has('orcamento'))
                            <a class="btn btn-primary pull-right" href="/orcamento"><i
                                        class="fa fa-shopping-cart"
                                        aria-hidden="true"></i>&nbsp;{{@$blocos["carrinho"]}} Itens</a>
                        @endif
                            <a class="btn btn-primary btn-popup-cidade pull-right" href="javascript:;">
                                <b>Unidade: </b>&nbsp; &nbsp;
                                <i class="fa fa-map-marker"></i>&nbsp;{{Session('municipio')}} {{Session('tipo') == 0 ? '(CEREALISTA)' : ''}}
                                - {{Session('telefone')}}&nbsp;&nbsp;<i
                                        class="fa fa-angle-down"></i></a>
                    </div>
                </div>

                {{--LOGOMARCA XS--}}
                <div class="col-sm-12 visible-xs clearfix">
                    <a href="/home">
                        <img src="{{asset("imgs/logomarca.png")}}"
                            style="margin-top:10px; margin-bottom:10px;" class="center-block img-responsive">
                    </a>
                </div>
            </div>
        </div>
    </section>
    <div class="section" style="background-color: #374E64;">
        <div class="container">
            <div class="row">
                <nav id="navbar" class="navbar navbar-default navbar-static-top" role="navigation">
                    <div class="container">

                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse"
                                    data-target="#main-menu"
                                    aria-expanded="true">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            {{--<a class="navbar-brand hidden-sm hidden-xs" href="">--}}
                            {{--<img src="{{asset("imgs/logomarca.png")}}">--}}
                            {{--</a>--}}
                            <div class="navbar-brand visible-xs">
                                {{Session('telefone')}}
                            </div>
                        </div>

                        <div class="collapse navbar-collapse" id="main-menu">
                            <ul class="nav navbar-nav">
                                <li>
                                    <a href="/home">
                                        HOME
                                    </a>
                                </li>
                                <li class="dropdown">
                                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                                        INSTITUCIONAL
                                        <span class="fa fa-chevron-down pull-right"
                                            style="font-size:10px;padding-top:3px;"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="/institucional"><i
                                                        class="fa fa-angle-right "></i>&nbsp;QUEM SOMOS</a>
                                        </li>
                                        <li><a href="/area-atuacao"><i
                                                        class="fa fa-angle-right "></i>&nbsp;ÁREA DE ATUAÇÃO</a></li>
                                        <li><a href="/unidades"><i class="fa fa-angle-right "></i>&nbsp;UNIDADES DE
                                                ATENDIMENTO</a>
                                        </li>
                                        <li><a href="/galerias"><i
                                                        class="fa fa-angle-right "></i>&nbsp;GALERIAS DE FOTOS</a>
                                        </li>
                                        <li><a href="/programas-radio"><i
                                                        class="fa fa-angle-right "></i>&nbsp;PROGRAMAS DE RÁDIO</a>
                                        </li>
                                        <li><a href="/noticias"><i
                                                        class="fa fa-angle-right "></i>&nbsp;NOTÍCIAS</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                                        AGRONEGÓCIO
                                        <span class="fa fa-chevron-down pull-right"
                                            style="font-size:10px;padding-top:3px;"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        @if(@$blocos["departamentos"] !== null)
                                            @foreach($blocos["departamentos"] as $departamento)
                                                <li><a href="/produtos/{{$departamento->DepCodigo}}"><i
                                                                class="fa fa-angle-right "></i>&nbsp;{{$departamento->DepTitulo}}
                                                    </a>
                                                </li>
                                            @endforeach
                                        @endif
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                                        CEREAIS
                                        <span class="fa fa-chevron-down pull-right"
                                            style="font-size:10px;padding-top:3px;"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        @if(@$blocos["cerealistas"] !== null)
                                            @foreach($blocos["cerealistas"] as $cerealista)
                                                <li><a href="/cerealista/id/{{$cerealista->UniCodigo}}"><i
                                                                class="fa fa-angle-right "></i>&nbsp;{{$cerealista->UniMunicipio}}
                                                    </a>
                                                </li>
                                            @endforeach
                                        @endif
                                        <li><a href="/cotacoes-agricolas"><i
                                            class="fa fa-angle-right "></i>&nbsp;Cotações Agrícolas
                                            </a>
                                        </li>                                        
                                    </ul>
                                </li>

                                {{--<li class="dropdown">
                                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                                        ENERGIAS RENOVÁVEIS
                                        <span class="fa fa-chevron-down pull-right"
                                            style="font-size:10px;padding-top:3px;"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{url('/produtos/6')}}"><i
                                                        class="fa fa-angle-right "></i>&nbsp;ENERGIA FOTOVOLTAICA</a>
                                        </li>
                                        <li><a href="{{url('/produtos/5')}}"><i
                                                        class="fa fa-angle-right "></i>&nbsp;AQUECEDOR SOLAR</a></li>
                                    </ul>
                                </li>--}}
                                
                                <li class="dropdown">
                                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                                        FALE CONOSCO
                                        <span class="fa fa-chevron-down pull-right"
                                            style="font-size:10px;padding-top:3px;"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{url('/contato')}}">
                                                <i class="fa fa-angle-right "></i>&nbsp;CONTATO
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{url('/trabalhe-conosco')}}">
                                                <i class="fa fa-angle-right "></i>&nbsp;TRABALHE CONOSCO</a>
                                        </li>
                                        <li>
                                            <a href="{{url('/vagas-de-emprego')}}">
                                                <i class="fa fa-angle-right "></i>&nbsp;VAGAS DISPONÍVEIS</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>

                                </li>
                            </ul>
                            {{--REDES SOCIAIS--}}
                            <ul class="nav navbar-nav navbar-right hidden-xs">
                                <li>
                                    <a href="https://www.facebook.com/gaio.agronegocios"
                                        style="padding-top: 19px; padding-bottom: 18px; background-color: #F58934; border-left: 5px solid #FFFFFF; "
                                        target="_blank">
                                        <i class="fa fa-facebook" aria-hidden="true" style="font-size: 20px;"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.instagram.com/gaioagro/"
                                        style="padding-top: 19px; padding-bottom: 18px; background-color: #F58934;  border-right: 5px solid #FFFFFF; ">
                                        <i class="fa fa-instagram" aria-hidden="true" style="font-size: 20px;"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <!-- /.navbar-collapse -->
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>
{{--FIM NAVBAR--}}