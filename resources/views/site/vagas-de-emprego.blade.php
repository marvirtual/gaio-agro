@extends("site.templates.app")
@section("title","Vagas de Emprego")
@section("content")
    <section class="section-conteudo">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="titulo-paginas">
                        Vagas Disponíveis
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    @if(count(@$vagas) > 0)
                        @foreach($vagas as $vaga)
                            <div class="box-vaga" style="position: relative; border: 2px #ccc solid; margin-bottom: 15px; padding: 15px;">
                                <h2 style="margin-top: 0px;">{{$vaga->titulo}}</h2>
                                <p style="margin-bottom: 0px;">Cidade: {{$vaga->unidade}}</p>
                                <p>Publicada em: {{date("d/m/Y", strtotime($vaga->created_at))}}</p>

                                <a href="{{url("/descricao-vaga/".$vaga->id)}}" class="btn" style="position: absolute; right: 15px; top: 15px;background-color: #374E64 !important; color: #fff !important;">Detalhes</a>
                            </div>
                        @endforeach
                    @else
                        <h4 class="text-center">Não há vagas disponíveis no momento.</h4>
                    @endif
                </div>
                @include("site.includes.menu")
            </div>
        </div>
    </section>
@endsection