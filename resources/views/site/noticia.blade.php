@extends("site.templates.app")
@section("title","Notícias - ".@$noticia->NotResumo)
@section("metatags")
    <meta property="og:locale" content="pt_BR"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="{{@$noticia->NotTitulo}}"/>
    <meta property="og:description" content="Gaio Agronegócios"/>
    <meta property="og:url" content="http://gaioagro.com.br/noticia/id/{{@$noticia->NotCodigo}}"/>
    <meta property="og:site_name" content="Gaio Agronegócios"/>
    <meta property="article:publisher" content="https://facebook.com"/>
    <meta property="og:image" content="{{asset("upload/noticias/p_".@$noticia->NotCodigo.".jpg")}}"/>
@endsection
@section("content")
    <?php
    setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
    ?>
    <section class="section-conteudo">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="titulo-paginas">
                        Notícias
                    </div>
                </div>
            </div>
            {{-- EXIBE AS GALERIAS --}}
            <div class="row">
                <div class="col-sm-12">
                    <h3 style="margin-top: 0px;">{!! $noticia->NotTitulo !!}
                        <h3>
                            <h6>{{ utf8_encode(strftime('%A, %d de %B de %Y', strtotime($noticia->NotData)))}}
                                <h6>
                                    <h5>{!! $noticia->NotResumo !!}<h5>
                </div>
                <div class="col-sm-12">

                    @if($noticia->NotVideo == "")
                        @if(File::exists(public_path()."/upload/noticias/g_" . $noticia->NotCodigo . ".jpg"))
                            <img src="{{asset("upload/noticias/g_" . $noticia->NotCodigo . ".jpg")}}"
                                 title="{!! $noticia->NotLegendaFoto !!}"
                                 class="center-block img-responsive img-thumbnail" style="padding:5px;"
                                 data-toggle="tooltip">
                        @endif
                    @else
                    <!-- 16:9 aspect ratio -->
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://www.youtube.com/embed/{{$noticia->NotVideo}}"></iframe>
                        </div>
                    @endif
                </div>
                <div class="col-sm-12" style="margin-top: 20px">
                    {!! $noticia->NotConteudo !!}
                </div>
                @if($noticia->NotFonte != "")
                    <div class="col-sm-12">
                        <h6>Fonte: {!! $noticia->NotFonte !!}<h6>
                    </div>
                @endif
            </div>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <div class="clearfix">&nbsp;</div>
                    <a class="btn btn-primary btn-xs"
                       href="http://www.facebook.com/sharer.php?u=http://gaioagro.com.br/noticia/id/{{@$noticia->NotCodigo."?".date("YmdHis")}}"
                       title="Compartilhar via Facebook" target="_blank"><span><i class="fa fa-facebook-square"></i> Facebook</span></a>

                    {{--<a class="btn btn-info btn-xs"
                       href="http://twitter.com/share?url=http://gaioagro.com.br/noticia/id/{{@$noticia->NotCodigo."?".date("YmdHis")}}&amp;text=Gaio Agronegócios - {{$noticia->NotTitulo}}"
                       title="Compartilhar no Twitter" target="_blank"><span><i
                                    class="fa fa-twitter"></i> Twitter</span></a>--}}
                    <a class="btn btn-success btn-xs"
                       href="https://api.whatsapp.com/send?text=http://gaioagro.com.br/noticia/id/{{@$noticia->NotCodigo."?".date("YmdHis")}}"
                       title="Compartilhar no WhatsApp" target="_blank"><span><i class="fa fa-whatsapp"></i><span> WhatsApp</span></span></a>
                </div>
            </div>
        </div>

    </section>
@endsection