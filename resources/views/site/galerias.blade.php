@extends("site.templates.app")
@section("title","Galerias de Fotos")
@section("content")
    <?php
    setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
    ?>
    <section class="section-conteudo">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="titulo-paginas">
                        Galerias de Fotos
                    </div>
                </div>
            </div>
            {{-- EXIBE AS GALERIAS --}}
            <div class="row">
                @foreach($itens as $item)
                    <a href="/galeria/id/{{$item->GalCodigo}}">
                        <div class="col-sm-4 altura-fixa-auto2" style="margin-bottom: 5px;">
                            <img src="{{asset("upload/galerias/dest_" . $item->GalCodigo . ".jpg")}}"
                                 class="center-block img-responsive img-thumbnail"
                                 style="padding:5px; margin-bottom: 5px;">
                            <div class="font12a">{{utf8_encode(strftime('%A, %d de %B de %Y', strtotime($item->GalData)))}}</div>
                            <div class="font15c"><b>{{$item->GalTitulo}}</b></div>
                            <div class="font14a">{!!substr(strip_tags($item->GalResumo),0,70)!!}...</div>
                        </div>
                    </a>
                @endforeach
            </div>
            <div class="col-sm-12 text-center">
                {!! $itens->render() !!}
            </div>
        </div>

    </section>
@endsection