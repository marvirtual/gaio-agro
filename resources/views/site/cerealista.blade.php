@extends("site.templates.app")
@section("title","Cerealista - ".@$cerprincipal->UniMunicipio)
@section("content")
    {{-- INICIO MODAL --}}
    <div class="modal fade" id="mapaModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel"><span id="tituloModalForm"></span></h4>
                </div>
                <div class="modal-body" style="background-color: #f7f7f7; padding-top: 20px;padding-bottom: 20px;">
                    <span id="localizacaoModalForm"></span>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>
    {{-- FIM MODAL --}}
    <section class="section-conteudo">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="titulo-paginas">
                        Cerealistas
                    </div>
                </div>
            </div>
            <div class="row">
                {{--COLUNA DE CONTEUDO--}}
                <div class="col-sm-12">
                    {{--CEREALISTA SELECIONADA--}}
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="titulos-cerealistas">{{@$cerprincipal->UniMunicipio}}</div>
                            <div class="conteudo-cerealistas">{{@$cerprincipal->UniEndereco}}<br></div>
                            <div class="conteudo-cerealistas"><b>{{@$cerprincipal->UniTelefone}}</b></div>
                            @if($cerprincipal->UniMapa != "")
                                <a href="#" class="btn btn-mapa-modal"
                                   data-title="{{(@$cerprincipal->UniTipo == 1) ? "Loja" : "Cerealista"}} - {{@$cerprincipal->UniMunicipio}}"
                                   data-localizacao="{{@$cerprincipal->UniMapa}}"
                                   style="color: #FFFFFF; background-color: #F58934; margin-top: 10px;"><i
                                            class="fa fa-map-marker" aria-hidden="true"
                                            style="color: #FFFFFF;"></i>&nbsp;&nbsp;VER LOCALIZAÇÃO</a>
                            @endif
                        </div>
                    </div>
                    @if(@$pagprincipal !== null)
                        <div class="row">
                            <div class="col-xs-12 font15c text-justify" style="margin-bottom: 15px; margin-top: 15px;">
                                {!! $pagprincipal->PagConteudo !!}
                            </div>
                        </div>
                        <div class="row light-gallery">
                            @if(@$destprincipal == "1")
                                <div class="col-xs-4 col-sm-2 altura-fixa-auto"
                                     data-src="{{asset("upload/paginas/dest_" . $pagprincipal->PagCodigo . ".jpg")}}"
                                     data-sub-html="{{$pagprincipal->PagTitulo}}">
                                    <img src="{{asset("upload/paginas/dest_" . $pagprincipal->PagCodigo . ".jpg")}}"
                                         class="center-block img-responsive img-thumbnail" style="padding:5px;">
                                </div>
                            @endif
                            @foreach(@$fotosprincipal as $foto)
                                <div class="col-xs-4 col-sm-2 altura-fixa-auto"
                                     data-src="{{asset("upload/paginas/g_" . $foto->FotCodigo . ".jpg")}}"
                                     data-sub-html="{{$foto->FotLegenda}}">
                                    <img src="{{asset("upload/paginas/p_" . $foto->FotCodigo . ".jpg")}}"
                                         class="center-block img-thumbnail img-responsive">
                                </div>
                            @endforeach
                        </div>
                    @endif

                    {{--DEMAIS CEREALISTAS--}}
                    @if(@$cerealistas !== null)
                        @foreach(@$cerealistas as $cerealista)
                            <div class="row" style="margin-top: 40px;">
                                <div class="col-xs-12">
                                    <div class="titulos-cerealistas">{{@$cerealista->UniMunicipio}}</div>
                                    <div class="conteudo-cerealistas">{{@$cerealista->UniEndereco}}<br></div>
                                    <div class="conteudo-cerealistas"><b>{{@$cerealista->UniTelefone}}</b></div>
                                    @if($cerealista->UniMapa != "")
                                        <a href="#" class="btn btn-mapa-modal"
                                           data-title="{{(@$cerealista->UniTipo == 1) ? "Loja" : "Cerealista"}} - {{@$cerealista->UniMunicipio}}"
                                           data-localizacao="{{@$cerealista->UniMapa}}"
                                           style="color: #FFFFFF; background-color: #F58934; margin-top: 10px;"><i
                                                    class="fa fa-map-marker" aria-hidden="true"
                                                    style="color: #FFFFFF;"></i>&nbsp;&nbsp;VER LOCALIZAÇÃO</a>
                                    @endif
                                </div>
                            </div>
                            @if(@$cerealista->paginas !== null)
                                <div class="row">
                                    <div class="col-xs-12 font15c text-justify"
                                         style="margin-bottom: 15px; margin-top: 15px;">
                                        {!! $cerealista->paginas->PagConteudo !!}
                                    </div>
                                </div>
                                <div class="row light-gallery">
                                    @if(@$cerealista->destaque == "1")
                                        <div class="col-xs-4 col-sm-2 altura-fixa-auto"
                                             data-src="{{asset("upload/paginas/dest_" . $cerealista->paginas->PagCodigo . ".jpg")}}"
                                             data-sub-html="{{$cerealista->paginas->PagTitulo}}">
                                            <img src="{{asset("upload/paginas/dest_" . $cerealista->paginas->PagCodigo . ".jpg")}}"
                                                 class="center-block img-responsive img-thumbnail" style="padding:5px;">
                                        </div>
                                    @endif
                                    @foreach(@$cerealista->fotos as $foto)
                                        <div class="col-xs-4 col-sm-2 altura-fixa-auto"
                                             data-src="{{asset("upload/paginas/g_" . $foto->FotCodigo . ".jpg")}}"
                                             data-sub-html="{{$foto->FotLegenda}}">
                                            <img src="{{asset("upload/paginas/p_" . $foto->FotCodigo . ".jpg")}}"
                                                 class="center-block img-thumbnail img-responsive">
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                        @endforeach
                    @endif

                </div>
            </div>

        </div>

    </section>
@endsection