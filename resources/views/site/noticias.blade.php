@extends("site.templates.app")
@section("title","Notícias")
@section("content")
    <?php
    setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
    ?>
    <section class="section-conteudo">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="titulo-paginas">
                        Notícias
                    </div>
                </div>
            </div>
            {{-- EXIBE AS NOTICIAS --}}
            @foreach (@$noticias as $noticia)

                <?php
                if(strlen($noticia->NotLinkFoto) > 10){
                    $url_foto = asset("imgs/sem-foto.jpg");
                }else{
                    $url_foto = asset("upload/noticias/p_".$noticia->NotCodigo.".jpg");
                }
                ?>

                @if($noticia->NotLink == "")
                    <a href="/noticia/id/{{$noticia->NotCodigo}}">
                        <div class="row">
                            <div class="col-sm-2">
                                @if($noticia->NotVideo == "")
                                    <img src="{{asset("upload/noticias/p_" . $noticia->NotCodigo . ".jpg")}}"
                                         class="center-block img-thumbnail img-responsive">
                                @else
                                    <img src="http://img.youtube.com/vi/{{$noticia->NotVideo}}/mqdefault.jpg"
                                         class="center-block img-thumbnail img-responsive">
                                @endif

                            </div>
                            <div class="col-sm-10">
                                <div class="font12a">{{ utf8_encode(strftime('%A, %d de %B de %Y', strtotime($noticia->NotData)))}}</div>
                                <div class="font20a">Gaio - {{$noticia->NotTitulo}}</div>
                                <div class="font14a">{{$noticia->NotResumo}}</div>
                            </div>
                        </div>
                    </a>
                    <div class="clearfix">&nbsp;</div>
                @else
                    <a href="{{$noticia->NotLink}}" target="_blank">
                        <div class="row">
                            <div class="col-sm-2">
                                <img src="{{$url_foto}}"
                                     class="center-block img-thumbnail img-responsive">
                            </div>
                            <div class="col-sm-10">
                                <div class="font12a">{{ utf8_encode(strftime('%A, %d de %B de %Y', strtotime($noticia->NotData)))}}</div>
                                <div class="font20a">{{$noticia->NotTitulo}}</div>
                                <div class="font14a">{{strip_tags($noticia->NotResumo)}}</div>
                            </div>
                        </div>
                    </a>
                    <div class="clearfix">&nbsp;</div>
                @endif
            @endforeach
            <div class="col-sm-12 text-center">
                {!! $noticias->render() !!}
            </div>
        </div>

    </section>
@endsection