@extends("site.templates.app")
@section("title","Servicos")

@section("content")
        <!--INICIO PRODUTOS-->
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2 col-lg-6 col-lg-offset-3">
                <div class="row" style="margin-top: 15px;">
                    <div class="col-xs-6 col-xs-offset-3"
                         style="border-top: 2px solid #333333; padding-bottom: 20px;"></div>
                </div>
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <p class="font25b text-uppercase">SERVIÇOS</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="padding-bottom: 15px;margin-top:15px;">
            <div class="col-sm-12">
                @if(@$servicos !== null)
                    @foreach($servicos as $servico)
                        <figure class="col-sm-4 altura-fixa-auto">

                            <a href="produtos/id/{{$servico->SerCodigo}}">
                                <img src="{{asset("upload/servicos/dest_".$servico->SerCodigo.".jpg")}}"
                                     style="min-height: 150px;width: 100%"
                                     class="img-responsive img-thumbnail">
                            </a>

                            <!-- Item Hover Options -->
                            <figcaption style="min-height:50px;" class="text-center">
                                <a href="servicos/id/{{$servico->SerCodigo}}">
                                    <h3 style="color: #666666;">{{$servico->SerTitulo}}</h3>
                                </a>
                                <a href="servicos/id/{{$servico->SerCodigo}}" class="btn btn-danger">
                                    ver detalhes
                                </a>
                            </figcaption>

                        </figure>
                    @endforeach
                @endif
            </div>
            <div class="col-sm-12 text-center">
                {!! $servicos->render() !!}
            </div>
        </div>
    </div>
</section>
@endsection