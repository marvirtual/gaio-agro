@extends("site.templates.app")
@section("title","Página Inicial")
@section("content")
    <?php
    setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
    ?>
    <section class="section-conteudo">
        <div class="container">
            <div class="row">
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
                <div class="col-xs-12">
                    <div class="titulo-paginas">
                        ÚLTIMAS NOTÍCIAS
                    </div>
                </div>
                <div class="col-sm-12 widgets_noticias">
                    <script type="text/javascript" src="//www.noticiasagricolas.com.br/widget/noticias.js.php?subsecao=2,3,6,7,8,10,64,80,85,5,4,11,12,40,60,13,97,14,95,15,1,84,28,99,26,69,90,62,27,92,132,76&largura=100%&altura=250px&fonte=Tahoma&tamanho=10pt&cortexto=333333&corlink=006666&qtd=10&output=js"></script>
                </div>
                <div class="col-xs-12">
                    {{-- EXIBE AS NOTICIAS--}}
                    <div class="row">
                        @foreach (@$noticias as $noticia)
                            @include("site.includes.noticias.noticia")
                            @include("site.includes.noticias.noticia-xs")
                        @endforeach

                        @foreach (@$noticiasexternas as $noticia)
                            @include("site.includes.noticias.noticia")
                            @include("site.includes.noticias.noticia-xs")
                        @endforeach
                    </div>

                    <div class="col-sm-3 col-sm-offset-9">
                        <a href="/noticias" class="btn btn-sm cor-botoes center-block">outras notícias</a>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <section class="section-externos">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <a href="/cerealista/id/7"><img
                                src="{{asset("imgs/icon-cotacoes.png")}}" class="img-responsive center-block"/></a>
                </div>
                <div class="clearfix visible-xs">&nbsp;</div>
                <div class="col-sm-4">
                    <a href="/previsao"><img
                                src="{{asset("imgs/icon-previsao.png")}}" class="img-responsive center-block"/></a>
                </div>
                <div class="clearfix visible-xs">&nbsp;</div>
                <div class="col-sm-4">
                    <a href="http://www.debit.com.br/consulta10.php" target="_blank"><img
                                src="{{asset("imgs/icon-indicadores.png")}}" class="img-responsive center-block"/></a>
                </div>
            </div>
        </div>
    </section>
    <section class="section-carousel">
        <div class="container">
            {{--            <div class="row row-conteudo">--}}
            {{--                @include("site.includes.depoimentos")--}}
            {{--            </div>--}}
            <div class="row row-conteudo row-parceiros">
                @include("site.includes.parceiros")
            </div>
            <div class="row row-conteudo">
                <div class="col-sm-12 texto-news">
                    <b>Fique por dentro das novidades</b> <br>
                    <a href="javascript:;" data-toggle="modal" data-target="#modal-news">Clique aqui</a> e cadastre-se
                    em nossa newsletter!
                </div>
            </div>
        </div>
        <img src="{{asset('imgs/porteira-left.png')}}" class="img-responsive porteira-left hidden-sm hidden-xs">
        <img src="{{asset('imgs/porteira-right.png')}}" class="img-responsive porteira-right hidden-sm hidden-xs">
    </section>
@endsection