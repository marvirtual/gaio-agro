@extends("site.templates.app")
@section("title",$pagina->PagTitulo)
@section("content")
    <section class="section-conteudo">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="titulo-paginas">
                        {{$pagina->PagTitulo}}
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                @if(($destaque == "1") or ($fotos !== null))
                    <div class="col-sm-4">
                        <div class="row light-gallery">
                            @if($destaque == "1")
                                <div class="col-sm-12"
                                     data-src="{{asset("upload/paginas/dest_" . $pagina->PagCodigo . ".jpg")}}"
                                     data-sub-html="{{"<b>".$pagina->PagTitulo."</b><br>".$pagina->PagResumo}}">
                                    <img src="{{asset("upload/paginas/dest_" . $pagina->PagCodigo . ".jpg")}}"
                                         class="center-block img-responsive img-thumbnail" style="padding:5px;">
                                </div>
                            @endif
                            @foreach($fotos as $foto)
                                <div class="col-sm-4 col-xs-4 altura-fixa-auto" style="padding-top:5px; padding-bottom:5px;"
                                     data-src="{{asset("upload/paginas/g_" . $foto->FotCodigo . ".jpg")}}"
                                     data-sub-html="{{$foto->FotLegenda}}">
                                    <img src="{{asset("upload/paginas/p_" . $foto->FotCodigo . ".jpg")}}"
                                         class="center-block img-thumbnail img-responsive">
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif
                <div class="{{(($destaque == "1") and ($fotos != null)) ? "col-sm-8" : "col-sm-12"}} font15c text-justify">
                    {!! $pagina->PagConteudo !!}
                </div>
            </div>
        </div>
    </section>
@endsection