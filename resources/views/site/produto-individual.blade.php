@extends("site.templates.app")
@section("title",$produto->ProTitulo)
@section("metatags")
    <meta property="og:locale" content="pt_BR"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="{{@$produto->ProTitulo}}"/>
    <meta property="og:description" content="Gaio Agronegócios"/>
    <meta property="og:url" content="http://gaioagro.com.br/produto/id/{{@$produto->ProCodigo}}"/>
    <meta property="og:site_name" content="Gaio Agronegócios"/>
    <meta property="article:publisher" content="https://facebook.com"/>
    <meta property="og:image" content="{{asset("upload/produtos/dest_".@$produto->ProCodigo.".jpg")}}"/>
@endsection
@section("content")
    <!--INICIO PRODUTO-->
    <section class="section-conteudo">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="titulo-paginas">
                        {{$produto->ProTitulo}}
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row" style="padding-bottom: 15px;margin-top:15px;">
                <div class="col-sm-9">
                    @if(file_exists(public_path('upload/produtos/arte_'.$produto->ProCodigo.'.jpg')))
                        <div class="row" style="margin-bottom: 20px;">
                            <div class="col-xs-12">
                                <img src="{{asset('upload/produtos/arte_'.$produto->ProCodigo.'.jpg')}}"
                                     class="img-responsive center-block"/>
                            </div>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="row light-gallery">
                                @if($destaque == "1")
                                    <div class="col-sm-12"
                                         data-src="{{asset("upload/produtos/dest_" . $produto->ProCodigo . ".jpg")}}"
                                         data-sub-html="{{$produto->ProTitulo}}">
                                        <img src="{{asset("upload/produtos/dest_" . $produto->ProCodigo . ".jpg")}}"
                                             class="center-block img-responsive img-thumbnail" style="padding:5px;">
                                    </div>
                                @endif
                                @foreach($fotos as $foto)
                                    <div class="col-xs-3 col-sm-3 altura-fixa-auto"
                                         style="padding-top:5px;padding-bottom:5px;"
                                         data-src="{{asset("upload/produtos/g_" . $foto->FotCodigo . ".jpg")}}"
                                         data-sub-html="{{$foto->FotLegenda}}">
                                        <img src="{{asset("upload/produtos/p_" . $foto->FotCodigo . ".jpg")}}"
                                             class="center-block img-thumbnail img-responsive">
                                    </div>
                                @endforeach

                            </div>
                        </div>
                        <div class="col-sm-6 font15c">
                            @if($produto->ProValor != "0.00")
                                <h2 style="margin-top: 5px; margin-bottom: 5px;">
                                    R$ {{number_format($produto->ProValor,2,',','')}}</h2>
                            @endif
                            <a href="/insert-orcamento/id/{{$produto->ProCodigo}}" class="btn btn-lg cor-botoes">Solicitar
                                Orçamento</a>

                            {!! $produto->ProConteudo !!}

                            <h4 style="margin-top: 5px; margin-bottom: 5px;">
                                Marca {{$produto->MarTitulo}}</h4>
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 20px;">
                        <div class="col-xs-12 text-center">
                            <div class="clearfix">&nbsp;</div>
                            <a class="btn btn-primary btn-xs"
                               href="http://www.facebook.com/sharer.php?u=http://gaioagro.com.br/produto/id/{{@$produto->ProCodigo."?".date("YmdHis")}}"
                               title="Compartilhar via Facebook" target="_blank"><span><i
                                            class="fa fa-facebook-square"></i> Facebook</span></a>
                            <a class="btn btn-info btn-xs"
                               href="http://twitter.com/share?url=http://gaioagro.com.br/produto/id/{{@$produto->ProCodigo."?".date("YmdHis")}}&amp;text=Gaio Agronegócios - {{@$produto->ProTitulo}}"
                               title="Compartilhar no Twitter" target="_blank"><span><i
                                            class="fa fa-twitter"></i> Twitter</span></a>
                            <a class="btn btn-success btn-xs"
                               href="https://api.whatsapp.com/send?text=http://gaioagro.com.br/produto/id/{{@$produto->ProCodigo."?".date("YmdHis")}}"
                               title="Compartilhar no WhatsApp" target="_blank"><span><i
                                            class="fa fa-whatsapp"></i><span> WhatsApp</span></span></a>
                        </div>
                    </div>
                </div>
                @include("site.includes.menu")
            </div>
        </div>
    </section>
@endsection