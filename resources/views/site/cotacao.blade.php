@extends("site.templates.app")
@section("title","Cerealista - ".@$cerprincipal->UniMunicipio)
@section("content")
    {{-- INICIO MODAL --}}
    <div class="modal fade" id="mapaModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel"><span id="tituloModalForm"></span></h4>
                </div>
                <div class="modal-body" style="background-color: #f7f7f7; padding-top: 20px;padding-bottom: 20px;">
                    <span id="localizacaoModalForm"></span>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>
    {{-- FIM MODAL --}}
    <section class="section-conteudo">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="titulo-paginas">
                        Cotações Agrícolas
                    </div>
                </div>
            </div>
            <div class="row">
                {{--COLUNA DE COTAÇÕES--}}
                <div class="col-sm-12">
                    <div class="clearfix visible-xs">&nbsp;</div>
                    <div class="row">
                        {{--COTAÇÕES GAIO--}}
                        <div class="col-xs-12 table-responsive">
                            <div style="background: #DCE7E9; padding: 10px;"><h5 style="margin: 0px;"
                                class="text-center"><b style="font-size: 23pt; color: #1e6d42">Cotações Gaio Agronegócios</b></h5>
                            </div>
                            <table class="table table-striped table-hover" style="width: 100%; font-size: 20pt; margin-bottom: 0">
                                <thead>
                                <tr style="background: #ccd6d8;">
                                    <th class="col-xs-4" style="color: #1e6d42">#</th>
                                    <th class="col-xs-4 text-center" style="color: #1e6d42">Compra</th>
                                    <th class="col-xs-4 text-center" style="color: #1e6d42">Data</th>
                                    {{--<th class="col-xs-4">Venda</th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td style="vertical-align: middle; color: #1e6d42;">
                                        <b>Milho</b></td>
                                    <td style="vertical-align: middle; color: #1e6d42" class="text-center">
                                        R$ {{number_format(@$cotacao->CotMilhoCompra,2,',','')}}</td>
                                    <td style="vertical-align: middle; color: #1e6d42" class="text-center">
                                        {{date("d/m/Y",strtotime(@$cotacao->CotData))}}</td>


                                    {{--<td style="vertical-align: middle;">--}}
                                        {{--R$ {{number_format($cotacao->CotMilhoVenda,2,',','')}}</td>--}}
                                </tr>
                                <tr style="background-color: #f9f9f9">
                                    <td style="vertical-align: middle; color: #1e6d42;">
                                        <b>Soja</b></td>
                                    <td style="vertical-align: middle; color: #1e6d42;" class="text-center">
                                        R$ {{number_format(@$cotacao->CotSojaCompra,2,',','')}}</td>
                                    <td style="vertical-align: middle; color: #1e6d42" class="text-center">
                                        {{date("d/m/Y",strtotime(@$cotacao->CotData))}}</td>
                                    {{--<td style="vertical-align: middle;">--}}
                                        {{--R$ {{number_format($cotacao->CotSojaVenda,2,',','')}}</td>--}}
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle; color: #1e6d42;">
                                        <b>Trigo PH {{floatval(@$cotacao->CotTrigoPH)}}</b></td>
                                    <td style="vertical-align: middle; color: #1e6d42;" class="text-center">
                                        R$ {{number_format(@$cotacao->CotTrigoCompra,2,',','')}}</td>
                                    <td style="vertical-align: middle; color: #1e6d42" class="text-center">
                                        {{date("d/m/Y",strtotime(@$cotacao->CotData))}}</td>
                                    {{--<td style="vertical-align: middle;">--}}
                                        {{--R$ {{number_format($cotacao->CotTrigoVenda,2,',','')}}</td>--}}
                                </tr>
                                </tbody>
                            </table>
                            <div style="background: #f9f9f9; color: #F58934; font-size: 20pt; text-align: center">
                                Faturamento de segunda a sexta-feira das 11H às 12H - 14H às 15H 
                            </div>
                            <div style="background: #f9f9f9;">
                                <img src="imgs/logomarca.png" style="width: 75px">
                            </div>
                        </div>
                        {{--PREÇO DO DOLLAR - NOTICIAS AGRICOLAS--}}
                        <div class="col-xs-12" style="margin-top: 20px; font-size: 12pt !important">
                            <!-- Widgets Notícias Agrícolas - www.noticiasagricolas.com.br/widgets -->
                            <script type="text/javascript" src="http://www.noticiasagricolas.com.br/widget/cotacoes.js.php?id=114&fonte=Arial%2C%20Helvetica%2C%20sans-serif&tamanho=15pt&largura=100%&cortexto=333333&corcabecalho=B2C3C6&corlinha=DCE7E9&imagem=true&output=js"></script>
                        </div>
                        {{--COTAÇÃO MILHO - NOTICIAS AGRICOLAS--}}
                        <div class="col-xs-12" style="margin-top: 20px;">
                            <!-- Widgets Notícias Agrícolas - www.noticiasagricolas.com.br/widgets -->
                            <script type="text/javascript"
                                    src="http://www.noticiasagricolas.com.br/widget/cotacoes.js.php?id=93&fonte=Arial%2C%20Helvetica%2C%20sans-serif&tamanho=15pt&largura=100%&cortexto=333333&corcabecalho=B2C3C6&corlinha=DCE7E9&imagem=false&output=js"></script>
                        </div>
                        {{--COTAÇÃO TRIGO - NOTICIAS AGRICOLAS--}}
                        <div class="col-xs-12" style="margin-top: 20px;">
                            <!-- Widgets Notícias Agrícolas - www.noticiasagricolas.com.br/widgets -->
                            <script type="text/javascript"
                                    src="http://www.noticiasagricolas.com.br/widget/cotacoes.js.php?id=143&fonte=Arial%2C%20Helvetica%2C%20sans-serif&tamanho=15pt&largura=100%&cortexto=333333&corcabecalho=B2C3C6&corlinha=DCE7E9&imagem=false&output=js"></script>
                        </div>
                        {{--COTAÇÃO SOJA - NOTICIAS AGRICOLAS--}}
                        <div class="col-xs-12" style="margin-top: 15px;">
                            <!-- Widgets Notícias Agrícolas - www.noticiasagricolas.com.br/widgets -->
                            <script type="text/javascript" src="http://www.noticiasagricolas.com.br/widget/cotacoes.js.php?id=23&fonte=Arial%2C%20Helvetica%2C%20sans-serif&tamanho=14pt&largura=100%&cortexto=333333&corcabecalho=B2C3C6&corlinha=DCE7E9&imagem=false&output=js"></script>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>
@endsection