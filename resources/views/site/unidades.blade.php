@extends("site.templates.app")
@section("title","Unidades de Atendimento")
@section("content")
    {{-- INICIO MODAL --}}
    <div class="modal fade" id="mapaModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel"><span id="tituloModalForm"></span></h4>
                </div>
                <div class="modal-body" style="background-color: #f7f7f7; padding-top: 20px;padding-bottom: 20px;">
                    <span id="localizacaoModalForm"></span>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>
    {{-- FIM MODAL --}}

    <section class="section-conteudo">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="titulo-paginas">
                        Unidades de Atendimento
                    </div>
                </div>
            </div>
            <div class="row">
                @if(sizeof(@$unidades) > 0)
                    @foreach(@$unidades as $unidade)
                        <div class="col-sm-4 altura-fixa-auto" style="margin-bottom: 15px;">
                            <div class="conteudo-cerealistas">
                                <b>{{(@$unidade->UniTipo == 1) ? "Loja" : 
                                    ((@$unidade->UniTipo == 2) ? "Gaio Solar" :
                                    ((@$unidade->UniTipo == 3) ? "Fábrica de farelo de Soja" : "Cerealista"))}}</b></div>
                            <div class="titulos-cerealistas">{{@$unidade->UniMunicipio}}</div>
                            <div class="conteudo-cerealistas">{{@$unidade->UniEndereco}}<br></div>
                            <div class="conteudo-cerealistas"><b>{{@$unidade->UniTelefone}}</b></div>
                            @if($unidade->UniMapa != "")
                                <a href="#" class="btn btn-mapa-modal"
                                   data-title="{{(@$unidade->UniTipo == 1) ? "Loja" : 
                                   ((@$unidade->UniTipo == 2) ? "Gaio Solar" :
                                   ((@$unidade->UniTipo == 3) ? "Fábrica de farelo de Soja" : "Cerealista"))}} - {{@$unidade->UniMunicipio}}"
                                   data-localizacao="{{@$unidade->UniMapa}}"
                                   style="color: #FFFFFF; background-color: #F58934; margin-top: 10px;"><i
                                            class="fa fa-map-marker" aria-hidden="true"
                                            style="color: #FFFFFF;"></i>&nbsp;&nbsp;VER LOCALIZAÇÃO</a>
                            @endif
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </section>
@endsection