@extends("site.templates.app")
@section("title","Orçamento")
@section("content")
    <section class="section-conteudo">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="titulo-paginas">
                        Orçamento
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-xs-12">
                            <h4>Passo 1 - Adicionar Produtos</h4>
                        </div>
                        <div class="col-sm-12 table-responsive">
                            <table class="table table-striped table-hover" style="width: 100%">
                                <thead>
                                <tr>
                                    <th class="col-sm-1 text-center">#</th>
                                    <th class="col-sm-8">Produto</th>
                                    <th class="col-xs-4 col-sm-1 text-center">Qtde</th>
                                    <th class="col-sm-1 text-center">#</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($itens as $item)
                                    <tr>
                                        <td style="vertical-align: middle;"><img src="{{asset("upload/produtos/dest_".$item->ProCodigo.".jpg")}}" width="100%"/></td>
                                        <td style="vertical-align: middle;">{{$item->ProTitulo}}</td>
                                        <td class="text-center" style="vertical-align: middle;">
                                            <input type="text" class="form-control qtde"
                                                   width="20" maxlength="4"
                                                   value="{{$item->IteQtde}}"
                                                   name="qtde" id="qtde"
                                                   data-id="{{$item->IteCodigo}}"><span
                                                    data-id="{{$item->IteCodigo}}" style="display: none;"
                                                    class="btn btn-lg btn-success label label-success">Ok</span>
                                        </td>
                                        <td class="text-center" style="vertical-align: middle;">
                                            <a href="/destroy-orcamento/id/{{$item->IteCodigo}}"
                                               class="btn btn-danger btn-sm btn-destroy"><i class="fa fa-close"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                @if($itens->count() < 1)
                                    <tr>
                                        <td class="text-center" colspan="3">
                                            Nenhum Registro Encontrado
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                            <a href="/produtos" class="btn btn-md cor-botoes pull-right">Adicionar mais Produtos</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <h4>Passo 2 - Preencher seus dados</h4>
                        </div>
                        <div class="col-sm-8 col-sm-offset-2">
                            <form action="/finaliza-orcamento" method="POST" enctype="multipart/form-data" id="formOrcamento" class="form-horizontal"
                                  style="margin-top: 20px;">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <input type="text" class="form-control form-orcamento" id="nome" name="nome"
                                               placeholder="Nome"
                                               required maxlength="150">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-9 col-sm-10">
                                        <input type="text" class="form-control form-orcamento" id="municipio"
                                               name="municipio"
                                               placeholder="Município"
                                               required maxlength="60">
                                    </div>
                                    <div class="col-xs-3 col-sm-2" style="padding-left: 0px !important;">
                                        <input type="text" class="form-control form-orcamento" id="estado" name="estado"
                                               placeholder="UF"
                                               required maxlength="2" style="text-transform: uppercase;">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <input type="text" class="form-control form-orcamento" name="endereco"
                                               id="endereco"
                                               placeholder="Endereço"
                                               required maxlength="100">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <input type="text" class="form-control form-orcamento telefone" name="telefone"
                                               id="telefone"
                                               placeholder="Telefone"
                                               required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <input type="email" class="form-control form-orcamento" name="email" id="email"
                                               placeholder="E-mail"
                                               required maxlength="150">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                            <textarea class="form-control form-orcamento" name="observacoes" id="observacoes"
                                      placeholder="Observações"
                                      rows="5"></textarea>
                                    </div>
                                </div>
                                @if(@$contaCopel == 1)
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                           Faça o envio de uma Fatura da COPEL: (Formatos suportados: PDF, JPG e PNG) <br>
                                            <input required type="file" accept=".pdf,.jpg,.png" class="form-control form-orcamento" name="file" id="file">
                                        </div>
                                    </div>
                                @endif
                                <div class="form-group">
                                    <div class="col-xs-12 text-center">
                                        <button type="submit" id="botaoOrcamento" class="btn btn-lg cor-botoes">
                                            SOLICITAR ORÇAMENTO
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                @include("site.includes.menu")
            </div>
        </div>
    </section>
@endsection