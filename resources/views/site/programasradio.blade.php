@extends("site.templates.app")
@section("title","Programas de Rádio")
@section("content")
    <section class="section-conteudo">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="titulo-paginas">
                        Programas de Rádio
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    @if(count(@$programas) > 0)
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>Data</th>
                                    <th>Ouça a Gravação</th>
                                    <th>Efetuar Download</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($programas as $programa)
                                    <tr>
                                        <td class="col-sm-2"
                                            style="vertical-align: middle;">{{date("d/m/Y",strtotime($programa->ProData))}}</td>
                                        <td class="col-sm-8">
                                            <audio controls="controls" preload="none"
                                                   src="{{asset('upload/programasradio/'.$programa->ProArquivo)}}"
                                                   style="max-width: 100%;">
                                                Seu navegador não suporta áudio HTML5.
                                            </audio>
                                        </td>
                                        <td class="col-sm-2">
                                            @if(File::exists(public_path()."/upload/programasradio/".$programa->ProArquivo))
                                                <a
                                                        href="/programa-download/id/{{$programa->ProCodigo}}"
                                                        class="btn btn-primary btn-sm center-block" target="_blank"><i
                                                            class="fa fa-cloud-download"></i> Download</a>
                                            @else
                                                Inexistente
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection