@extends("site.templates.app")
@section("title","Contato")
@section("content")
    <section class="section-conteudo">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="titulo-paginas">
                        Contato
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4 class="text-center">Entre em contato conosco preenchendo o formulário abaixo.</h4>
                        </div>
                        <div class="col-sm-8 col-sm-offset-2">
                            @if (session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                            @endif
                            @if (session('error'))
                                <div class="alert alert-danger">
                                    {{ session('error') }}
                                </div>
                            @endif
                            <form action="{{url('/contato')}}" method="POST" id="form-enviar-btn" class="form-horizontal" style="margin-top: 20px;">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <input type="text" class="form-control form-orcamento" id="nome" name="nome"
                                               placeholder="Nome"
                                               required maxlength="150">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <input type="text" class="form-control form-orcamento telefone" name="telefone"
                                               id="telefone"
                                               placeholder="Telefone"
                                               required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <input type="email" class="form-control form-orcamento" name="email" id="email"
                                               placeholder="E-mail"
                                               required maxlength="150">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                         <textarea class="form-control form-orcamento" name="mensagem" id="mensagem"
                                                   placeholder="Mensagem"
                                                   rows="5"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <div class="box-recaptcha">
                                            <div class="g-recaptcha" data-sitekey="6LcA2qIUAAAAAMKXPFxk3EK6F_NAW6trkoOTlVxK"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12 text-center">
                                        <button type="submit" class="btn btn-lg cor-botoes" id="btn-enviar-form">
                                            SOLICITAR ORÇAMENTO
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                @include("site.includes.menu")
            </div>
        </div>
    </section>
@endsection