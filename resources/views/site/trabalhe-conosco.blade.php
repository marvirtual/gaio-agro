@extends("site.templates.app")
@section("title","Trabalhe Conosco")
@section("content")
    <section class="section-conteudo">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="titulo-paginas">
                        Trabalhe Conosco
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <div class="row" style="margin-bottom: 20px;">
                        <div class="col-xs-12 text-center">
                            Cadastre-se seu currículo em nosso banco de dados e em breve faremos uma análise dos dados!
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2">
                            @if (session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                            @endif
                            @if (session('error'))
                                <div class="alert alert-danger">
                                    {{ session('error') }}
                                </div>
                            @endif
                            <form action="/trabalhe-conosco" method="POST" class="form-horizontal" id="form-enviar-btn" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <input type="text" class="form-control form-orcamento" id="nome" name="nome"
                                               placeholder="Nome"
                                               required maxlength="100">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-5">
                                        <input type="text" class="form-control form-orcamento datanascimento"
                                               id="datanascimento"
                                               name="datanascimento"
                                               placeholder="Data Nascimento"
                                               required maxlength="20">
                                    </div>
                                    <div class="col-xs-7" style="padding-left: 0px !important; padding-top: 5px;">
                                        <div class="radio-inline">
                                            <input type="radio" name="sexo" id="sexo" value="Masculino"
                                                   checked>Masculino
                                        </div>
                                        <div class="radio-inline">
                                            <input type="radio" name="sexo" id="sexo" value="Feminino">Feminino
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-9 col-sm-10">
                                        <input type="text" class="form-control form-orcamento" id="municipio"
                                               name="municipio"
                                               placeholder="Município"
                                               required maxlength="60">
                                    </div>
                                    <div class="col-xs-3 col-sm-2" style="padding-left: 0px !important;">
                                        <input type="text" class="form-control form-orcamento" id="estado" name="estado"
                                               placeholder="UF"
                                               required maxlength="2" style="text-transform: uppercase;">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <input type="text" class="form-control form-orcamento" name="endereco"
                                               id="endereco"
                                               placeholder="Endereço"
                                               required maxlength="100">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-6">
                                        <input type="text" class="form-control form-orcamento telefone" name="telefone"
                                               id="telefone"
                                               placeholder="Telefone"
                                               required>
                                    </div>
                                    <div class="col-xs-6" style="padding-left: 0px !important;">
                                        <input type="text" class="form-control form-orcamento telefone" name="celular"
                                               id="celular"
                                               placeholder="Celular">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <input type="email" class="form-control form-orcamento" name="email" id="email"
                                               placeholder="E-mail"
                                               required maxlength="100">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <input type="text" class="form-control form-orcamento" name="cargo" id="cargo"
                                               placeholder="Cargo Pretendido"
                                               required maxlength="100">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <textarea class="form-control form-orcamento" name="formacao" id="formacao"
                                            placeholder="Formação"
                                            rows="8" required></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <textarea class="form-control form-orcamento" name="experiencia" id="experiencia"
                                            placeholder="Experiência Profissional"
                                            rows="8" required></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <label for="exampleFormControlFile1">Você pode anexar seu currículo aqui.</label>
                                        <input type="file" name="file" id="file" accept="application/pdf" class="form-control-file" id="exampleFormControlFile1">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <div class="box-recaptcha">
                                            <div class="g-recaptcha" data-sitekey="6LcA2qIUAAAAAMKXPFxk3EK6F_NAW6trkoOTlVxK"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12 text-center">
                                        <button type="submit" class="btn btn-lg cor-botoes" id="btn-enviar-form">
                                            ENVIAR
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                @include("site.includes.menu")
            </div>
        </div>
    </section>
@endsection