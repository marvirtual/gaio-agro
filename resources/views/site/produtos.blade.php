@extends("site.templates.app")
@if(@$titulos->DepTitulo !== null && @$titulos->CatTitulo !== null)
    @section("title","Produtos - ".@$titulos->DepTitulo." - ".@$titulos->CatTitulo)
@elseif(@$titulos->DepTitulo !== null && @$titulos->CatTitulo === null)
    @section("title","Produtos - ". @$titulos->DepTitulo)
@else
    @section("title","Produtos")
@endif
@section("content")
    <section class="section-conteudo">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="titulo-paginas">
                        @if(@$titulos->DepTitulo !== null && @$titulos->CatTitulo !== null)
                            PRODUTOS <h4>{{@$titulos->DepTitulo}} - {{@$titulos->CatTitulo}}</h4>
                        @elseif(@$titulos->DepTitulo !== null && @$titulos->CatTitulo === null)
                            PRODUTOS <h4>{{@$titulos->DepTitulo}}</h4>
                        @else
                            PRODUTOS
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    @if(@$produtos !== null)
                        @foreach($produtos as $produto)
                            <figure class="col-sm-4 altura-fixa-auto">

                                <a href="{{url("produto/id/".$produto->ProCodigo)}}">
                                    <img src="{{asset("upload/produtos/dest_".$produto->ProCodigo.".jpg")}}"
                                         style="min-height: 150px; width: 100%;"
                                         class="img-responsive img-thumbnail">
                                </a>

                                <!-- Item Hover Options -->
                                <figcaption class="text-center">
                                    <a href="{{url("produto/id/".$produto->ProCodigo)}}">
                                        <h4 style="color: #333333;">{{$produto->ProTitulo}}
                                            @if($produto->ProValor != "0.00")
                                                - R$ {{number_format($produto->ProValor,2,',','')}}
                                            @endif
                                        </h4>
                                        <p class="text-muted subtitulo-home-produto">{{substr(strip_tags($produto->ProConteudo),0,70)}}</p>
                                    </a>
                                    <a href="/insert-orcamento/id/{{$produto->ProCodigo}}" class="btn btn-md cor-botoes">Solicitar Orçamento</a>
                                </figcaption>

                            </figure>
                            <div class="clearfix visible-xs">&nbsp;</div>
                        @endforeach
                    @endif
                    <div class="col-sm-12 text-center">
                        {!! $produtos->render() !!}
                    </div>
                </div>
                @include("site.includes.menu")
            </div>
        </div>
    </section>
@endsection