<link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<style>
    .texto-orcamento {
        font-family: Roboto;
        color: #333333;
    }
</style>
<h3 class="texto-orcamento">Orçamento<br>
    <small>www.gaioagro.com.br</small>
</h3>

<div class="texto-orcamento"><strong>Nome:</strong> {{$nome}}</div>
<div class="texto-orcamento"><strong>Município:</strong> {{$municipio}}</div>
<div class="texto-orcamento"><strong>Estado:</strong> {{$estado}}</div>
<div class="texto-orcamento"><strong>Endereço:</strong> {{$endereco}}</div>
<div class="texto-orcamento"><strong>Telefone:</strong> {{$telefone}}</div>
<div class="texto-orcamento"><strong>Email:</strong> {{$email}}</div>
<div class="texto-orcamento"><strong>Observações</strong></div>
<p class="texto-orcamento">{{$observacoes}}</p>
<br><br>
<h4 class="texto-orcamento">Itens do Orçamento</h4>
@if(count($itens) > 0)
    <table style="width: 600px;">
        <thead>
        <tr>
            <th align="left" class="texto-orcamento">Produto</th>
            <th align="center" class="texto-orcamento">Qtde</th>
            <th align="center" class="texto-orcamento">Valor Unit.</th>
        </tr>
        </thead>
        <tbody>
        @foreach($itens as $item)
            <tr>
                <td style="vertical-align: middle;" align="left" class="texto-orcamento">
                    {{$item->ProTitulo}}
                </td>
                <td align="center"
                    style="vertical-align: middle;" class="texto-orcamento">
                    {{$item->IteQtde}}
                </td>
                <td align="center"
                    style="vertical-align: middle;" class="texto-orcamento">
                    @if($item->IteValorUnitario > 0 || $item->IteValorUnitario <> null)
                        R$ {{$item->IteValorUnitario}}
                    @else
                        -
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif
