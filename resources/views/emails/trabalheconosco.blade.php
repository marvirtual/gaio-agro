<link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<style>
    .texto-orcamento {
        font-family: Roboto;
        color: #333333;
    }
</style>
<h3 class="texto-orcamento">Trabalhe Conosco<br><small>www.gaioagro.com.br</small></h3>

<div class="texto-orcamento"><strong>Nome:</strong> {{$nome}}</div>
<div class="texto-orcamento"><strong>Data Nascimento:</strong> {{$datanascimento}}</div>
<div class="texto-orcamento"><strong>Sexo:</strong> {{$sexo}}</div>
<div class="texto-orcamento"><strong>Município:</strong> {{$municipio}}</div>
<div class="texto-orcamento"><strong>Estado:</strong> {{$estado}}</div>
<div class="texto-orcamento"><strong>Endereço:</strong> {{$endereco}}</div>
<div class="texto-orcamento"><strong>Telefone:</strong> {{$telefone}}</div>
<div class="texto-orcamento"><strong>Celular:</strong> {{$celular}}</div>
<div class="texto-orcamento"><strong>Email:</strong> {{$email}}</div>
<div class="texto-orcamento"><strong>Cargo Pretendido:</strong> {{$cargo}}</div>
<div class="texto-orcamento"><strong>Formação:</strong></div>
<p class="texto-orcamento">{{$formacao}}</p>
<div class="texto-orcamento"><strong>Experiência Profissional:</strong></div>
<p class="texto-orcamento">{{$experiencia}}</p>
