<link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<style>
    .texto-orcamento {
        font-family: Roboto;
        color: #333333;
    }
</style>
<h3 class="texto-orcamento">Mensagem do Site<br><small>www.gaioagro.com.br</small></h3>

<div class="texto-orcamento"><strong>Nome:</strong> {{@$nome}}</div>
<div class="texto-orcamento"><strong>Email:</strong> {{@$email}}</div>
<div class="texto-orcamento"><strong>Telefone:</strong> {{@$telefone}}</div>
<div class="texto-orcamento"><strong>Mensagem</strong></div>
<p class="texto-orcamento">{{@$mensagem}}</p>