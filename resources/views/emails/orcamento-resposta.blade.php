<style>
    .texto-orcamento {
        font-family: Arial;
        color: #333333;
    }
</style>
<h3 class="texto-orcamento">Retorno de Orçamento<br>
    <small>www.gaioagro.com.br</small>
</h3>

<div class="texto-orcamento"><strong>Nome:</strong> {{@$nome}}</div>
<div class="texto-orcamento"><strong>Data:</strong> {{@$data}}</div>
<div class="texto-orcamento"><strong>Observações:</strong></div>
<div class="texto-orcamento">{{@$observacoes}}</div>
<br><br>
<h4 class="texto-orcamento">Itens do Orçamento</h4>
@if(count($itens) > 0)
    <table style="width: 600px;">
        <thead>
        <tr>
            <th align="left" class="texto-orcamento">Produto</th>
            <th align="center" class="texto-orcamento">Qtde</th>
            <th align="center" class="texto-orcamento">Valor Unit.</th>
            <th align="center" class="texto-orcamento">Valor Total</th>
            <th align="center" class="texto-orcamento">Disponível</th>
        </tr>
        </thead>
        <tbody>
        @foreach($itens as $item)
            <tr>
                <td style="vertical-align: middle;" align="left" class="texto-orcamento">
                    @if($item->IteDisponivel == 0)
                        {{$item->ProTitulo}}
                    @else
                        <s>{{$item->ProTitulo}}</s>
                    @endif
                </td>
                <td align="center"
                    style="vertical-align: middle;" class="texto-orcamento">{{$item->IteQtde}}</td>
                <td align="center"
                    style="vertical-align: middle;" class="texto-orcamento">
                    @if($item->IteDisponivel == 0)
                        R$ {{$item->IteValorUnitario}}
                    @else
                        -
                    @endif
                </td>
                <td align="center"
                    style="vertical-align: middle;" class="texto-orcamento">
                    @if($item->IteDisponivel == 0)
                        R$ {{$item->IteValorTotal}}
                    @else
                        -
                    @endif
                </td>
                <td align="center" style="vertical-align: middle;" class="texto-orcamento">
                    @if($item->IteDisponivel == 0)
                        Sim
                    @else
                        Não
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif
<br><br>
<p>
<div class="texto-orcamento"><strong>Informações:</strong></div>
<div class="texto-orcamento">{!! @$informacoes !!}</div>
</p>