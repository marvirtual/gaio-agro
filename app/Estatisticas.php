<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Estatisticas extends Model
{

    protected $table = 'testatisticas';
    protected $fillable = ['EstCodigo', 'EstData', 'EstHora', 'EstIp', 'EstPagina'];
    protected $primaryKey = 'EstCodigo';

}
