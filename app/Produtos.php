<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Produtos extends Model
{
    use SoftDeletes;

    protected $table = 'tprodutos';
    protected $primaryKey = 'ProCodigo';
    protected $dates = ['deleted_at'];

//    protected $fillable = ['ProCodigo', 'ProCatCodigo', 'ProTitulo', 'ProConteudo', 'ProLiberado'];
}
