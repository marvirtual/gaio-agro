<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Servicos extends Model
{
    use SoftDeletes;

    protected $table = 'tservicos';
    protected $primaryKey = 'SerCodigo';
    protected $dates = ['deleted_at'];

//    protected $fillable = ['SerCodigo', 'SerTitulo', 'SerConteudo', 'SerLiberado'];
}
