<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vagas extends Model
{
    use SoftDeletes;

    protected $table = 'tvagas';
    protected $primaryKey = 'id';
    protected $dates = ['deleted_at'];

    public function candidatos(){
        return $this->hasMany('\App\VagasCandidatos', 'vaga_id', 'id');
    }

    public function unidadeVaga(){
        return $this->hasOne('\App\UnidadesGaio', 'UniCodigo', 'unidade');
    }
}
