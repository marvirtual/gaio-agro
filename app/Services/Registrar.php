<?php
namespace App\Services;

use App\User;
use Validator;
use Illuminate\Contracts\Auth\Registrar as RegistrarContract;

class Registrar implements RegistrarContract
{

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator(array $data)
    {
        return Validator::make($data, [
            'UserNome' => 'required|max:255',
            'UserId' => 'required|max:15|unique:tusers',
            'UserSenha' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    public function create(array $data)
    {
        return User::create([
            'UserNome' => $data['UserNome'],
            'UserId' => $data['UserId'],
            'UserSenha' => bcrypt($data['UserSenha']),
        ]);
    }

}
