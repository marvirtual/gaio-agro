<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrcamentosArquivos extends Model
{
    use SoftDeletes;

    protected $table = 'torcamentosarquivos';
    protected $primaryKey = 'ArqCodigo';
    protected $dates = ['deleted_at'];

//    protected $fillable = ['ArqCodigo', 'ArqProCodigo', 'ArqTitulo'];
    //protected $hidden = ['password', 'remember_token'];

}
