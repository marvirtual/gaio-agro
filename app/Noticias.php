<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Noticias extends Model {

    use SoftDeletes;

    protected $table = 'tnoticias';
    protected $primaryKey = 'NotCodigo';
    protected $dates = ['deleted_at'];

//    protected $fillable = ['NotCodigo', 'NotData', 'NotTitulo', 'NotResumo', 'NotConteudo', 'NotLegendaFoto', 'NotFonte', 'NotLiberado'];
    //protected $hidden = ['password', 'remember_token'];

}
