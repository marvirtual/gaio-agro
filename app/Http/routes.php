<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'SiteController@home');
Route::get('/home', 'SiteController@home');
Route::get('/empresa', 'SiteController@empresa');
Route::get('/institucional', 'SiteController@institucional');
Route::get('/cotacoes-agricolas', 'SiteController@cotacao');
Route::get('/produtos', 'SiteController@produtos');
Route::get('/produtos/{dep?}', 'SiteController@produtos');
Route::get('/produtos/{dep}/{cat?}', 'SiteController@produtos');
Route::get('/produto/id/{id}', 'SiteController@produto_individual');
Route::get('/insert-orcamento/id/{id}', 'SiteController@insert_orcamento');
Route::get('/destroy-orcamento/id/{id}', 'SiteController@destroy_orcamento');
Route::post('/finaliza-orcamento', 'SiteController@finaliza_orcamento');
Route::get('/qtde-orcamento', 'SiteController@qtde_orcamento');
Route::get('/orcamento', 'SiteController@orcamento');
Route::get('/unidades', 'SiteController@unidades');
Route::get('/z', 'SiteController@trabalhe_conosco');
Route::get('/vagas-de-emprego', 'SiteController@vagas_de_emprego');
Route::get('/vaga/{id}', 'SiteController@vaga');
Route::get('/descricao-vaga/{id}', 'SiteController@descricao_vaga');
Route::get('/cerealista/id/{id}', 'SiteController@cerealista');
Route::get('/contato', 'SiteController@contato');
Route::post('/contato', 'SiteController@contato2');
Route::post('/trabalhe-conosco', 'SiteController@trabalhe_conosco_insert');
Route::post('/vagas-de-emprego', 'SiteController@vagas_de_emprego_insert');
Route::post('/seleciona-unidade', 'SiteController@seleciona_unidade');
Route::get('/galerias', 'SiteController@galerias');
Route::get('/galeria/id/{id}', 'SiteController@galeria');
Route::get('/noticias', 'SiteController@noticias');
Route::get('/noticia/id/{id}', 'SiteController@noticia');
Route::get('/previsao', 'SiteController@previsao');
Route::get('/area-atuacao', 'SiteController@area_atuacao');
Route::get('/programas-radio', 'SiteController@programas_radio');
Route::get('/programa-download/id/{id}', 'SiteController@programa_download');


Route::group(['prefix' => 'paineldecontroleadm', 'middleware' => 'auth'], function () {

    //Home
    Route::get('/', 'DashboardController@index');

    //Minha Conta
    Route::get('/profile', 'UserController@profile');

    //Rotas Usuários
    Route::get('usuarios', 'UserController@index');
    Route::get('usuarios/create', 'UserController@create');
    Route::post('usuarios/create', 'UserController@create2');
    Route::get('usuarios/update/{id}', 'UserController@update');
    Route::post('usuarios/update', 'UserController@update2');
    Route::get('usuarios/destroy/{id}', 'UserController@destroy');
    Route::post('usuarios/upload/{id}', 'UserController@upload');

    //Rotas Agenda
    Route::get('agendaeventos', 'AgendaController@index');
    Route::post('agendaeventos', 'AgendaController@index');
    Route::get('agendaeventos/create', 'AgendaController@create');
    Route::post('agendaeventos/create', 'AgendaController@create2');
    Route::get('agendaeventos/update/{id}', 'AgendaController@update');
    Route::post('agendaeventos/update', 'AgendaController@update2');
    Route::get('agendaeventos/destroy/{id}', 'AgendaController@destroy');

    //Rotas Notícias
    Route::get('noticias', 'NoticiasController@index');
    Route::post('noticias', 'NoticiasController@index');
    Route::get('noticias/create', 'NoticiasController@create');
    Route::post('noticias/create', 'NoticiasController@create2');
    Route::get('noticias/update/{id}', 'NoticiasController@update');
    Route::post('noticias/update', 'NoticiasController@update2');
    Route::get('noticias/destroy/{id}', 'NoticiasController@destroy');
    Route::post('noticias/upload/{id}', 'NoticiasController@upload');

    Route::get('vagas', 'VagasController@index');
    Route::post('vagas', 'VagasController@index');
    Route::get('vagas/create', 'VagasController@create');
    Route::post('vagas/create', 'VagasController@create2');
    Route::get('vagas/update/{id}', 'VagasController@update');
    Route::post('vagas/update', 'VagasController@update2');
    Route::get('vagas/destroy/{id}', 'VagasController@destroy');

    Route::get('vagas/{id}/candidatos', 'VagasCandidatosController@index');
    Route::get('vagas/{id}/candidatos/{id_candidato}', 'VagasCandidatosController@view');
    Route::get('vagas/{id}/candidatos/{id_candidato}/view', 'VagasCandidatosController@view2');
    Route::get('/candidatos/{id_candidato}/destroy', 'VagasCandidatosController@destroy');
    Route::post('vagas/{id}/candidatos/{id_candidato}/entrevista', 'VagasCandidatosController@entrevista');
    Route::post('vagas/{id}/candidatos/{id_candidato}/contratacao', 'VagasCandidatosController@contratacao');

    //Rotas Contatos
    Route::get('contatos', 'ContatosController@index');
    Route::post('contatos', 'ContatosController@index');
    Route::get('contatos/view/{id}', 'ContatosController@view');
    Route::get('contatos/destroy/{id}', 'ContatosController@destroy');

    //Rotas Orçamentos
    Route::get('orcamentos', 'OrcamentosController@index');
    Route::post('orcamentos', 'OrcamentosController@index');
    Route::get('orcamentos/view/{id}', 'OrcamentosController@view');
    Route::get('orcamentos/valor-orcamento', 'OrcamentosController@valor_orcamento');
    Route::get('orcamentos/destroy/{id}', 'OrcamentosController@destroy');
    Route::get('orcamentos/destroy-item/{id}', 'OrcamentosController@destroy_item');
    Route::post('orcamentos/enviar', 'OrcamentosController@enviar');

    //Rotas Trabalhe Conosco
    Route::get('trabalheconosco', 'TrabalheConoscoController@index');
    Route::post('trabalheconosco', 'TrabalheConoscoController@index');
    Route::get('trabalheconosco/view/{id}', 'TrabalheConoscoController@view');
    Route::get('trabalheconosco/destroy/{id}', 'TrabalheConoscoController@destroy');

    //Rotas Galerias
    Route::get('galerias', 'GaleriasController@index');
    Route::post('galerias', 'GaleriasController@index');
    Route::get('galerias/create', 'GaleriasController@create');
    Route::post('galerias/create', 'GaleriasController@create2');
    Route::get('galerias/update/{id}', 'GaleriasController@update');
    Route::post('galerias/update', 'GaleriasController@update2');
    Route::get('galerias/destroy/{id}', 'GaleriasController@destroy');
    Route::post('galerias/upload/{id}', 'GaleriasController@upload');

    Route::get('galerias/{id}', 'GaleriasController@fotos');
    Route::post('galerias/{id}', 'GaleriasController@fotosmultiploupload');
    Route::get('galerias/{id}/destroy/{idfoto}', 'GaleriasController@fotosdestroy');
    Route::get('galerias/{id}/update/{idfoto}', 'GaleriasController@fotosupdate');
    Route::post('galerias/{id}/update/{idfoto}', 'GaleriasController@fotosupdate2');
    Route::post('galerias/{id}/upload/{idfoto}', 'GaleriasController@fotosupload');


    //Rotas Páginas
    Route::get('paginas', 'PaginasController@index');
    Route::post('paginas', 'PaginasController@index');
    Route::get('paginas/create', 'PaginasController@create');
    Route::post('paginas/create', 'PaginasController@create2');
    Route::get('paginas/update/{id}', 'PaginasController@update');
    Route::post('paginas/update', 'PaginasController@update2');
    Route::get('paginas/destroy/{id}', 'PaginasController@destroy');
    Route::post('paginas/upload/{id}', 'PaginasController@upload');

    Route::get('paginas/{id}', 'PaginasController@fotos');
    Route::post('paginas/{id}', 'PaginasController@fotosmultiploupload');
    Route::get('paginas/{id}/destroy/{idfoto}', 'PaginasController@fotosdestroy');
    Route::get('paginas/{id}/update/{idfoto}', 'PaginasController@fotosupdate');
    Route::post('paginas/{id}/update/{idfoto}', 'PaginasController@fotosupdate2');
    Route::post('paginas/{id}/upload/{idfoto}', 'PaginasController@fotosupload');

    //Rotas Unidades
    Route::get('unidades', 'UnidadesController@index');
    Route::post('unidades', 'UnidadesController@index');
    Route::get('unidades/create', 'UnidadesController@create');
    Route::post('unidades/create', 'UnidadesController@create2');
    Route::get('unidades/update/{id}', 'UnidadesController@update');
    Route::post('unidades/update', 'UnidadesController@update2');
    Route::get('unidades/destroy/{id}', 'UnidadesController@destroy');

    //Rotas Cotações Agrícolas
    Route::get('cotacoesagricolas', 'CotacoesAgricolasController@index');
    Route::post('cotacoesagricolas', 'CotacoesAgricolasController@index');
    Route::get('cotacoesagricolas/create', 'CotacoesAgricolasController@create');
    Route::post('cotacoesagricolas/create', 'CotacoesAgricolasController@create2');
    Route::get('cotacoesagricolas/update/{id}', 'CotacoesAgricolasController@update');
    Route::post('cotacoesagricolas/update', 'CotacoesAgricolasController@update2');
    Route::get('cotacoesagricolas/destroy/{id}', 'CotacoesAgricolasController@destroy');

    //Rotas Parceiros
    Route::get('parceiros', 'ParceirosController@index');
    Route::post('parceiros', 'ParceirosController@index');
    Route::get('parceiros/create', 'ParceirosController@create');
    Route::post('parceiros/create', 'ParceirosController@create2');
    Route::get('parceiros/update/{id}', 'ParceirosController@update');
    Route::post('parceiros/update', 'ParceirosController@update2');
    Route::get('parceiros/destroy/{id}', 'ParceirosController@destroy');
    Route::post('parceiros/upload/{id}', 'ParceirosController@upload');

    //Rotas Produtos
    Route::get('produtos', 'ProdutosController@index');
    Route::post('produtos', 'ProdutosController@index');
    Route::get('produtos/create', 'ProdutosController@create');
    Route::post('produtos/create', 'ProdutosController@create2');
    Route::get('produtos/update/{id}', 'ProdutosController@update');
    Route::post('produtos/update', 'ProdutosController@update2');
    Route::get('produtos/destroy/{id}', 'ProdutosController@destroy');
    Route::post('produtos/upload/{id}', 'ProdutosController@upload');
    Route::post('produtos/upload-arte/{id}', 'ProdutosController@upload_arte');

    Route::get('produtos/{id}', 'ProdutosController@fotos');
    Route::post('produtos/{id}', 'ProdutosController@fotosmultiploupload');
    Route::get('produtos/{id}/destroy/{idfoto}', 'ProdutosController@fotosdestroy');
    Route::get('produtos/{id}/update/{idfoto}', 'ProdutosController@fotosupdate');
    Route::post('produtos/{id}/update/{idfoto}', 'ProdutosController@fotosupdate2');
    Route::post('produtos/{id}/upload/{idfoto}', 'ProdutosController@fotosupload');

    Route::get('produtos/arquivos/{id}', 'ProdutosController@arquivos');
    Route::get('produtos/arquivos/{id}/create', 'ProdutosController@arquivoscreate');
    Route::post('produtos/arquivos/{id}/create', 'ProdutosController@arquivoscreate2');
    Route::post('produtos/arquivos/{id}', 'ProdutosController@arquivosmultiploupload');
    Route::get('produtos/arquivos/{id}/destroy/{idarquivo}', 'ProdutosController@arquivosdestroy');
    Route::get('produtos/arquivos/{id}/update/{idarquivo}', 'ProdutosController@arquivosupdate');
    Route::post('produtos/arquivos/{id}/update/{idarquivo}', 'ProdutosController@arquivosupdate2');
    Route::post('produtos/arquivos/{id}/upload/{idarquivo}', 'ProdutosController@arquivosupload');

    //Rotas Produtos Categorias
    Route::get('produtoscategorias', 'ProdutosCategoriasController@index');
    Route::post('produtoscategorias', 'ProdutosCategoriasController@index');
    Route::get('produtoscategorias/create', 'ProdutosCategoriasController@create');
    Route::post('produtoscategorias/create', 'ProdutosCategoriasController@create2');
    Route::get('produtoscategorias/update/{id}', 'ProdutosCategoriasController@update');
    Route::post('produtoscategorias/update', 'ProdutosCategoriasController@update2');
    Route::get('produtoscategorias/getbydepid/{id}', 'ProdutosCategoriasController@getbydepid');
    Route::get('produtoscategorias/destroy/{id}', 'ProdutosCategoriasController@destroy');

    //Rotas Produtos Departamentos
    Route::get('produtosdepartamentos', 'ProdutosDepartamentosController@index');
    Route::post('produtosdepartamentos', 'ProdutosDepartamentosController@index');
    Route::get('produtosdepartamentos/create', 'ProdutosDepartamentosController@create');
    Route::post('produtosdepartamentos/create', 'ProdutosDepartamentosController@create2');
    Route::get('produtosdepartamentos/update/{id}', 'ProdutosDepartamentosController@update');
    Route::post('produtosdepartamentos/update', 'ProdutosDepartamentosController@update2');
    Route::get('produtosdepartamentos/destroy/{id}', 'ProdutosDepartamentosController@destroy');

    //Rotas Produtos Marcas
    Route::get('produtosmarcas', 'ProdutosMarcasController@index');
    Route::post('produtosmarcas', 'ProdutosMarcasController@index');
    Route::get('produtosmarcas/create', 'ProdutosMarcasController@create');
    Route::post('produtosmarcas/create', 'ProdutosMarcasController@create2');
    Route::get('produtosmarcas/update/{id}', 'ProdutosMarcasController@update');
    Route::post('produtosmarcas/update', 'ProdutosMarcasController@update2');
    Route::get('produtosmarcas/destroy/{id}', 'ProdutosMarcasController@destroy');

    //Rotas Serviços
    Route::get('servicos', 'ServicosController@index');
    Route::post('servicos', 'ServicosController@index');
    Route::get('servicos/create', 'ServicosController@create');
    Route::post('servicos/create', 'ServicosController@create2');
    Route::get('servicos/update/{id}', 'ServicosController@update');
    Route::post('servicos/update', 'ServicosController@update2');
    Route::get('servicos/destroy/{id}', 'ServicosController@destroy');
    Route::post('servicos/upload/{id}', 'ServicosController@upload');

    Route::get('servicos/{id}', 'ServicosController@fotos');
    Route::post('servicos/{id}', 'ServicosController@fotosmultiploupload');
    Route::get('servicos/{id}/destroy/{idfoto}', 'ServicosController@fotosdestroy');
    Route::get('servicos/{id}/update/{idfoto}', 'ServicosController@fotosupdate');
    Route::post('servicos/{id}/update/{idfoto}', 'ServicosController@fotosupdate2');
    Route::post('servicos/{id}/upload/{idfoto}', 'ServicosController@fotosupload');

    //Rotas Banners
    Route::get('banners', 'BannersController@index');
    Route::post('banners', 'BannersController@index');
    Route::get('banners/create', 'BannersController@create');
    Route::post('banners/create', 'BannersController@create2');
    Route::get('banners/update/{id}', 'BannersController@update');
    Route::post('banners/update', 'BannersController@update2');
    Route::get('banners/destroy/{id}', 'BannersController@destroy');
    Route::post('banners/upload/{id}', 'BannersController@upload');
    Route::get('banners/{id_ban}/ordem', 'BannersController@ordem');

    //Rotas Programas na Rádio
    Route::get('programasradio', 'ProgramasRadioController@index');
    Route::post('programasradio', 'ProgramasRadioController@index');
    Route::get('programasradio/create', 'ProgramasRadioController@create');
    Route::post('programasradio/create', 'ProgramasRadioController@create2');
    Route::get('programasradio/update/{id}', 'ProgramasRadioController@update');
    Route::post('programasradio/update', 'ProgramasRadioController@update2');
    Route::get('programasradio/destroy/{id}', 'ProgramasRadioController@destroy');
    Route::post('programasradio/upload/{id}', 'ProgramasRadioController@upload');
});

Route::group(['prefix' => 'paineldecontroleadm'], function () {
    //Rotas Login
    Route::get('login', 'UserController@login_index');
    Route::post('login', 'UserController@login');
    Route::get('logout', 'UserController@logout');
});