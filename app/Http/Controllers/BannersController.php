<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Upload;
use App\Banners;
use Input;
use File;
use Route;


class BannersController extends Controller
{

    public function index()
    {
        $itens = Banners::orderBy('BanOrdem', 'asc');

        if (Input::has("busca")) {
            $itens = Banners::where(function ($query) {
                $query->where('BanTitulo', 'like', '%' . Input::get("busca") . '%');
            });
        }

        $itens = $itens->paginate(15);

        return view("painel.banners.index", compact('itens'));
    }

    public function create()
    {
        return view("painel.banners.create");
    }

    public function create2(Request $request)
    {
        $create = new Banners;

        $create->BanTitulo = $request->input('titulo');
        $create->BanLink = $request->input('link');
        $create->BanTarget = $request->input('target');
        $create->BanLiberado = $request->input('liberado');
        $create->BanOrdem = $request->input('ordem');

        $create->save();

        $this->save_log("C", $create->getTable(), $create);

        return redirect(getenv("PAINEL") . "/banners")->with('success', 'Registro incluído com sucesso!');
    }

    public function update($id)
    {
        $item = Banners::where('BanCodigo', '=', $id)->firstOrFail();
        return view("painel.banners.update", compact('item'));
    }

    public function update2(Request $request)
    {
        $id = $request->input('id');

        $update = Banners::find($id);

        $update->BanTitulo = $request->input('titulo');
        $update->BanLink = $request->input('link');
        $update->BanTarget = $request->input('target');
        $update->BanLiberado = $request->input('liberado');
        $update->BanOrdem = $request->input('ordem');

        $update->save();

        $this->save_log("U", $update->getTable(), $update);

        return redirect(getenv("PAINEL") . "/banners")->with('success', 'Registro alterado com sucesso!');
    }

    public function destroy($id)
    {
        Banners::find($id)->delete();

        $this->save_log("D", "tbanners", $id);

//        if (File::exists("upload/banners/" . $id . ".jpg")) {
//            File::delete("upload/banners/" . $id . ".jpg");
//        }

        return redirect(getenv("PAINEL") . "/banners")->with('success', 'Registro excluido com sucesso!');
    }

    public function upload(Request $request, Upload $upload, $id)
    {
        if (Input::hasFile('file')) {
            $upload->resize($request->file("file"), public_path() . "/upload/banners/" . $id . ".jpg", getenv("TamBW"), getenv("TamBH"), true);

            $this->save_log("UP", "tbanners", $id);

            return redirect(getenv("PAINEL") . "/banners")->with("success", "Imagem enviada com sucesso!");
        } else {
            return redirect(getenv("PAINEL") . "/banners")->with("error", "Imagem não enviada!");
        }
    }

    public function ordem()
    {

        $update = Banners::find(Route::input('id_ban'));

        if (count(@$update) > 0) {
            $update->BanOrdem = Input::get('ordem');

            $update->save();

            return 1;
        } else {
            return 0;
        }

    }

}
