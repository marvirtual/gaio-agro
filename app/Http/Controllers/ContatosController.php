<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Upload;
use App\Contatos;
use Input;
use File;


class ContatosController extends Controller 
{

    public function index()
    {
        if (Input::has("busca")) {
            $itens = Contatos::where(function ($query) {
                $query->where('ConNome', 'like', '%' . Input::get("busca") . '%')
                    ->orWhere('ConConteudo', 'like', '%' . Input::get("busca") . '%');
            })
                ->where('ConTipo', '=', '0')
                ->orderBy('created_at', 'desc')
                ->paginate(500);
        } else {
            $itens = Contatos::where('ConTipo', '=', '0')->orderBy('created_at', 'desc')->paginate(15);
        }

        return view("painel.contatos.index", compact('itens'));
    }

    public function view($id)
    {
        $item = Contatos::where('ConCodigo', '=', $id)
            ->where('ConTipo', '=', '0')
            ->firstOrFail();

        return view("painel.contatos.view", compact('item'));
    }

    public function destroy($id)
    {
        Contatos::find($id)->delete();

        $this->save_log("D", "tcontatos", $id);

        return redirect(getenv("PAINEL") . "/contatos")->with('success', 'Registro excluido com sucesso!');
    }

}
