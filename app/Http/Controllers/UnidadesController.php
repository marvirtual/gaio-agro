<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\unidades;
use App\ProdutosCategorias;
use App\ProdutosDepartamentos;
use App\ProdutosMarcas;
use App\UnidadesGaio;
use Illuminate\Http\Request;
use App\Services\Upload;
use App\Produtos;
use App\ProdutosFotos;
use App\ProdutosArquivos;
use Input;
use File;
use Validator;

class UnidadesController extends Controller
{

    public function index()
    {
        if (Input::has("busca")) {
            $itens = UnidadesGaio::where(function ($query) {
                $query->where('UniMunicipio', 'like', '%' . Input::get("busca") . '%');
            })
                ->orderBy('created_at', 'desc')
                ->paginate(500);
        } else {
            $itens = UnidadesGaio::orderBy('created_at', 'desc')
                ->paginate(15);
        }
        return view("painel.unidades.index", compact('itens'));
    }

    public function create()
    {
        return view("painel.unidades.create");
    }


    public function create2(Request $request)
    {
        $create = new UnidadesGaio;

        $create->UniMunicipio = $request->input('municipio');
        $create->UniEndereco = $request->input('endereco');
        $create->UniTelefone = $request->input('telefone');
        $create->UniMapa = $request->input('mapa');
        $create->UniTipo = $request->input('tipo');

        $create->save();

        $this->save_log("C", $create->getTable(), $create);

        return redirect(getenv("PAINEL") . '/unidades')->with('success', 'Registro incluído com sucesso!');
    }

    public function update($id)
    {
        $item = UnidadesGaio::where('UniCodigo', '=', $id)->firstOrFail();

        return view("painel.unidades.update", compact('item'));
    }

    public function update2(Request $request)
    {
        $id = $request->input('id');

        $update = UnidadesGaio::find($id);

        $update->UniMunicipio = $request->input('municipio');
        $update->UniEndereco = $request->input('endereco');
        $update->UniTelefone = $request->input('telefone');
        $update->UniMapa = $request->input('mapa');
        $update->UniTipo = $request->input('tipo');

        $update->save();

        $this->save_log("U", $update->getTable(), $update);

        return redirect(getenv("PAINEL") . '/unidades')->with('success', 'Registro alterado com sucesso!');
    }

    public function upload(Request $request, Upload $upload, $id)
    {
        if (Input::hasFile('file')) {

            $upload->resize($request->file("file"), public_path() . "/upload/unidades/" . $id . ".jpg", getenv("TamPW"), getenv("TamPH"));

            $this->save_log("UP", "tunidades", $id);

            return redirect(getenv("PAINEL") . '/unidades')->with("success", "Imagem enviada com sucesso!");
        } else {
            return redirect(getenv("PAINEL") . '/unidades')->with("error", "Imagem não enviada!");
        }
    }

    public function destroy($id)
    {
        UnidadesGaio::find($id)->delete();

        $this->save_log("D", "tunidadesgaio", $id);

        return redirect(getenv("PAINEL")."/unidades")->with('success', 'Registro excluido com sucesso!');
    }
}
