<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ProdutosDepartamentos;
use App\ProdutosMarcas;
use Illuminate\Http\Request;
use App\Services\Upload;
use App\ProdutosCategorias;
use Input;


class ProdutosMarcasController extends Controller
{

    public function __construct()
    {

    }

    public function index()
    {
        if (Input::has("busca")) {
            $itens = ProdutosMarcas::where(function ($query) {
                $query->where('MarTitulo', 'like', '%' . Input::get("busca") . '%');
            })
                ->orderBy('created_at', 'desc')
                ->paginate(500);
        } else {
            $itens = ProdutosMarcas::orderBy('created_at', 'desc')->paginate(15);
        }

        return view("painel.produtosmarcas.index", compact('itens'));
    }

    public function create()
    {
        return view("painel.produtosmarcas.create");
    }

    public function create2(Request $request)
    {
        $create = new ProdutosMarcas();

        $create->MarTitulo = $request->input('titulo');
        $create->MarLiberado = $request->input('liberado');

        $create->save();

        $this->save_log("C", $create->getTable(), $create);

        return redirect(getenv("PAINEL")."/produtosmarcas")->with('success', 'Registro incluído com sucesso!');
    }

    public function update($id)
    {
        $item = ProdutosMarcas::where('MarCodigo', '=', $id)
            ->firstOrFail();

        return view("painel.produtosmarcas.update", compact('item'));
    }

    public function update2(Request $request)
    {
        $id = $request->input('id');

        $update = ProdutosMarcas::find($id);

        $update->MarTitulo = $request->input('titulo');
        $update->MarLiberado = $request->input('liberado');

        $update->save();

        $this->save_log("U", $update->getTable(), $update);

        return redirect(getenv("PAINEL")."/produtosmarcas")->with('success', 'Registro alterado com sucesso!');
    }

    public function destroy($id)
    {
        ProdutosMarcas::find($id)->delete();

        $this->save_log("D", "tprodutosmarcas", $id);

        return redirect(getenv("PAINEL")."/produtosmarcas")->with('success', 'Registro excluido com sucesso!');
    }
}
