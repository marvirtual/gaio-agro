<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Parceiros;
use App\ProdutosCategorias;
use App\ProdutosDepartamentos;
use App\ProdutosMarcas;
use Illuminate\Http\Request;
use App\Services\Upload;
use App\Produtos;
use App\ProdutosFotos;
use App\ProdutosArquivos;
use Input;
use File;
use Validator;

class ParceirosController extends Controller
{

    public function index()
    {
        if (Input::has("busca")) {
            $itens = Parceiros::where(function ($query) {
                $query->where('ParTitulo', 'like', '%' . Input::get("busca") . '%');
            })
                ->orderBy('created_at', 'desc')
                ->paginate(500);
        } else {
            $itens = Parceiros::orderBy('created_at', 'desc')
                ->paginate(15);
        }
        return view("painel.parceiros.index", compact('itens'));
    }

    public function create()
    {
        return view("painel.parceiros.create");
    }


    public function create2(Request $request)
    {
        $create = new parceiros;

        $create->ParTitulo = $request->input('titulo');
        $create->ParLink = $request->input('link');
        $create->ParLiberado = $request->input('liberado');

        $create->save();

        $this->save_log("C", $create->getTable(), $create);

        return redirect(getenv("PAINEL") . '/parceiros')->with('success', 'Registro incluído com sucesso!');
    }

    public function update($id)
    {
        $item = Parceiros::where('ParCodigo', '=', $id)->firstOrFail();

        return view("painel.parceiros.update", compact('item'));
    }

    public function update2(Request $request)
    {
        $id = $request->input('id');

        $update = Parceiros::find($id);

        $update->ParTitulo = $request->input('titulo');
        $update->ParLink = $request->input('link');
        $update->ParLiberado = $request->input('liberado');

        $update->save();

        $this->save_log("U", $update->getTable(), $update);

        return redirect(getenv("PAINEL") . '/parceiros')->with('success', 'Registro alterado com sucesso!');
    }

    public function upload(Request $request, Upload $upload, $id)
    {
        if (Input::hasFile('file')) {

            $upload->resize($request->file("file"), public_path() . "/upload/parceiros/" . $id . ".jpg", getenv("TamPW"), getenv("TamPH"));

            $this->save_log("UP", "tParceiros", $id);

            return redirect(getenv("PAINEL") . '/parceiros')->with("success", "Imagem enviada com sucesso!");
        } else {
            return redirect(getenv("PAINEL") . '/parceiros')->with("error", "Imagem não enviada!");
        }
    }

    public function destroy($id)
    {
        ProdutosCategorias::find($id)->delete();

        $this->save_log("D", "tparceiros", $id);

        return redirect(getenv("PAINEL")."/parceiros")->with('success', 'Registro excluido com sucesso!');
    }
}
