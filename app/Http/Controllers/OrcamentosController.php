<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Orcamentos;
use App\OrcamentosItens;
use App\Produtos;
use Illuminate\Http\Request;
use App\Services\Upload;
use App\Contatos;
use Input;
use File;
use Mail;


class OrcamentosController extends Controller
{

    public function index()
    {
        if (Input::has("busca")) {
            $itens = Orcamentos::where(function ($query) {
                $query->where('OrcNome', 'like', '%' . Input::get("busca") . '%')
                    ->orWhere('OrcMunicipio', 'like', '%' . Input::get("busca") . '%');
            })
                ->where('OrcStatus', '=', '1')
                ->orderBy('created_at', 'desc')
                ->paginate(500);
        } else {
            $itens = Orcamentos::where('OrcStatus', '=', '1')
                ->orderBy('created_at', 'desc')
                ->paginate(15);
        }

        return view("painel.orcamentos.index", compact('itens'));
    }

    public function view($id)
    {
        $orcamento = Orcamentos::where('OrcCodigo', '=', $id)
            ->where('OrcStatus', '=', '1')
            ->firstOrFail();

        $itens = OrcamentosItens::join('tprodutos', 'ProCodigo', '=', 'IteProCodigo')
            ->where('IteOrcCodigo', '=', $id)
            ->orderBy('IteCodigo', 'desc')
            ->get();

        return view("painel.orcamentos.view", compact('orcamento', 'itens'));
    }

    public function destroy($id)
    {
        Orcamentos::find($id)->delete();

        $this->save_log("D", "torcamentos", $id);

        return redirect(getenv("PAINEL") . "/orcamentos")->with('success', 'Registro excluido com sucesso!');
    }

    public function destroy_item($id)
    {
        $item = OrcamentosItens::where('IteCodigo', '=', $id)
            ->firstOrFail();

        $update = OrcamentosItens::find($id);

        if ($item->IteDisponivel == 0) {
            $update->IteDisponivel = 1;
        } else {
            $update->IteDisponivel = 0;
        }

        $update->save();

        $this->save_log("U", $update->getTable(), $update);

        return redirect(getenv("PAINEL") . "/orcamentos/view/" . $item->IteOrcCodigo)->with('success', 'Registro excluido com sucesso!');
    }

    public function valor_orcamento(Request $request)
    {
        $id = $request->input('id');

        $update = OrcamentosItens::where('IteCodigo', '=', $id)
            ->firstOrFail();

        $update->IteValorUnitario = $request->input('valor');
        $update->IteValorTotal = $request->input('valor') * $update->IteQtde;

        $update->save();

        return number_format($update->IteValorTotal, 2, ".", "");
    }

    public function enviar(Request $request)
    {
        $id = $request->input('id');

        $update = Orcamentos::where('OrcCodigo', '=', $id)
            ->firstOrFail();
        $update->OrcInformacoes = $request->input('informacoes');
        $update->OrcEnviado = '1';
        $update->save();

        $orcamento = Orcamentos::where('OrcCodigo', '=', $id)
            ->where('OrcStatus', '=', '1')
            ->firstOrFail();

        $itens = OrcamentosItens::join('tprodutos', 'ProCodigo', '=', 'IteProCodigo')
            ->where('IteOrcCodigo', '=', $id)
            ->orderBy('IteCodigo', 'desc')
            ->get();

        $dados = [
            'nome' => $orcamento->OrcNome,
            'data' => implode("/", array_reverse(explode("-", $orcamento->OrcData))),
            'observacoes' => $orcamento->OrcObservacoes,
            'informacoes' => $orcamento->OrcInformacoes,
            'itens' => $itens
        ];

        Mail::send('emails.orcamento-resposta', $dados, function ($m) use ($orcamento){
                $m->from(getenv("MAIL_USERNAME"), 'Gaio Agronegócios');
                $m->to($orcamento->OrcEmail, $orcamento->OrcNome)->subject('Orçamento - www.gaioagro.com.br!');
            });
        return redirect(getenv("PAINEL") . "/orcamentos")->with('success', "Mensagem Enviada com Sucesso");
    }
}
