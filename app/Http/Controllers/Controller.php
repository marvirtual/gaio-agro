<?php namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Logging;
use Auth;

abstract class Controller extends BaseController {

	use DispatchesCommands, ValidatesRequests;

	protected $UserCodigo;
	protected $UserNivel;

	public function __construct()
	{
		if (Auth::check()) {
			$this->UserCodigo = Auth::user()->UserCodigo;
			$this->UserNivel = Auth::user()->UserNivel;
		}

	}
	public function save_log($tipo = "", $tabela = "", $mensagem = "")
	{
		$log = new Logging();
		$log->LogTipo = $tipo;
		$log->LogTabela = $tabela;
		$log->LogMensagem = $mensagem;
		$log->LogUserCodigo = $this->UserCodigo;
		$log->save();
	}
}
