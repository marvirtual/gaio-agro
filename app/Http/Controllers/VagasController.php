<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\UnidadesGaio;
use App\Vagas;
use App\VagasCandidatos;
use Illuminate\Http\Request;
use App\Services\Upload;
use App\Noticias;
use Input;
use File;


class VagasController extends Controller
{
    public function index()
    {
        if (Input::has("busca")) {
            $itens = Vagas::where(function ($query) {
                $query->where('titulo', 'like', '%' . Input::get("busca") . '%')
                    ->orWhere('atribuicoes', 'like', '%' . Input::get("busca") . '%')
                    ->orWhere('requisitos', 'like', '%' . Input::get("busca") . '%');
            })
                ->orderBy('created_at', 'desc')
                ->paginate(500);
        } else {
            $itens = Vagas::orderBy('created_at', 'desc')->paginate(15);
        }

        return view("painel.vagas.index", compact('itens'));
    }

    public function create()
    {
        $unidades = UnidadesGaio::groupBy("UniMunicipio")
        ->get();

        return view("painel.vagas.create", compact("unidades"));
    }

    public function create2(Request $request)
    {
        $create = new Vagas();

        $create->titulo = $request->input('titulo');
        $create->atribuicoes = $request->input('atribuicoes');
        $create->requisitos = $request->input('requisitos');
        $create->unidade = $request->input('unidade');
        $create->liberado = $request->input('liberado');

        $create->save();

        $this->save_log("C", $create->getTable(), $create);

        return redirect(getenv("PAINEL") . "/vagas")->with('success', 'Registro incluído com sucesso!');
    }

    public function update($id)
    {
        $item = Vagas::where('id', '=', $id)
            ->firstOrFail();

        $unidades = UnidadesGaio::all();

        return view("painel.vagas.update", compact("item", "unidades"));
    }

    public function update2(Request $request)
    {
        $id = $request->input('id');

        $update = Vagas::find($id);

        $update->titulo = $request->input('titulo');
        $update->atribuicoes = $request->input('atribuicoes');
        $update->requisitos = $request->input('requisitos');
        $update->unidade = $request->input('unidade');
        $update->liberado = $request->input('liberado');

        $update->save();

        $this->save_log("U", $update->getTable(), $update);

        return redirect(getenv("PAINEL") . "/vagas")->with('success', 'Registro alterado com sucesso!');
    }

    public function destroy($id)
    {
        Vagas::find($id)->delete();

        $this->save_log("D", "tvagas", $id);

        return redirect(getenv("PAINEL") . "/vagas")->with('success', 'Registro excluido com sucesso!');
    }
}
