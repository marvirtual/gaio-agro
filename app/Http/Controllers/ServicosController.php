<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Upload;
use App\Servicos;
use App\ServicosFotos;
use Input;
use File;


class ServicosController extends Controller
{

    public function index()
    {
        if (Input::has("busca")) {
            $itens = Servicos::where(function ($query) {
                $query->where('SerTitulo', 'like', '%' . Input::get("busca") . '%')
                    ->orWhere('SerConteudo', 'like', '%' . Input::get("busca") . '%');
            })
                ->orderBy('created_at', 'desc')
                ->paginate(500);
        } else {
            $itens = Servicos::orderBy('created_at', 'desc')->paginate(15);
        }
        return view("painel.servicos.index", compact('itens'));

    }

    public function fotos($id)
    {
        $itens = ServicosFotos::where('FotSerCodigo', '=', $id)
            ->orderBy('created_at', 'desc')
            ->paginate(15);

        return view("painel.servicos.fotos", compact('itens', 'id'));
    }

    public function create()
    {
        return view("painel.servicos.create");
    }

    public function create2(Request $request)
    {
        $create = new Servicos;

        $create->SerTitulo = $request->input('titulo');
        $create->SerConteudo= $request->input('conteudo');
        $create->SerLiberado = $request->input('liberado');

        $create->save();

        $this->save_log("C", $create->getTable(), $create);

        return redirect(getenv("PAINEL") . '/servicos')->with('success', 'Registro incluído com sucesso!');
    }

    public function update($id)
    {
        $item = Servicos::where('SerCodigo', '=', $id)
            ->firstOrFail();

        return view("painel.servicos.update", compact('item'));
    }

    public function update2(Request $request)
    {
        $id = $request->input('id');

        $update = Servicos::find($id);

        $update->SerTitulo = $request->input('titulo');
        $update->SerConteudo= $request->input('conteudo');
        $update->SerLiberado = $request->input('liberado');

        $update->save();

        $this->save_log("U", $update->getTable(), $update);

        return redirect(getenv("PAINEL") . '/servicos')->with('success', 'Registro alterado com sucesso!');
    }

    public function fotosupdate($id, $idfoto)
    {
        $item = ServicosFotos::where('FotCodigo', '=', $idfoto)
            ->firstOrFail();

        return view("painel.servicos.fotosupdate", compact('item', 'id', 'idfoto'));
    }

    public function fotosupdate2(Request $request)
    {
        $id = $request->input('id');
        $idfoto = $request->input('idfoto');

        $update = ServicosFotos::find($idfoto);

        $update->FotLegenda = $request->input('legenda');

        $update->save();

        $this->save_log("U", $update->getTable(), $update);

        return redirect(getenv("PAINEL") . '/servicos/' . $id)->with('success', 'Registro alterado com sucesso!');
    }


    public function destroy($id)
    {
        $itens = ServicosFotos::where('FotSerCodigo', '=', $id)->get();

        foreach ($itens as $item) {
//            if (File::exists("upload/servicos/p_" . $item->FotCodigo . ".jpg")) {
//                File::delete("upload/servicos/p_" . $item->FotCodigo . ".jpg");
//            }
//            if (File::exists("upload/servicos/g_" . $item->FotCodigo . ".jpg")) {
//                File::delete("upload/servicos/g_" . $item->FotCodigo . ".jpg");
//            }

            ServicosFotos::find($item->FotCodigo)->delete();

            $this->save_log("D", "tServicosFotos", $item->FotCodigo);
        }

//        if (File::exists("upload/servicos/dest_" . $id . ".jpg")) {
//            File::delete("upload/servicos/dest_" . $id . ".jpg");
//        }
        Servicos::find($id)->delete();

        $this->save_log("D", "tServicos", $id);

        return redirect(getenv("PAINEL") . '/servicos')->with('success', 'Registro excluido com sucesso!');
    }

    public function fotosdestroy($id, $idfoto)
    {
//        if (File::exists("upload/servicos/p_" . $idfoto . ".jpg")) {
//            File::delete("upload/servicos/p_" . $idfoto . ".jpg");
//        }
//        if (File::exists("upload/servicos/g_" . $idfoto . ".jpg")) {
//            File::delete("upload/servicos/g_" . $idfoto . ".jpg");
//        }
        ServicosFotos::find($idfoto)->delete();

        $this->save_log("D", "tServicosFotos", $idfoto);

        return redirect(getenv("PAINEL") . '/servicos/' . $id)->with('success', 'Registro excluido com sucesso!');
    }

    public function upload(Request $request, Upload $upload, $id)
    {
        if (Input::hasFile('file')) {

            $upload->resize($request->file("file"), public_path() . "/upload/servicos/dest_" . $id . ".jpg", getenv("TamGW"), getenv("TamGH"));

            $this->save_log("UP", "tServicos", $id);

            return redirect(getenv("PAINEL") . '/servicos')->with("success", "Imagem enviada com sucesso!");
        } else {
            return redirect(getenv("PAINEL") . '/servicos')->with("error", "Imagem não enviada!");
        }
    }

    public function fotosupload(Request $request, Upload $upload, $id, $idfoto)
    {
        if (Input::hasFile('file')) {

            $upload->resize($request->file("file"), public_path() . "/upload/servicos/g_" . $idfoto . ".jpg", getenv("TamGW"), getenv("TamGH"));
            $upload->resize($request->file("file"), public_path() . "/upload/servicos/p_" . $idfoto . ".jpg", getenv("TamPW"), getenv("TamPH"));

            $this->save_log("D", "tServicosFotos", $idfoto);

            return redirect(getenv("PAINEL") . '/servicos/' . $id)->with("success", "Imagem enviada com sucesso!");
        } else {
            return redirect(getenv("PAINEL") . '/servicos/' . $id)->with("error", "Imagem não enviada!");
        }
    }

    public function fotosmultiploupload(Request $request, Upload $upload, $id)
    {

        if (Input::hasFile('files')) {

            foreach (Input::file('files') as $file) {

                $foto = new ServicosFotos;

                $foto->FotSerCodigo = $id;

                $foto->save();

                $this->save_log("C", $foto->getTable(), $foto);

                $codigo = $foto->FotCodigo;

                $upload->resize($file, public_path() . "/upload/servicos/g_" . $codigo . ".jpg", getenv("TamGW"), getenv("TamGH"));
                $upload->resize($file, public_path() . "/upload/servicos/p_" . $codigo . ".jpg", getenv("TamPW"), getenv("TamPH"));

                $this->save_log("UP", $foto->getTable(), $foto);

            }
            return redirect(getenv("PAINEL") . '/servicos/' . $id)->with("success", "Imagens enviadas com sucesso!");
        } else {
            return redirect(getenv("PAINEL") . '/servicos/' . $id)->with("error", "Imagens não enviadas!");
        }
    }

}
