<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Upload;
use App\Noticias;
use Input;
use File;


class NoticiasController extends Controller
{

    public function index()
    {
        if (Input::has("busca")) {
            $itens = Noticias::where(function ($query) {
                $query->where('NotTitulo', 'like', '%' . Input::get("busca") . '%')
                    ->orWhere('NotResumo', 'like', '%' . Input::get("busca") . '%');
            })
                ->where(function ($query){
                    $query->where('NotLinkFoto', '=', '')
                          ->where('NotLink', '=', '');
                })
                ->orderBy('created_at', 'desc')
                ->paginate(500);
        } else {
            $itens = Noticias::where(function ($query){
                $query->where('NotLinkFoto', '=', '')
                      ->where('NotLink', '=', '');
            })
                ->orderBy('created_at', 'desc')->paginate(15);
        }

        return view("painel.noticias.index", compact('itens'));
    }

    public function create()
    {
        return view("painel.noticias.create");
    }

    public function create2(Request $request)
    {
        $create = new Noticias;

        $create->NotData = implode("-", array_reverse(explode("/", $request->input('data'))));
        $create->NotTitulo = $request->input('titulo');
        $create->NotResumo = $request->input('resumo');
        $create->NotConteudo = $request->input('conteudo');
        $create->NotLegendaFoto = $request->input('legenda');
        $create->NotFonte = $request->input('fonte');
        $create->NotVideo = $request->input('video');
        $create->NotLinkFoto = '';
        $create->NotLink = '';
        $create->NotLiberado = $request->input('liberado');

        $create->save();

        $this->save_log("C", $create->getTable(), $create);

        return redirect(getenv("PAINEL") . "/noticias")->with('success', 'Registro incluído com sucesso!');
    }

    public function update($id)
    {
        $item = Noticias::where('NotCodigo', '=', $id)
            ->firstOrFail();

        return view("painel.noticias.update", compact('item'));
    }

    public function update2(Request $request)
    {
        $id = $request->input('id');

        $update = Noticias::find($id);

        $update->NotData = implode("-", array_reverse(explode("/", $request->input('data'))));
        $update->NotTitulo = $request->input('titulo');
        $update->NotResumo = $request->input('resumo');
        $update->NotConteudo = $request->input('conteudo');
        $update->NotLegendaFoto = $request->input('legenda');
        $update->NotFonte = $request->input('fonte');
        $update->NotVideo = $request->input('video');
        $update->NotLinkFoto = '';
        $update->NotLink = '';
        $update->NotLiberado = $request->input('liberado');

        $update->save();

        $this->save_log("U", $update->getTable(), $update);

        return redirect(getenv("PAINEL") . "/noticias")->with('success', 'Registro alterado com sucesso!');
    }

    public function destroy($id)
    {
        Noticias::find($id)->delete();

        $this->save_log("D", "tnoticias", $id);

//        if (File::exists("upload/noticias/g_" . $id . ".jpg")) {
//            File::delete("upload/noticias/g_" . $id . ".jpg");
//        }
//        if (File::exists("upload/noticias/p_" . $id . ".jpg")) {
//            File::delete("upload/noticias/p_" . $id . ".jpg");
//        }
        return redirect(getenv("PAINEL") . "/noticias")->with('success', 'Registro excluido com sucesso!');
    }

    public function upload(Request $request, Upload $upload, $id)
    {
        if (Input::hasFile('file')) {
            $upload->resize($request->file("file"), public_path() . "/upload/noticias/g_" . $id . ".jpg", getenv("TamGW"), getenv("TamGH"));
            $upload->resize($request->file("file"), public_path() . "/upload/noticias/p_" . $id . ".jpg", getenv("TamPW"), getenv("TamPH"));

            $this->save_log("UP", "tnoticias", $id);

            return redirect(getenv("PAINEL") . "/noticias")->with("success", "Imagem enviada com sucesso!");
        } else {
            return redirect(getenv("PAINEL") . "/noticias")->with("error", "Imagem não enviada!");
        }
    }

}
