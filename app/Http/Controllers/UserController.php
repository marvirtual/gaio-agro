<?php namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Services\Upload;
use Input;
use Auth;
use Validator;
use App\User;
use Hash;


class UserController extends Controller
{

    public function __construct()
    {

    }

    public function index()
    {
        if (Auth::user()->UserNivel <= 2) {
            if (Auth::user()->UserNivel == 1) {
                $itens = User::orderBy('created_at', 'desc')->paginate(15);
            } else {
                $itens = User::where('UserNivel', '<>', 1)->orderBy('created_at', 'desc')->paginate(15);
            }
            return view("painel.usuarios.index", compact('itens'));
        } else {
            return redirect(getenv("PAINEL") . "/")->with("error", "Você não tem autorização para acessar esta página");
        }
    }

    public function create()
    {
        if (Auth::user()->UserNivel <= 2) {
            return view("painel.usuarios.create");
        } else {
            return redirect(getenv("PAINEL") . "/")->with("error", "Você não tem autorização para acessar esta página");
        }
    }

    public function create2(Request $request)
    {
        if (Auth::user()->UserNivel <= 2) {
            $usuario = new User();
            $usuario->UserNome = $request->input('nome');
            $usuario->UserId = $request->input('usuario');
            $usuario->UserSenha = Hash::make($request->input('senha'));
            $usuario->UserSenhaRelembrar = $request->input('senha');
            $usuario->UserNivel = $request->input('nivel');
            $usuario->UserLiberado = $request->input('liberado');

            $usuario->save();
            return redirect(getenv("PAINEL") . "/usuarios")->with('success', 'Registro incluído com sucesso!');
        } else {
            return redirect(getenv("PAINEL") . "/")->with("error", "Você não tem autorização para acessar esta página");
        }
    }

    public function update($id)
    {
        $item = User::find($id);
        if (@$item !== null && (Auth::user()->UserNivel == 1 || Auth::user()->UserCodigo == $id || (Auth::user()->UserNivel == 2 && @$item->UserNivel == 3))) {
            return view("painel.usuarios.update", compact('item'));
        } else {
            return redirect(getenv("PAINEL") . "/")->with("error", "Você não tem autorização para acessar esta página");
        }
    }

    public function update2(Request $request)
    {
        $id = $request->input('id');
        if (Auth::user()->UserNivel <= 2 || Auth::user()->UserCodigo == $id) {

            $usuario = User::find($id);
            $usuario->UserNome = $request->input('nome');
            $usuario->UserId = $request->input('usuario');
            if ($request->input('senha') != "") {
                $usuario->UserSenhaRelembrar = $request->input('senha');
                $usuario->UserSenha = Hash::make($request->input('senha'));
            }
            if ($request->input('nivel') != "") {
                $usuario->UserNivel = $request->input('nivel');
            }
            $usuario->UserLiberado = $request->input('liberado');

            $usuario->save();

            return redirect(getenv("PAINEL") . "/usuarios")->with('success', 'Registro alterado com sucesso!');
        } else {
            return redirect(getenv("PAINEL") . "/")->with("error", "Você não tem autorização para acessar esta página");
        }
    }

    public function upload(Request $request, Upload $upload, $id)
    {
        if (Auth::user()->UserCodigo == $id) {
            if (Input::hasFile('file')) {
                $upload->resize($request->file("file"), public_path() . "/upload/usuarios/avatar_" . $id . ".jpg", getenv("TamPW"), getenv("TamPH"));
                return redirect(getenv("PAINEL"))->with("success", "Imagem enviada com sucesso!");
            } else {
                return redirect(getenv("PAINEL"))->with("error", "Imagem não enviada!");
            }
        } else {
            return redirect(getenv("PAINEL") . "/")->with("error", "Você não tem autorização para acessar esta página");
        }
    }

    public function destroy($id)
    {
        if (Auth::user()->UserNivel <= 2) {
            User::find($id)->delete();
            return redirect(getenv("PAINEL") . "/usuarios")->with('success', 'Registro excluido com sucesso!');
        } else {
            return redirect(getenv("PAINEL") . "/")->with("error", "Você não tem autorização para acessar esta página");
        }
    }

    public function login_index()
    {
        return view("painel.login.index");
    }

    public function login(Request $request)
    {
        $rules = [
            'usuario' => 'required',
            'senha' => 'required',
        ];
        $messages = [
            'usuario' => ['required' => 'O campo usuário é obrigatório.'],
            'senha' => ['required' => 'O campo senha é obrigatório.']
        ];
        $validator = Validator::make($request->all(), $rules, $messages);

        $credentials = array(
            'UserId' => $request->UserId,
            'password' => $request->password,
            'UserLiberado' => "1"
        );
        if (Auth::attempt($credentials, $request->has("remember"))) {
            return redirect(getenv("PAINEL") . "/");
        } else {
            return redirect(getenv("PAINEL") . "/login")->withErrors($validator)->withInput();
        }

    }

    public function logout()
    {
        Auth::logout();
        return redirect(getenv("PAINEL") . "/login")->with("success", "Logout Efetuado com Sucesso!");
    }
}
