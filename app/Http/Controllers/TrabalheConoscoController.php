<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\VagasCandidatos;
use Illuminate\Http\Request;
use App\Services\Upload;
use App\Contatos;
use Input;
use File;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;


class TrabalheConoscoController extends Controller
{
    public function paginate($items, $perPage = 15, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    public function index()
    {
        if (Input::has("busca")) {
            $contatos = Contatos::where(function ($query) {
                $query->where('ConNome', 'like', '%' . Input::get("busca") . '%')
                    ->orWhere('ConConteudo', 'like', '%' . Input::get("busca") . '%');
            })->where('ConTipo', '=', '1')
                ->get();

            $candidatos = VagasCandidatos::where(function($query2){
                $query2->where('nome', 'like', '%' . Input::get("busca") . '%')
                    ->orWhere('conteudo', 'like', '%' . Input::get("busca") . '%');
            })->get();
        } else {
            $contatos = Contatos::where('ConTipo', '=', '1')->get();
            $candidatos = VagasCandidatos::get();
        }

        $itens = $contatos->merge($candidatos)->sortByDesc('created_at')->all();

        $perPage = 15; // How many items do you want to display.
        $itens = $this->paginate($itens, $perPage)->setPath(url(getenv("painel").'/trabalheconosco'));

        $busca = Input::get("busca");

        return view("painel.trabalheconosco.index", compact('itens', 'busca'));
    }

    public function view($id)
    {
        $item = Contatos::where('ConCodigo', '=', $id)
            ->where('ConTipo', '=', '1')
            ->firstOrFail();

        return view("painel.trabalheconosco.view", compact('item'));
    }

    public function destroy($id)
    {
        Contatos::find($id)->delete();

        $this->save_log("D", "tcontatos", $id);

        return redirect(getenv("PAINEL") . "/trabalheconosco")->with('success', 'Registro excluido com sucesso!');
    }

}
