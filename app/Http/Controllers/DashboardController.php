<?php namespace App\Http\Controllers;

use App\Estatisticas;
use DB;

class DashboardController extends Controller
{

    public function __construct()
    {
        //$this->user = $user;
    }

    /**
     * Show the application welcome screen to the user.
     *
     * @return Response
     */
    public function index()
    {
        $data = date("Y-m-d");
        $data_ant = date("Y-m-d", strtotime("-15 days"));
        $estatisticas = Estatisticas::select(DB::raw('count(1) as total,EstData, EstHora'))->whereBetween('EstData', array($data_ant, $data))
            ->groupBy('EstData')
            ->orderBy('EstData', 'desc')
            ->get();
        return view("painel.estatistica", compact('estatisticas'));
    }

}
