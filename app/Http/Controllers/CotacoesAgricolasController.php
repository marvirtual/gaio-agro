<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Parceiros;
use App\ProdutosCategorias;
use App\ProdutosDepartamentos;
use App\ProdutosMarcas;
use Illuminate\Http\Request;
use App\Services\Upload;
use App\Produtos;
use App\ProdutosFotos;
use App\ProdutosArquivos;
use App\CotacoesAgricolas;
use Input;
use File;
use Validator;

class CotacoesAgricolasController extends Controller
{

    public function index()
    {
        if (Input::has("busca")) {
            $itens = CotacoesAgricolas::where(function ($query) {
                $query->where('CotData', 'like', '%' . Input::get("busca") . '%');
            })
                ->orderBy('created_at', 'desc')
                ->paginate(500);
        } else {
            $itens = CotacoesAgricolas::orderby('CotCodigo', 'desc')
                ->paginate(15);
        }
        return view("painel.cotacoesagricolas.index", compact('itens'));
    }

    public function create()
    {
        $item = CotacoesAgricolas::orderby('CotCodigo', 'desc')
            ->limit(1)
            ->first();

        return view("painel.cotacoesagricolas.create", compact('item'));
    }


    public function create2(Request $request)
    {
        $create = new CotacoesAgricolas;

        $create->CotMilhoCompra = $request->input('milhocompra');
//        $create->CotMilhoVenda = $request->input('milhovenda');
        $create->CotSojaCompra = $request->input('sojacompra');
//        $create->CotSojaVenda = $request->input('sojavenda');
        $create->CotTrigoCompra = $request->input('trigocompra');
//        $create->CotTrigoVenda = $request->input('trigovenda');
        $create->CotTrigoPH = $request->input('trigoph');

        $create->save();

        $this->save_log("C", $create->getTable(), $create);

        return redirect(getenv("PAINEL") . '/cotacoesagricolas')->with('success', 'Registro incluído com sucesso!');
    }

    public function update($id)
    {
        $item = CotacoesAgricolas::where('ParCodigo', '=', $id)->firstOrFail();

        return view("painel.cotacoesagricolas.update", compact('item'));
    }

    public function update2(Request $request)
    {
        $id = $request->input('id');

        $update = CotacoesAgricolas::find($id);

        $update->CotMilhoCompra = $request->input('milhocompra');
//        $update->CotMilhoVenda = $request->input('milhovenda');
        $update->CotSojaCompra = $request->input('sojacompra');
//        $update->CotSojaVenda = $request->input('sojavenda');
        $update->CotTrigoCompra = $request->input('trigocompra');
//        $update->CotTrigoVenda = $request->input('trigovenda');
        $update->CotTrigoPH = $request->input('trigoph');

        $update->save();

        $this->save_log("U", $update->getTable(), $update);

        return redirect(getenv("PAINEL") . '/cotacoesagricolas')->with('success', 'Registro alterado com sucesso!');
    }

    public function upload(Request $request, Upload $upload, $id)
    {
        if (Input::hasFile('file')) {

            $upload->resize($request->file("file"), public_path() . "/upload/cotacoesagricolas/" . $id . ".jpg", getenv("TamPW"), getenv("TamPH"));

            $this->save_log("UP", "tParceiros", $id);

            return redirect(getenv("PAINEL") . '/cotacoesagricolas')->with("success", "Imagem enviada com sucesso!");
        } else {
            return redirect(getenv("PAINEL") . '/cotacoesagricolas')->with("error", "Imagem não enviada!");
        }
    }

    public function destroy($id)
    {
        CotacoesAgricolas::find($id)->delete();

        $this->save_log("D", "tcotacoes", $id);

        return redirect(getenv("PAINEL")."/cotacoesagricolas")->with('success', 'Registro excluido com sucesso!');
    }
}
