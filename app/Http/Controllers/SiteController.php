<?php namespace App\Http\Controllers;

use App\Galerias;
use App\GaleriasFotos;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Noticias;
use App\Orcamentos;
use App\OrcamentosArquivos;
use App\OrcamentosItens;
use App\Parceiros;
use App\ProdutosArquivos;
use App\ProdutosDepartamentos;
use App\ProgramasRadio;
use App\Services\Upload;
use App\UnidadesGaio;
use App\Vagas;
use App\VagasCandidatos;
use Illuminate\Http\Request;
use App\Banners;
use App\Contatos;
use App\Estatisticas;
use App\Paginas;
use App\PaginasFotos;
use App\Produtos;
use App\ProdutosCategorias;
use App\ProdutosFotos;
use App\Servicos;
use App\ServicosFotos;
use App\CotacoesAgricolas;
use File;
use Mail;
use Input;
use Session;

class SiteController extends Controller
{

    protected $banners;
    protected $parceiros;

    protected $blocos = array();

    public function __construct()
    {
        if (!Session::has('municipio')) {
            $unidades = UnidadesGaio::find(1);

            Session::put('municipio', $unidades->UniMunicipio);
            Session::put('telefone', $unidades->UniTelefone);
            Session::put('endereco', $unidades->UniEndereco);
            Session::put('tipo', $unidades->UniTipo);
            Session::put('unidadecodigo', $unidades->UniCodigo);
        }

        $this->banners = Banners::where('BanLiberado', '=', 1)
            ->orderBy('BanOrdem', 'asc')
            ->get();
        $this->parceiros = Parceiros::where('ParLiberado', '=', 1)->orderByRaw('RAND()')->get();

        $this->blocos["departamentos"] = ProdutosDepartamentos::where('DepLiberado', '=', 1)->orderBy('DepTitulo', 'asc')->get();
        $this->blocos["cerealistas"] = UnidadesGaio::where('UniTipo', '=', 0)->orderBy('UniMunicipio', 'asc')->get();
        $this->blocos["carrinho"] = OrcamentosItens::where('IteOrcCodigo', '=', session('orcamento'))->count();
        $this->blocos["unidades"] = UnidadesGaio::orderBy('UniMunicipio', 'asc')->get();

        $prodepartamentoslist = ProdutosDepartamentos::where('DepLiberado', '=', '1')
            ->orderBy('DepTitulo', 'asc')
            ->get();
        foreach ($prodepartamentoslist as $pdl) {
            $this->blocos["menus"][$pdl->DepCodigo] = $pdl;
            $this->blocos["menus"][$pdl->DepCodigo]["submenus"] = ProdutosCategorias::where('CatDepCodigo', '=', $pdl->DepCodigo)
                ->where('CatLiberado', '=', '1')
                ->orderBy('CatTitulo', 'asc')
                ->get();
        }
    }

    public function registro($pagina = "")
    {
        $estatisticas = new Estatisticas();
        $estatisticas->EstData = date("Y-m-d");
        $estatisticas->EstHora = date("H:i:s");
        $estatisticas->EstIp = $_SERVER["REMOTE_ADDR"];
        $estatisticas->EstPagina = $pagina;
        $estatisticas->save();
    }

    public function home()
    {
        $this->registro("HOME");
        $banners = $this->banners;
        $parceiros = $this->parceiros;
        $blocos = $this->blocos;

        $noticiasexternas = Noticias::where('NotLiberado', '=', 21)
            ->where('NotLink', '<>', '')
            ->orderBy('NotData', 'desc')
            ->get();

        $noticias = Noticias::where('NotLiberado', '=', 1)
            ->where('NotLink', '=', '')
            ->orderBy('NotData', 'desc')
            ->get();

        return view("site.home", compact('banners', 'blocos', 'parceiros', 'noticiasexternas', 'noticias'));
    }

    public function institucional()
    {
        $this->registro("INSTITUCIONAL");
        $blocos = $this->blocos;

        $pagina = Paginas::find(1);

        $destaque = 0;
        if (File::exists("upload/paginas/dest_" . $pagina->PagCodigo . ".jpg")) {
            $destaque = 1;
        }

        $fotos = PaginasFotos::where('FotPagCodigo', '=', $pagina->PagCodigo)->get();

        return view("site.paginas", compact('blocos', 'pagina', 'destaque', 'fotos'));
    }

    public function previsao()
    {
        $this->registro("PREVISÃO DO TEMPO");
        $blocos = $this->blocos;

        return view("site.previsao", compact('blocos'));
    }

    public function area_atuacao()
    {
        $this->registro("PREVISÃO DO TEMPO");
        $blocos = $this->blocos;

        $pagina = Paginas::find(3);

        $destaque = 0;
        if (File::exists("upload/paginas/dest_" . $pagina->PagCodigo . ".jpg")) {
            $destaque = 1;
        }

        $fotos = PaginasFotos::where('FotPagCodigo', '=', $pagina->PagCodigo)->get();

        return view("site.paginas", compact('blocos', 'pagina', 'destaque', 'fotos'));
    }

    public function produtos($dep = false, $cat = false)
    {
        $this->registro("PRODUTOS");
        $blocos = $this->blocos;

        $titulos = null;

        if (Input::has("cat") || $cat) {
            $produtos = Produtos::join('tprodutoscategorias', 'CatCodigo', '=', 'ProCatCodigo')
                ->join('tprodutosdepartamentos', 'DepCodigo', '=', 'CatDepCodigo')
                ->where('ProLiberado', '=', 1)
                ->where('CatCodigo', '=', (Input::has("cat")) ? Input::get("cat") : $cat)
                ->orderByRaw('RAND()')
                ->paginate(15);

            $titulos = ProdutosCategorias::where('CatCodigo', '=', (Input::has("cat")) ? Input::get("cat") : $cat)
                ->join('tprodutosdepartamentos', 'DepCodigo', '=', 'CatDepCodigo')
                ->first();

        } elseif (Input::has("dep") || $dep) {
            $produtos = Produtos::join('tprodutoscategorias', 'CatCodigo', '=', 'ProCatCodigo')
                ->join('tprodutosdepartamentos', 'DepCodigo', '=', 'CatDepCodigo')
                ->where('ProLiberado', '=', 1)
                ->where('DepCodigo', '=', (Input::has("dep")) ? Input::get("dep") : $dep)
                ->orderByRaw('RAND()')
                ->paginate(15);

            $titulos = ProdutosDepartamentos::where('DepCodigo', '=', (Input::has("dep")) ? Input::get("dep") : $dep)
                ->orderBy('DepTitulo', 'asc')
                ->first();

        } else {
            $produtos = Produtos::where('ProLiberado', '=', 1)
                ->orderByRaw('RAND()')
                ->paginate(15);
        }

        return view("site.produtos", compact('blocos', 'produtos', 'menus', 'titulos'));
    }

    public function produto_individual($id)
    {
        $this->registro("PRODUTO - ID:" . $id);
        $blocos = $this->blocos;

        $produto = Produtos::join('tprodutosmarcas', 'MarCodigo', '=', 'ProMarCodigo')
            ->where('ProLiberado', 1)
            ->where('ProCodigo', $id)
            ->firstOrFail();

        $destaque = 0;
        if (File::exists("upload/produtos/dest_" . $produto->ProCodigo . ".jpg")) {
            $destaque = 1;
        }

        $fotos = ProdutosFotos::where('FotProCodigo', '=', $produto->ProCodigo)->get();

        return view("site.produto-individual", compact('blocos', 'produto', 'destaque', 'fotos'));
    }

    public function insert_orcamento($id)
    {
        if (Session::has('orcamento')) {
//            VERIFICA SE O ITEM JÁ SE ENCONTRA NO CARRINHO
            $existente = OrcamentosItens::where('IteProCodigo', '=', $id)->where('IteOrcCodigo', '=', session('orcamento'))->count();

            if ($existente == 0) {
                $createitem = new OrcamentosItens();
                $createitem->IteOrcCodigo = session('orcamento');
                $createitem->IteProCodigo = $id;
                $createitem->IteQtde = 1;
                $createitem->save();

                return redirect("/orcamento")->with('success', 'Produto inserido no Carrinho de Orçamento!');
            }
            return redirect("/orcamento")->with('error', 'Produto já existente no Carrinho de Orçamento!');
        } else {
            $create = new Orcamentos();
            $create->OrcData = date("Y-m-d");
            $create->save();

            Session::put('orcamento', $create->OrcCodigo);

            $createitem = new OrcamentosItens();
            $createitem->IteOrcCodigo = session('orcamento');
            $createitem->IteProCodigo = $id;
            $createitem->IteQtde = 1;
            $createitem->save();

            return redirect("/orcamento")->with('success', 'Produto inserido no Carrinho de Orçamento!');
        }
    }

    public function qtde_orcamento(Request $request)
    {
        $id = $request->input('id');

        $update = OrcamentosItens::where('IteCodigo', '=', $id)
            ->where("IteOrcCodigo", "=", session('orcamento'))
            ->firstOrFail();

        if ($request->input('qtde') == "" || $request->input('qtde') == "0")
            $update->IteQtde = 1;
        else
            $update->IteQtde = $request->input('qtde');

        $update->save();

        return 1;
    }

    public function destroy_orcamento($id)
    {
        OrcamentosItens::where('IteCodigo', '=', $id)
            ->where("IteOrcCodigo", "=", session('orcamento'))
            ->delete();

        return redirect("/orcamento")->with('success', 'Produto removido do Carrinho de Orçamento!');
    }

    public function orcamento()
    {

        $this->registro("ORÇAMENTO - ID:" . session('orcamento'));
        $blocos = $this->blocos;

        if (Session::has('orcamento')) {

            $itens = OrcamentosItens::join('tprodutos', 'ProCodigo', '=', 'IteProCodigo')
                ->where('IteOrcCodigo', '=', session('orcamento'))
                ->orderBy('IteCodigo', 'desc')
                ->get();

            $contaCopel = 0;

            if(count(@$itens) > 0){
                foreach(@$itens as $item){
                    if($item->ProContaCopel == 1){$contaCopel = 1;}
                }
            }

            return view("site.orcamento", compact('blocos', 'itens', 'contaCopel'));
        } else {
            return redirect("/home");
        }
    }

    public function finaliza_orcamento(Request $request)
    {
        $update = Orcamentos::where('OrcCodigo', '=', session('orcamento'))
            ->firstOrFail();

        $update->OrcNome = $request->input('nome');
        $update->OrcMunicipio = $request->input('municipio');
        $update->OrcEstado = $request->input('estado');
        $update->OrcEndereco = $request->input('endereco');
        $update->OrcTelefone = $request->input('telefone');
        $update->OrcEmail = $request->input('email');
        $update->OrcObservacoes = $request->input('observacoes');
        $update->OrcStatus = 1;

        $update->save();

        if (Input::hasFile('file')) {

            $_UP['pasta'] = 'upload/orcamentos/';
            $_UP['extensoes'] = array('pdf', 'jpg', 'jpeg', 'png');

            $_UP['erros'][0] = 'Não houve erro';
            $_UP['erros'][1] = 'O arquivo no upload é maior do que o limite do PHP';
            $_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especifiado no HTML';
            $_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente';
            $_UP['erros'][4] = 'Não foi feito o upload do arquivo';

            if ($_FILES['file']['error'] != 0) {
                return redirect('/' )->with("error", "Não foi possível fazer o upload, erro:" . $_UP['erros'][$_FILES['file']['error']]);
            }

            $nome = explode('.', $_FILES['file']['name']);
            $extensao = strtolower($nome[count($nome) - 1]);

            if (array_search($extensao, $_UP['extensoes']) === false) {
                return redirect('/')->with("error", "Por favor, envie arquivos com as seguintes extensões: pdf, jpg, jpeg, png");
            }

            $nome_final = 'conta_' . $update->OrcCodigo . '.' . $extensao;

            if (move_uploaded_file($_FILES['file']['tmp_name'], $_UP['pasta'] . $nome_final)) {

                $arquivo = new OrcamentosArquivos();
                $arquivo->ArqArquivo = "conta_" . $update->OrcCodigo . "." . $request->file("file")->getClientOriginalExtension();
                $arquivo->ArqOrcCodigo = $update->OrcCodigo;
                $arquivo->ArqTitulo = "Conta COPEL - Orçamento" . $update->OrcCodigo;

                $arquivo->save();

                $this->save_log("UP", $arquivo->getTable(), $arquivo);

            } else {
                return redirect('/' )->with("error", "Erro Desconhecido: Anexo não enviado!");
            }

        }

        $itens = OrcamentosItens::join('tprodutos', 'ProCodigo', '=', 'IteProCodigo')
            ->where('IteOrcCodigo', '=', session('orcamento'))
            ->orderBy('IteCodigo', 'desc')
            ->get();

        $dados = [
            'nome' => $request->input("nome"),
            'cnpjcpf' => $request->input("cnpjcpf"),
            'municipio' => $request->input("municipio"),
            'estado' => $request->input("estado"),
            'endereco' => $request->input("endereco"),
            'telefone' => $request->input("telefone"),
            'celular' => $request->input("celular"),
            'email' => $request->input("email"),
            'observacoes' => $request->input("observacoes"),
            'itens' => $itens
        ];

        Mail::send('emails.orcamento', $dados, function ($m) use ($request) {
            $m->from(getenv("MAIL_USERNAME"), $request->input("nome"))->replyTo($request->input("email"));
            // $m->to(getenv("MAIL_CONTATO"), null)->subject('Solicitação de Orçamento - www.gaioagro.com.br!');
            $m->to(getenv("MAIL_CONTATO2"), null)->subject('Solicitação de Orçamento - www.gaioagro.com.br!');
        });

        Session::forget('orcamento');

        return redirect("/home")->with('success', 'Solicitação de Orçamento enviada com sucesso!');
    }

    public function seleciona_unidade(Request $request)
    {
        $unidades = UnidadesGaio::find($request->input('id'));

        Session::put('municipio', $unidades->UniMunicipio);
        Session::put('telefone', $unidades->UniTelefone);
        Session::put('tipo', $unidades->UniTipo);
        Session::put('endereco', $unidades->UniEndereco);
        Session::put('unidadecodigo', $unidades->UniCodigo);

        return redirect("/home");
    }

    public function cerealista($id)
    {
        $this->registro("CEREALISTA - ID:" . $id);
        $blocos = $this->blocos;
        $cotacao = CotacoesAgricolas::orderby('CotCodigo', 'desc')
            ->limit(1)
            ->first();

//        PRIMEIRA CEREALISTA SEMPRE A SER EXIBIDA
        $cerprincipal = UnidadesGaio::find($id);
        $pagprincipal = null;
        if ($cerprincipal->UniPagCodigo != 0) {
            $pagprincipal = Paginas::find($cerprincipal->UniPagCodigo);

            $destprincipal = 0;
            if (File::exists("upload/paginas/dest_" . $pagprincipal->PagCodigo . ".jpg")) {
                $destprincipal = 1;
            }
            $fotosprincipal = PaginasFotos::where('FotPagCodigo', '=', $pagprincipal->PagCodigo)->get();
        }

//        DEMAIS CEREALISTAS
        $cersecundarias = UnidadesGaio::where('UniCodigo', '<>', $id)
            ->where('UniTipo', '=', '0')
            ->orderBy('UniMunicipio', 'asc')
            ->get();

        $cerealistas = null;
        foreach ($cersecundarias as $cer) {
            $cerealistas[$cer->UniCodigo] = $cer;
            $cerealistas[$cer->UniCodigo]["paginas"] = Paginas::where('PagCodigo', '=', $cer->UniPagCodigo)->first();

            if (sizeof($cerealistas[$cer->UniCodigo]["paginas"]) > 0) {
                $cerealistas[$cer->UniCodigo]["destaque"] = 0;
                if (File::exists("upload/paginas/dest_" . $cerealistas[$cer->UniCodigo]["paginas"]->PagCodigo . ".jpg")) {
                    $cerealistas[$cer->UniCodigo]["destaque"] = 1;
                }

                $cerealistas[$cer->UniCodigo]["fotos"] = PaginasFotos::where('FotPagCodigo', '=', $cerealistas[$cer->UniCodigo]["paginas"]->PagCodigo)
                    ->get();
            }
        }


        return view("site.cerealista", compact('cerprincipal', 'pagprincipal', 'destprincipal', 'fotosprincipal', 'cerealistas', 'blocos', 'cotacao'));
    }

    public function unidades()
    {
        $this->registro("UNIDADES");
        $blocos = $this->blocos;

        $unidades = UnidadesGaio::orderBy("UniMunicipio", 'asc')->get();

        return view("site.unidades", compact('unidades', 'blocos'));
    }

    public function programas_radio()
    {
        $this->registro("PROGRAMAS DE RÁDIO");
        $blocos = $this->blocos;

        $programas = ProgramasRadio::where('ProLiberado', '=', 1)
            ->orderBy("ProData", 'desc')->get();

        return view("site.programasradio", compact('programas', 'blocos'));
    }

    public function programa_download($id)
    {
        $download = ProgramasRadio::where("ProCodigo", "=", $id)
            ->first();

        $file = public_path() . "/upload/programasradio/" . $download->ProArquivo;

        $fileinfo = pathinfo($file);
        header('Content-Disposition: attachment; filename="' . $download->ProCodigo . "." . $fileinfo["extension"] . '"');
        header('Content-Length: ' . filesize($file));
        ob_clean();
        flush();
        readfile($file);
        exit;
    }

    public function trabalhe_conosco()
    {
        $this->registro("TRABALHE CONOSCO");
        $blocos = $this->blocos;

        return view("site.trabalhe-conosco", compact('blocos'));
    }

    public function trabalhe_conosco_insert(Request $request)
    {
        //verify captcha
        $recaptcha_secret = getenv("RECAPCHA_SECRET");

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array(
            'secret' => $recaptcha_secret,
            'response' => @$request->input('g-recaptcha-response'),
            'remoteIp' => $_SERVER['REMOTE_ADDR']
        )));
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 60);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
        $response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($response, true);

        if ($response["success"]) {

            $create = new Contatos;

            $create->ConNome = $request->input('nome');
            $create->ConEmail = $request->input('email');
            $create->ConTelefone = $request->input('telefone');
            $create->ConTipo = 1;
            $create->ConConteudo = view("emails.trabalheconosco", [
                'nome' => $request->input("nome"),
                'datanascimento' => $request->input("datanascimento"),
                'sexo' => $request->input("sexo"),
                'municipio' => $request->input("municipio"),
                'estado' => $request->input("estado"),
                'endereco' => $request->input("endereco"),
                'telefone' => $request->input("telefone"),
                'celular' => $request->input("celular"),
                'email' => $request->input("email"),
                'cargo' => $request->input("cargo"),
                'formacao' => $request->input("formacao"),
                'experiencia' => $request->input("experiencia")
            ]);

            $create->save();

            if($request->hasFile('file')){

                $file = $request->file('file');

                $fileName = 'curriculo_' . $create->ConCodigo. ".pdf";
                $path = 'upload/trabalheconosco/';
                $file->move(public_path($path), $fileName);

            }else{
                $path = "";
                $fileName = "";
            }

            Mail::send('emails.trabalheconosco', ['nome' => $request->input("nome"),
                'datanascimento' => $request->input("datanascimento"),
                'sexo' => $request->input("sexo"),
                'municipio' => $request->input("municipio"),
                'estado' => $request->input("estado"),
                'endereco' => $request->input("endereco"),
                'telefone' => $request->input("telefone"),
                'celular' => $request->input("celular"),
                'email' => $request->input("email"),
                'cargo' => $request->input("cargo"),
                'formacao' => $request->input("formacao"),
                'experiencia' => $request->input("experiencia")], function ($m) use ($request, $path, $fileName) {
                
                $m->from(getenv("MAIL_USERNAME"), $request->input("nome"))->replyTo($request->input("email"));

                if(strlen(@$path) > 0) {
                    $m->attach(public_path($path) . $fileName);
                }
                $m->to(getenv("MAIL_TRABALHE"), null)->subject('Trabalhe Conosco - www.gaioagro.com.br!');
            });
            return redirect("/trabalhe-conosco")->with('success', "Formulário enviado com sucesso!");
        } else {
            return redirect(url("/trabalhe-conosco"))->with('error', 'Falha na checagem "Não sou um robô"');
        }
    }

    public function vagas_de_emprego()
    {
        $this->registro("VAGAS DE EMPREGO");
        $blocos = $this->blocos;

        $vagas = Vagas::where("liberado", '=', 1)
            ->where('unidade', '=', Session::get('municipio'))
            ->orderBy("created_at", "desc")
            ->get();

        return view("site.vagas-de-emprego", compact('blocos', 'vagas'));
    }

    public function vaga($id)
    {
        $blocos = $this->blocos;

        $vaga = Vagas::find($id);
        $this->registro("VAGAS DE EMPREGO - ".$vaga->titulo);

        $vagas = Vagas::where("liberado", '=', 1)
            ->where('unidade', '=', Session::get('municipio'))
            ->orderBy("created_at", "desc")
            ->get();

        return view("site.vaga", compact('blocos', 'vaga', "vagas"));
    }

    public function descricao_vaga($id)
    {
        $blocos = $this->blocos;

        $vaga = Vagas::find($id);

        $this->registro("VAGAS DE EMPREGO - ".$vaga->titulo);

        return view("site.descricao-vaga", compact('blocos', 'vaga'));
    }

    public function vagas_de_emprego_insert(Request $request)
    {
        //verify captcha
        $recaptcha_secret = getenv("RECAPCHA_SECRET");

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array(
            'secret' => $recaptcha_secret,
            'response' => @$request->input('g-recaptcha-response'),
            'remoteIp' => $_SERVER['REMOTE_ADDR']
        )));
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 60);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
        $response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($response, true);

        $vaga = Vagas::find($request->input('vaga'));

        if ($response["success"]) {

            $create = new VagasCandidatos();

            $create->vaga_id = $request->input('vaga');
            $create->nome = $request->input('nome');
            $create->nascimento = $request->input('datanascimento');
            $create->sexo = $request->input('sexo');
            $create->municipio = $request->input('municipio');
            $create->uf = $request->input('estado');
            $create->endereco = $request->input('endereco');
            $create->telefone = $request->input('telefone');
            $create->celular = $request->input('celular');
            $create->email = $request->input('email');
            $create->cargopretendido = $request->input('cargo');
            $create->formacao = $request->input('formacao');
            $create->experiencia = $request->input('experiencia');
            $create->conteudo = view("emails.vagas", [
                'nome' => $request->input("nome"),
                'datanascimento' => $request->input("datanascimento"),
                'sexo' => $request->input("sexo"),
                'municipio' => $request->input("municipio"),
                'estado' => $request->input("estado"),
                'endereco' => $request->input("endereco"),
                'telefone' => $request->input("telefone"),
                'celular' => $request->input("celular"),
                'email' => $request->input("email"),
                'cargo' => $request->input("cargo"),
                'formacao' => $request->input("formacao"),
                'experiencia' => $request->input("experiencia"),
                'vaga' => $vaga
            ]);

            $create->save();

            if($request->hasFile('file')){

                $file = $request->file('file');

                $fileName = 'curriculo_' . $create->id. ".pdf";
                $path = 'upload/vagas/';
                $file->move(public_path($path), $fileName);

            }else{
                $path = "";
                $fileName = "";
            }

            Mail::send('emails.vagas', ['nome' => $request->input("nome"),
                'datanascimento' => $request->input("datanascimento"),
                'sexo' => $request->input("sexo"),
                'municipio' => $request->input("municipio"),
                'estado' => $request->input("estado"),
                'endereco' => $request->input("endereco"),
                'telefone' => $request->input("telefone"),
                'celular' => $request->input("celular"),
                'email' => $request->input("email"),
                'cargo' => $request->input("cargo"),
                'formacao' => $request->input("formacao"),
                'experiencia' => $request->input("experiencia"),
                'vaga' => $vaga], function ($m) use ($request, $path, $fileName) {

                $m->from(getenv("MAIL_USERNAME"), $request->input("nome"))->replyTo($request->input("email"));

                if(strlen($path) > 0) {
                    $m->attach(public_path($path) . $fileName);
                }

                $m->to(getenv("MAIL_TRABALHE"), null)->subject('Vagas de Emprego - www.gaioagro.com.br!');
            });
            return redirect("/vaga/".$vaga->id)->with('success', "Formulário enviado com sucesso!");
        } else {
            return redirect(url("/vaga/".$vaga->id))->with('error', 'Falha na checagem "Não sou um robô"');
        }
    }

    public function contato()
    {
        $this->registro("CONTATO");
        $blocos = $this->blocos;

        return view("site.contato", compact('blocos'));
    }

    public function contato2(Request $request)
    {
        //verify captcha
        $recaptcha_secret = getenv("RECAPCHA_SECRET");

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array(
            'secret' => $recaptcha_secret,
            'response' => @$request->input('g-recaptcha-response'),
            'remoteIp' => $_SERVER['REMOTE_ADDR']
        )));
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 60);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
        $response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($response, true);

        if ($response["success"]) {

            $contato = [
                'nome' => Input::get("nome"),
                'email' => Input::get("email"),
                'telefone' => Input::get("telefone"),
                'mensagem' => Input::get("mensagem")
            ];

            $create = new Contatos();

            $create->ConNome = Input::get('nome');
            $create->ConEmail = Input::get('email');
            $create->ConTelefone = Input::get('telefone');
            $create->ConMensagem = Input::get('mensagem');
            $create->ConConteudo = view("emails.contato", $contato);

            $create->save();

            Mail::send('emails.contato', $contato, function ($m) use ($request) {
                $m->from(getenv("MAIL_USERNAME"), $request->input("nome"))->replyTo(Input::get("email"));

                $m->to(getenv("MAIL_CONTATO"), null)->subject('Mensagem do Site - ' . getenv("SITE") . '/contato!');
            });
            return redirect(url("/contato#"))->with('success', "Mensagem enviada com Sucesso!");
        } else {
            return redirect(url("/contato#"))->with('error', 'Falha na checagem "Não sou um robô"');
        }
    }
//    {
//        $create = new Contatos;
//
//        $create->ConNome = $request->input('nome');
//        $create->ConEmail = $request->input('email');
//        $create->ConTelefone = $request->input('telefone');
//        $create->ConMensagem = $request->input('mensagem');
//        $create->ConConteudo = view("emails.contato", [
//            'nome' => $request->input("nome"),
//            'email' => $request->input("email"),
//            'telefone' => $request->input("telefone"),
//            'mensagem' => $request->input("mensagem")
//        ]);
//
//        $create->save();
//
//        Mail::send('emails.contato', ['nome' => $request->input("nome"), 'email' => $request->input("email"), 'telefone' => $request->input("telefone"), 'municipio' => $request->input("municipio"), 'mensagem' => $request->input("mensagem")], function ($m) use ($request) {
//            $m->from(getenv("MAIL_USERNAME"));
////                ->replyTo($request->input("email"));
//
//            $m->to(getenv("MAIL_CONTATO"), null)->subject('Mensagem do Site - www.gaioagro.com.br!');
//        });
//        return redirect("/contato")->with('success', "Mensagem Enviada com Sucesso");
//    }

    public function galerias()
    {
        $this->registro("GALERIAS");
        $blocos = $this->blocos;

        $itens = Galerias::where('GalLiberado', '=', 1)
            ->orderBy('GalData', 'desc')
            ->paginate(15);

        return view("site.galerias", compact('itens', 'blocos'));
    }

    public function galeria($id)
    {
        $this->registro("GALERIA - ID = " . $id);
        $blocos = $this->blocos;

        $galeria = Galerias::where('GalCodigo', '=', $id)
            ->where('GalLiberado', '=', 1)
            ->firstOrFail();

        $itens = GaleriasFotos::where('FotGalCodigo', '=', $id)
            ->orderBy('FotCodigo', 'desc')
            ->get();

        return view("site.galeria", compact('itens', 'galeria', 'blocos'));
    }

    public function noticias()
    {
        $this->registro("NOTÍCIAS");
        $blocos = $this->blocos;

        $noticias = Noticias::where('NotLiberado', '=', 1)
            ->orderBy('NotData', 'desc')
            ->paginate(15);

        return view("site.noticias", compact('noticias', 'blocos'));
    }

    public function noticia($id)
    {
        $this->registro("NOTÍCIA - ID = " . $id);
        $blocos = $this->blocos;

        $noticia = Noticias::where('NotLiberado', 1)
            ->where('NotCodigo', '=', $id)
            ->firstOrFail();

        return view("site.noticia", compact('noticia', 'blocos'));

    }

    public function cotacao()
    {
        $this->registro("COTAÇÕES AGRÍCOLAS");
        $blocos = $this->blocos;
        $cotacao = CotacoesAgricolas::orderby('CotCodigo', 'desc')
            ->limit(1)
            ->first();

        return view("site.cotacao", compact('cotacao', 'blocos'));
    }
}
