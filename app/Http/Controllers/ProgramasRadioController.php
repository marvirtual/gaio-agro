<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Upload;
use App\ProgramasRadio;
use Input;
use File;


class ProgramasRadioController extends Controller
{

    public function index()
    {
        if (Input::has("busca")) {
            $itens = ProgramasRadio::where('ProData', 'like', '%' . Input::get("busca") . '%')
                ->orderBy('created_at', 'desc')
                ->paginate(500);
        } else {
            $itens = ProgramasRadio::orderBy('created_at', 'desc')
                ->paginate(15);
        }
        return view("painel.programasradio.index", compact('itens'));
    }

    public function create()
    {
        return view("painel.programasradio.create");
    }

    public function create2(Request $request)
    {
        $create = new ProgramasRadio;

        $create->ProData = implode("-", array_reverse(explode("/", $request->input('data'))));
        $create->ProLiberado = $request->input('liberado');

        $create->save();

        $this->save_log("C", $create->getTable(), $create);

        return redirect(getenv("PAINEL")."/programasradio")->with('success', 'Registro incluído com sucesso!');
    }

    public function update($id)
    {
        $item = ProgramasRadio::where('ProCodigo', '=', $id)->firstOrFail();
        return view("painel.programasradio.update", compact('item'));
    }

    public function update2(Request $request)
    {
        $id = $request->input('id');

        $update = ProgramasRadio::find($id);

        $update->ProData = implode("-", array_reverse(explode("/", $request->input('data'))));
        $update->ProLiberado = $request->input('liberado');

        $update->save();

        $this->save_log("U", $update->getTable(), $update);

        return redirect(getenv("PAINEL")."/programasradio")->with('success', 'Registro alterado com sucesso!');
    }

    public function destroy($id)
    {
        ProgramasRadio::find($id)->delete();

        $this->save_log("D", "tprogramasradio", $id);

        return redirect(getenv("PAINEL")."/programasradio")->with('success', 'Registro excluido com sucesso!');
    }

    public function upload(Request $request, $id)
    {
        if (Input::hasFile('file')) {
            $request->file("file")->move("upload/programasradio", $id . "." . $request->file("file")->getClientOriginalExtension());

            $arquivo2 = ProgramasRadio::find($id);
            $arquivo2->ProArquivo = $id . "." . $request->file("file")->getClientOriginalExtension();
            $arquivo2->save();

            $this->save_log("UP", $arquivo2->getTable(), $arquivo2);

            return redirect(getenv("PAINEL") . '/programasradio')->with("success", "Arquivo enviado com sucesso!");
        } else {
            return redirect(getenv("PAINEL") . '/programasradio')->with("error", "Arquivo não enviado!");
        }
    }
}
