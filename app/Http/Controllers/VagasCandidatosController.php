<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\UnidadesGaio;
use App\Vagas;
use App\VagasCandidatos;
use Illuminate\Http\Request;
use App\Services\Upload;
use App\Noticias;
use Input;
use File;
use Route;


class VagasCandidatosController extends Controller
{
    public function index()
    {
        if (Input::has("busca")) {
            $itens = VagasCandidatos::where(function ($query) {
                $query->where('nome', 'like', '%' . Input::get("busca") . '%')
                    ->orWhere('conteudo', 'like', '%' . Input::get("busca") . '%');
            })->where('vaga_id', '=', Route::input("id"))
                ->orderBy('created_at', 'desc')
                ->paginate(500);
        } else {
            $itens = VagasCandidatos::where('vaga_id', '=', Route::input("id"))->orderBy('created_at', 'desc')->paginate(15);
        }

        $vaga = Vagas::find(Route::input("id"));

        return view("painel.vagas.candidatos.index", compact('itens', 'vaga'));
    }

    public function view()
    {
        $item = VagasCandidatos::find(Route::input("id_candidato"));
        $vaga = Vagas::find(Route::input("id"));
        $retorno = getenv("painel")."/vagas/".Route::input("id")."/candidatos";

        return view("painel.vagas.candidatos.view", compact('item', 'vaga', 'retorno'));
    }

    public function view2()
    {
        $item = VagasCandidatos::find(Route::input("id_candidato"));
        $vaga = Vagas::find(Route::input("id"));
        $retorno = getenv("painel")."/trabalheconosco";

        return view("painel.vagas.candidatos.view", compact('item', 'vaga', 'retorno'));
    }

    public function destroy()
    {
        $item = VagasCandidatos::find(Route::input("id_candidato"));
        $item->delete();

        return back()->with("success", "registro excluído com sucesso");
    }

    public function entrevista()
    {
        $item = VagasCandidatos::find(Route::input("id_candidato"));

        $data = implode('-', array_reverse(explode("/", Input::get("data"))));
        $hora = Input::get("hora").":00";

        $item->dataentrevista = $data." ".$hora;

        $item->save();

        $vaga = Vagas::find($item->vaga_id);
        $itens = VagasCandidatos::where('vaga_id', '=', $item->vaga_id)->orderBy('created_at', 'desc')->paginate(15);

        return view("painel.vagas.candidatos.index", compact('itens', 'vaga'));
    }

    public function contratacao()
    {
        $item = VagasCandidatos::find(Route::input("id_candidato"));

        $data = implode('-', array_reverse(explode("/", Input::get("data"))));
        $hora = Input::get("hora").":00";

        $item->datacontratacao = $data." ".$hora;

        $item->save();

        $vaga = Vagas::find($item->vaga_id);

        $vaga->liberado = 0;

        $vaga->save();

        $itens = VagasCandidatos::where('vaga_id', '=', $item->vaga_id)->orderBy('created_at', 'desc')->paginate(15);

        return view("painel.vagas.candidatos.index", compact('itens', 'vaga'));
    }
}
