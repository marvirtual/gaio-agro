<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ProdutosDepartamentos;
use Illuminate\Http\Request;
use App\Services\Upload;
use App\ProdutosCategorias;
use Input;


class ProdutosDepartamentosController extends Controller
{

    public function __construct()
    {

    }

    public function index()
    {
        if (Input::has("busca")) {
            $itens = ProdutosDepartamentos::where(function ($query) {
                $query->where('DepTitulo', 'like', '%' . Input::get("busca") . '%');
            })
                ->orderBy('created_at', 'desc')
                ->paginate(500);
        } else {
            $itens = ProdutosDepartamentos::orderBy('created_at', 'desc')->paginate(15);
        }

        return view("painel.produtosdepartamentos.index", compact('itens'));
    }

    public function create()
    {
        return view("painel.produtosdepartamentos.create");
    }

    public function create2(Request $request)
    {
        $create = new ProdutosDepartamentos();

        $create->DepTitulo = $request->input('titulo');
        $create->DepLiberado = $request->input('liberado');

        $create->save();

        $this->save_log("C", $create->getTable(), $create);

        return redirect(getenv("PAINEL")."/produtosdepartamentos")->with('success', 'Registro incluído com sucesso!');
    }

    public function update($id)
    {
        $item = ProdutosDepartamentos::where('DepCodigo', '=', $id)
            ->firstOrFail();

        return view("painel.produtosdepartamentos.update", compact('item'));
    }

    public function update2(Request $request)
    {
        $id = $request->input('id');

        $update = ProdutosDepartamentos::find($id);

        $update->DepTitulo = $request->input('titulo');
        $update->DepLiberado = $request->input('liberado');

        $update->save();

        $this->save_log("U", $update->getTable(), $update);

        return redirect(getenv("PAINEL")."/produtosdepartamentos")->with('success', 'Registro alterado com sucesso!');
    }

    public function destroy($id)
    {
        ProdutosDepartamentos::find($id)->delete();

        $this->save_log("D", "produtosdepartamentos", $id);

        return redirect(getenv("PAINEL")."/produtosdepartamentos")->with('success', 'Registro excluido com sucesso!');
    }
}
