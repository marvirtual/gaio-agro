<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ProdutosCategorias;
use App\ProdutosDepartamentos;
use App\ProdutosMarcas;
use Illuminate\Http\Request;
use App\Services\Upload;
use App\Produtos;
use App\ProdutosFotos;
use App\ProdutosArquivos;
use Input;
use File;
use Validator;

class ProdutosController extends Controller
{

    public function index()
    {
        if (Input::has("busca")) {
            $itens = Produtos::where(function ($query) {
                $query->where('ProTitulo', 'like', '%' . Input::get("busca") . '%')
                    ->orWhere('ProConteudo', 'like', '%' . Input::get("busca") . '%');
            })
                ->join('tprodutoscategorias', 'ProCatCodigo', '=', 'CatCodigo')
                ->orderBy('tprodutos.created_at', 'desc')
                ->paginate(500);
        } else {
            $itens = Produtos::orderBy('tprodutos.created_at', 'desc')
                ->join('tprodutoscategorias', 'ProCatCodigo', '=', 'CatCodigo')
                ->paginate(15);
        }

        return view("painel.produtos.index", compact('itens'));

    }

    public function fotos($id)
    {
        $itens = ProdutosFotos::where('FotProCodigo', '=', $id)
            ->orderBy('created_at', 'desc')
            ->paginate(15);

        return view("painel.produtos.fotos", compact('itens', 'id'));
    }

    public function arquivos($id)
    {
        $itens = ProdutosArquivos::where('ArqProCodigo', '=', $id)
            ->orderBy('created_at', 'desc')
            ->paginate(15);

        return view("painel.produtos.arquivosindex", compact('itens', 'id'));

    }

    public function create()
    {
        $departamentos = ProdutosDepartamentos::orderBy('created_at', 'desc')
            ->get();

        $marcas = ProdutosMarcas::orderBy('created_at', 'desc')
            ->get();

        return view("painel.produtos.create", compact('departamentos','marcas'));
    }

    public function arquivoscreate($id)
    {
        return view("painel.produtos.arquivoscreate", compact('id'));
    }

    public function create2(Request $request)
    {
        $create = new Produtos;

        $create->ProTitulo = $request->input('titulo');
        $create->ProValor = $request->input('valor');
        $create->ProCatCodigo = $request->input('categoria');
        $create->ProMarCodigo = $request->input('marca');
        $create->ProConteudo = $request->input('conteudo');
        $create->ProLiberado = $request->input('liberado');

        $create->save();

        $this->save_log("C", $create->getTable(), $create);

        return redirect(getenv("PAINEL") . '/produtos')->with('success', 'Registro incluído com sucesso!');
    }

    public function arquivoscreate2(Request $request)
    {
        $id = $request->input('id');

        $create = new ProdutosArquivos();

        $create->ArqTitulo = $request->input('titulo');
        $create->ArqProCodigo = $id;

        $create->save();

        $this->save_log("C", $create->getTable(), $create);

        return redirect(getenv("PAINEL") . '/produtos/arquivos/'.$id)->with('success', 'Registro incluído com sucesso!');
    }

    public function update($id)
    {
        $item = Produtos::join('tprodutoscategorias', 'ProCatCodigo', '=', 'CatCodigo')
            ->join('tprodutosdepartamentos', 'DepCodigo', '=', 'CatDepCodigo')
            ->join('tprodutosmarcas', 'MarCodigo', '=', 'ProMarCodigo')
            ->where('ProCodigo', '=', $id)
            ->firstOrFail();

        $departamentos = ProdutosDepartamentos::orderBy('created_at', 'desc')
            ->get();

        $categorias = ProdutosCategorias::where('CatDepCodigo','=',$item->DepCodigo)
            ->orderBy('created_at', 'desc')
            ->get();

        $marcas = ProdutosMarcas::orderBy('created_at', 'desc')
            ->get();

        return view("painel.produtos.update", compact('item', 'departamentos', 'categorias', 'marcas'));
    }

    public function update2(Request $request)
    {
        $id = $request->input('id');

        $update = Produtos::find($id);

        $update->ProTitulo = $request->input('titulo');
        $update->ProValor = $request->input('valor');
        $update->ProCatCodigo = $request->input('categoria');
        $update->ProMarCodigo = $request->input('marca');
        $update->ProConteudo = $request->input('conteudo');
        $update->ProLiberado = $request->input('liberado');

        $update->save();

        $this->save_log("U", $update->getTable(), $update);

        return redirect(getenv("PAINEL") . '/produtos')->with('success', 'Registro alterado com sucesso!');
    }

    public function fotosupdate($id, $idfoto)
    {
        $item = ProdutosFotos::where('FotCodigo', '=', $idfoto)
            ->firstOrFail();

        return view("painel.produtos.fotosupdate", compact('item', 'id', 'idfoto'));
    }

    public function fotosupdate2(Request $request)
    {
        $id = $request->input('id');
        $idfoto = $request->input('idfoto');

        $update = ProdutosFotos::find($idfoto);

        $update->FotLegenda = $request->input('legenda');

        $update->save();

        $this->save_log("U", $update->getTable(), $update);

        return redirect(getenv("PAINEL") . '/produtos/' . $id)->with('success', 'Registro alterado com sucesso!');
    }

    public function arquivosupdate($id, $idarquivo)
    {
        $item = ProdutosArquivos::where('ArqCodigo', '=', $idarquivo)
            ->firstOrFail();

        return view("painel.produtos.arquivosupdate", compact('item', 'id', 'idarquivo'));
    }

    public function arquivosupdate2(Request $request)
    {
        $id = $request->input('id');
        $idarquivo = $request->input('idarquivo');

        $update = ProdutosArquivos::find($idarquivo);

        $update->ArqTitulo = $request->input('titulo');

        $update->save();

        $this->save_log("U", $update->getTable(), $update);

        return redirect(getenv("PAINEL") . '/produtos/arquivos/' . $id)->with('success', 'Registro alterado com sucesso!');
    }

    public function upload_arte(Request $request, Upload $upload, $id)
    {
        if (Input::hasFile('file')) {

            $upload->resize($request->file("file"), public_path() . "/upload/produtos/arte_" . $id . ".jpg", getenv("TamAW"), getenv("TamAH"));

            $this->save_log("UP", "tProdutos", $id);

            return redirect(getenv("PAINEL") . '/produtos')->with("success", "Imagem enviada com sucesso!");
        } else {
            return redirect(getenv("PAINEL") . '/produtos')->with("error", "Imagem não enviada!");
        }
    }

    public function destroy($id)
    {
        $itens = ProdutosFotos::where('FotProCodigo', '=', $id)->get();

        foreach ($itens as $item) {
//            if (File::exists("upload/produtos/p_" . $item->FotCodigo . ".jpg")) {
//                File::delete("upload/produtos/p_" . $item->FotCodigo . ".jpg");
//            }
//            if (File::exists("upload/produtos/g_" . $item->FotCodigo . ".jpg")) {
//                File::delete("upload/produtos/g_" . $item->FotCodigo . ".jpg");
//            }

            ProdutosFotos::find($item->FotCodigo)->delete();

            $this->save_log("D", "tProdutosFotos", $item->FotCodigo);
        }

        $itens2 = ProdutosArquivos::where('ArqProCodigo', '=', $id)->get();

        foreach ($itens2 as $item) {
//            if (File::exists("upload/produtosarquivos/" . $item->ArqArquivo)) {
//                File::delete("upload/produtosarquivos/" . $item->ArqArquivo);
//            }
            ProdutosArquivos::find($item->ArqCodigo)->delete();

            $this->save_log("D", "tProdutosArquivos", $item->ArqCodigo);
        }

//        if (File::exists("upload/produtos/dest_" . $id . ".jpg")) {
//            File::delete("upload/produtos/dest_" . $id . ".jpg");
//        }
        Produtos::find($id)->delete();

        $this->save_log("D", "tProdutos", $id);

        return redirect(getenv("PAINEL") . '/produtos')->with('success', 'Registro excluido com sucesso!');
    }

    public function fotosdestroy($id, $idfoto)
    {
//        if (File::exists("upload/produtos/p_" . $idfoto . ".jpg")) {
//            File::delete("upload/produtos/p_" . $idfoto . ".jpg");
//        }
//        if (File::exists("upload/produtos/g_" . $idfoto . ".jpg")) {
//            File::delete("upload/produtos/g_" . $idfoto . ".jpg");
//        }
        ProdutosFotos::find($idfoto)->delete();

        $this->save_log("D", "tProdutosFotos", $idfoto);

        return redirect(getenv("PAINEL") . '/produtos/' . $id)->with('success', 'Registro excluido com sucesso!');
    }

    public function arquivosdestroy($id, $idarquivo)
    {
        $arquivo = ProdutosArquivos::find($idarquivo);

//        if (File::exists("upload/produtosarquivos/" . $arquivo->ArqArquivo)) {
//            File::delete("upload/produtosarquivos/" . $arquivo->ArqArquivo);
//        }

        ProdutosArquivos::find($idarquivo)->delete();

        $this->save_log("D", "tProdutosArquivos", $idarquivo);

        return redirect(getenv("PAINEL") . '/produtos/arquivos/' . $id)->with('success', 'Registro excluido com sucesso!');
    }

    public function upload(Request $request, Upload $upload, $id)
    {
        if (Input::hasFile('file')) {

            $upload->resize($request->file("file"), public_path() . "/upload/produtos/dest_" . $id . ".jpg", getenv("TamGW"), getenv("TamGH"));

            $this->save_log("UP", "tProdutos", $id);

            return redirect(getenv("PAINEL") . '/produtos')->with("success", "Imagem enviada com sucesso!");
        } else {
            return redirect(getenv("PAINEL") . '/produtos')->with("error", "Imagem não enviada!");
        }
    }

    public function fotosupload(Request $request, Upload $upload, $id, $idfoto)
    {
        if (Input::hasFile('file')) {

            $upload->resize($request->file("file"), public_path() . "/upload/produtos/g_" . $idfoto . ".jpg", getenv("TamGW"), getenv("TamGH"));
            $upload->resize($request->file("file"), public_path() . "/upload/produtos/p_" . $idfoto . ".jpg", getenv("TamPW"), getenv("TamPH"));

            $this->save_log("UP", "tProdutosFotos", $idfoto);

            return redirect(getenv("PAINEL") . '/produtos/' . $id)->with("success", "Imagem enviada com sucesso!");
        } else {
            return redirect(getenv("PAINEL") . '/produtos/' . $id)->with("error", "Imagem não enviada!");
        }
    }

    public function arquivosupload(Request $request, Upload $upload, $id, $idarquivo)
    {
        if (Input::hasFile('file')) {
            $request->file("file")->move("upload/produtosarquivos", $idarquivo . "." . $request->file("file")->getClientOriginalExtension());

            $arquivo2 = ProdutosArquivos::find($idarquivo);
            $arquivo2->ArqArquivo = $idarquivo . "." . $request->file("file")->getClientOriginalExtension();
            $arquivo2->save();

            $this->save_log("UP", $arquivo2->getTable(), $arquivo2);

            return redirect(getenv("PAINEL") . '/produtos/arquivos/' . $id)->with("success", "Arquivo enviado com sucesso!");
        } else {
            return redirect(getenv("PAINEL") . '/produtos/arquivos/' . $id)->with("error", "Arquivo não enviado!");
        }
    }

    public function fotosmultiploupload(Request $request, Upload $upload, $id)
    {

        if (Input::hasFile('files')) {

            foreach (Input::file('files') as $file) {

                $foto = new ProdutosFotos;
                $foto->FotProCodigo = $id;
                $foto->save();

                $codigo = $foto->FotCodigo;

                $this->save_log("C", $foto->getTable(), $foto);

                $upload->resize($file, public_path() . "/upload/produtos/g_" . $codigo . ".jpg", getenv("TamGW"), getenv("TamGH"));
                $upload->resize($file, public_path() . "/upload/produtos/p_" . $codigo . ".jpg", getenv("TamPW"), getenv("TamPH"));

                $this->save_log("UP", $foto->getTable(), $foto);
            }
            return redirect(getenv("PAINEL") . '/produtos/' . $id)->with("success", "Imagens enviadas com sucesso!");
        } else {
            return redirect(getenv("PAINEL") . '/produtos/' . $id)->with("error", "Imagens não enviadas!");
        }
    }

    public function arquivosmultiploupload(Request $request, $id)
    {

        if (Input::hasFile('files')) {

            foreach (Input::file('files') as $file) {

                $arquivo = new ProdutosArquivos;
                $arquivo->ArqProCodigo = $id;
                $arquivo->save();

                $this->save_log("C", $arquivo->getTable(), $arquivo);

                $codigo = $arquivo->ArqCodigo;

                $arquivo2 = ProdutosArquivos::find($codigo);
                $arquivo2->ArqArquivo = $codigo . "." . $file->getClientOriginalExtension();
                $arquivo2->save();

                $this->save_log("U", $arquivo2->getTable(), $arquivo2);

                $file->move("upload/produtosarquivos", $codigo . "." . $file->getClientOriginalExtension());

                $this->save_log("UP", $arquivo2->getTable(), $arquivo2);
            }
            return redirect(getenv("PAINEL") . '/produtos/arquivos/' . $id)->with("success", "Arquivos enviados com sucesso!");
        } else {
            return redirect(getenv("PAINEL") . '/produtos/arquivos/' . $id)->with("error", "Arquivos não enviados!");
        }
    }

}
