<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Upload;
use App\AgendaEventos;
use Input;


class AgendaController extends Controller
{

    public function index()
    {
        if (Input::has("busca")) {
            $itens = AgendaEventos::where(function($query){
                    $query->where('AgeTitulo', 'like', '%' . Input::get("busca") . '%')
                        ->orWhere('AgeConteudo', 'like', '%' . Input::get("busca") . '%');
                })
                ->orderBy('created_at', 'desc')
                ->paginate(500);
        } else {
            $itens = AgendaEventos::orderBy('created_at', 'desc')
                ->paginate(15);
        }
        return view("painel.agendaeventos.index", compact('itens'));
    }

    public function create()
    {
        return view("painel.agendaeventos.create");
    }

    public function create2(Request $request)
    {
        $create = new AgendaEventos();

        $create->AgeDataInicio = implode("-", array_reverse(explode("/", $request->input('datainicio'))));
        $create->AgeDataFim = implode("-", array_reverse(explode("/", $request->input('datafim'))));
        $create->AgeTitulo = $request->input('titulo');
        $create->AgeConteudo = $request->input('conteudo');
        $create->AgeLiberado = $request->input('liberado');

        $create->save();

        $this->save_log("C", $create->getTable(), $create);

        return redirect(getenv("PAINEL")."/agendaeventos")->with('success', 'Registro incluído com sucesso!');
    }

    public function update($id)
    {
        $item = AgendaEventos::where('AgeCodigo', '=', $id)->firstOrFail();
        return view("painel.agendaeventos.update", compact('item'));
    }

    public function update2(Request $request)
    {
        $id = $request->input('id');

        $update = AgendaEventos::find($id);

        $update->AgeDataInicio = implode("-", array_reverse(explode("/", $request->input('datainicio'))));
        $update->AgeDataFim = implode("-", array_reverse(explode("/", $request->input('datafim'))));
        $update->AgeTitulo = $request->input('titulo');
        $update->AgeConteudo = $request->input('conteudo');
        $update->AgeLiberado = $request->input('liberado');

        $update->save();

        $this->save_log("U", $update->getTable(), $update);

        return redirect(getenv("PAINEL")."/agendaeventos")->with('success', 'Registro alterado com sucesso!');
    }

    public function destroy($id)
    {
        AgendaEventos::find($id)->delete();

        $this->save_log("D", "tagendaeventos", $id);

        return redirect(getenv("PAINEL")."/agendaeventos")->with('success', 'Registro excluido com sucesso!');
    }
}
