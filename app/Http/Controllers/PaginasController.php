<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Upload;
use App\Paginas;
use App\PaginasFotos;
use Input;
use File;


class PaginasController extends Controller
{

    public function index()
    {
        if (Input::has("busca")) {
            $itens = Paginas::where(function ($query) {
                $query->where('PagTitulo', 'like', '%' . Input::get("busca") . '%')
                    ->orWhere('PagResumo', 'like', '%' . Input::get("busca") . '%');
            })
                ->orderBy('created_at', 'desc')
                ->paginate(500);
        } else {
            $itens = Paginas::orderBy('created_at', 'desc')->paginate(15);
        }

        return view("painel.paginas.index", compact('itens'));

    }

    public function fotos($id)
    {
        $itens = PaginasFotos::where('FotPagCodigo', '=', $id)
            ->orderBy('created_at', 'desc')
            ->paginate(15);

        return view("painel.paginas.fotos", compact('itens', 'id'));
    }

    public function create()
    {
        return view("painel.paginas.create");
    }

    public function create2(Request $request)
    {
        $create = new Paginas;

        $create->PagTitulo = $request->input('titulo');
        $create->PagResumo = $request->input('resumo');
        $create->PagConteudo= $request->input('conteudo');

        $create->save();

        $this->save_log("C", $create->getTable(), $create);

        return redirect(getenv("PAINEL") . '/paginas')->with('success', 'Registro incluído com sucesso!');
    }

    public function update($id)
    {
        $item = Paginas::where('PagCodigo', '=', $id)
            ->firstOrFail();

        return view("painel.paginas.update", compact('item'));
    }

    public function update2(Request $request)
    {
        $id = $request->input('id');

        $update = Paginas::find($id);

        $update->PagTitulo = $request->input('titulo');
        $update->PagResumo = $request->input('resumo');
        $update->PagConteudo = $request->input('conteudo');

        $update->save();

        $this->save_log("U", $update->getTable(), $update);

        return redirect(getenv("PAINEL") . '/paginas')->with('success', 'Registro alterado com sucesso!');
    }

    public function fotosupdate($id, $idfoto)
    {
        $item = PaginasFotos::where('FotCodigo', '=', $idfoto)
            ->firstOrFail();

        return view("painel.paginas.fotosupdate", compact('item', 'id', 'idfoto'));
    }

    public function fotosupdate2(Request $request)
    {
        $id = $request->input('id');
        $idfoto = $request->input('idfoto');

        $update = PaginasFotos::find($idfoto);
        $update->FotLegenda = $request->input('legenda');
        $update->save();

        $this->save_log("U", $update->getTable(), $update);

        return redirect(getenv("PAINEL") . '/paginas/' . $id)->with('success', 'Registro alterado com sucesso!');
    }


    public function destroy($id)
    {
        $itens = PaginasFotos::where('FotPagCodigo', '=', $id)->get();

        foreach ($itens as $item) {
//            if (File::exists("upload/paginas/p_" . $item->FotCodigo . ".jpg")) {
//                File::delete("upload/paginas/p_" . $item->FotCodigo . ".jpg");
//            }
//            if (File::exists("upload/paginas/g_" . $item->FotCodigo . ".jpg")) {
//                File::delete("upload/paginas/g_" . $item->FotCodigo . ".jpg");
//            }

            PaginasFotos::find($item->FotCodigo)->delete();

            $this->save_log("D", "tpaginasfotos", $item->FotCodigo);
        }

//        if (File::exists("upload/paginas/dest_" . $id . ".jpg")) {
//            File::delete("upload/paginas/dest_" . $id . ".jpg");
//        }

        Paginas::find($id)->delete();

        $this->save_log("D", "tpaginas", $id);

        return redirect(getenv("PAINEL") . '/paginas')->with('success', 'Registro excluido com sucesso!');
    }

    public function fotosdestroy($id, $idfoto)
    {
//        if (File::exists("upload/paginas/p_" . $idfoto . ".jpg")) {
//            File::delete("upload/paginas/p_" . $idfoto . ".jpg");
//        }
//        if (File::exists("upload/paginas/g_" . $idfoto . ".jpg")) {
//            File::delete("upload/paginas/g_" . $idfoto . ".jpg");
//        }
        PaginasFotos::find($idfoto)->delete();

        $this->save_log("D", "tpaginasfotos", $idfoto);

        return redirect(getenv("PAINEL") . '/paginas/' . $id)->with('success', 'Registro excluido com sucesso!');
    }

    public function upload(Request $request, Upload $upload, $id)
    {
        if (Input::hasFile('file')) {

            $upload->resize($request->file("file"), public_path() . "/upload/paginas/dest_" . $id . ".jpg", getenv("TamGW"), getenv("TamGH"));

            $this->save_log("UP", "tpaginas", $id);

            return redirect(getenv("PAINEL") . '/paginas')->with("success", "Imagem enviada com sucesso!");
        } else {
            return redirect(getenv("PAINEL") . '/paginas')->with("error", "Imagem não enviada!");
        }
    }

    public function fotosupload(Request $request, Upload $upload, $id, $idfoto)
    {
        if (Input::hasFile('file')) {

            $upload->resize($request->file("file"), public_path() . "/upload/paginas/g_" . $idfoto . ".jpg", getenv("TamGW"), getenv("TamGH"));
            $upload->resize($request->file("file"), public_path() . "/upload/paginas/p_" . $idfoto . ".jpg", getenv("TamPW"), getenv("TamPH"));

            $this->save_log("UP", "tpaginasfotos", $idfoto);

            return redirect(getenv("PAINEL") . '/paginas/' . $id)->with("success", "Imagem enviada com sucesso!");
        } else {
            return redirect(getenv("PAINEL") . '/paginas/' . $id)->with("error", "Imagem não enviada!");
        }
    }

    public function fotosmultiploupload(Request $request, Upload $upload, $id)
    {

        if (Input::hasFile('files')) {

            foreach (Input::file('files') as $file) {

                $foto = new PaginasFotos;
                $foto->FotPagCodigo = $id;
                $foto->save();

                $codigo = $foto->FotCodigo;

                $this->save_log("C", "tPaginasFotos", $codigo);

                $upload->resize($file, public_path() . "/upload/paginas/g_" . $codigo . ".jpg", getenv("TamGW"), getenv("TamGH"));
                $upload->resize($file, public_path() . "/upload/paginas/p_" . $codigo . ".jpg", getenv("TamPW"), getenv("TamPH"));

                $this->save_log("UP", "tPaginasFotos", $codigo);
            }
            return redirect(getenv("PAINEL") . '/paginas/' . $id)->with("success", "Imagens enviadas com sucesso!");
        } else {
            return redirect(getenv("PAINEL") . '/paginas/' . $id)->with("error", "Imagens não enviadas!");
        }
    }

}
