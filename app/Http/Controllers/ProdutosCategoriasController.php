<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ProdutosDepartamentos;
use Illuminate\Http\Request;
use App\Services\Upload;
use App\ProdutosCategorias;
use Input;


class ProdutosCategoriasController extends Controller
{

    public function __construct()
    {

    }

    public function index()
    {
        if (Input::has("busca")) {
            $itens = ProdutosCategorias::where(function ($query) {
                $query->where('CatTitulo', 'like', '%' . Input::get("busca") . '%');
            })
                ->orderBy('created_at', 'desc')
                ->paginate(500);
        } else {
            $itens = ProdutosCategorias::join('tprodutosdepartamentos', 'DepCodigo', '=', 'CatDepCodigo')
            ->orderBy('tprodutoscategorias.created_at', 'desc')->paginate(15);
        }

        return view("painel.produtoscategorias.index", compact('itens'));
    }

    public function getbydepid($id)
    {
        $itens = ProdutosCategorias::where('CatDepCodigo', '=', $id)
            ->where('CatLiberado', '=', '1')
            ->select('CatCodigo', 'CatTitulo')
            ->orderBy('tprodutoscategorias.created_at', 'desc')->get();

        return $itens;
    }

    public function create()
    {
        $departamentos = ProdutosDepartamentos::orderBy('created_at', 'desc')
            ->get();

        return view("painel.produtoscategorias.create", compact('departamentos'));
    }

    public function create2(Request $request)
    {
        $create = new ProdutosCategorias();

        $create->CatTitulo = $request->input('titulo');
        $create->CatDepCodigo = $request->input('departamento');
        $create->CatLiberado = $request->input('liberado');

        $create->save();

        $this->save_log("C", $create->getTable(), $create);

        return redirect(getenv("PAINEL")."/produtoscategorias")->with('success', 'Registro incluído com sucesso!');
    }

    public function update($id)
    {
        $item = ProdutosCategorias::where('CatCodigo', '=', $id)
            ->firstOrFail();

        $departamentos = ProdutosDepartamentos::orderBy('created_at', 'desc')
            ->get();

        return view("painel.produtoscategorias.update", compact('item', 'departamentos'));
    }

    public function update2(Request $request)
    {
        $id = $request->input('id');

        $update = ProdutosCategorias::find($id);

        $update->CatTitulo = $request->input('titulo');
        $update->CatDepCodigo = $request->input('departamento');
        $update->CatLiberado = $request->input('liberado');

        $update->save();

        $this->save_log("U", $update->getTable(), $update);

        return redirect(getenv("PAINEL")."/produtoscategorias")->with('success', 'Registro alterado com sucesso!');
    }

    public function destroy($id)
    {
        ProdutosCategorias::find($id)->delete();

        $this->save_log("D", "tprodutoscategorias", $id);

        return redirect(getenv("PAINEL")."/produtoscategorias")->with('success', 'Registro excluido com sucesso!');
    }
}
