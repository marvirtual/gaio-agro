<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Upload;
use App\Galerias;
use App\GaleriasFotos;
use Input;
use File;


class GaleriasController extends Controller
{

    public function index()
    {
        if (Input::has("busca")) {
            $itens = Galerias::where(function($query){
                $query->where('GalTitulo', 'like', '%' . Input::get("busca") . '%')
                    ->orWhere('GalResumo', 'like', '%' . Input::get("busca") . '%');
            })
                ->orderBy('created_at', 'desc')
                ->paginate(500);
        } else {
            $itens = Galerias::orderBy('created_at', 'desc')->paginate(15);
        }
        return view("painel.galerias.index", compact('itens'));
    }

    public function fotos($id)
    {
        $itens = GaleriasFotos::where('FotGalCodigo', '=', $id)
            ->orderBy('created_at', 'desc')
            ->paginate(15);

        return view("painel.galerias.fotos", compact('itens', 'id'));

    }

    public function create()
    {
        return view("painel.galerias.create");
    }

    public function create2(Request $request)
    {
        $create = new Galerias;

        $create->GalData = implode("-", array_reverse(explode("/", $request->input('data'))));
        $create->GalTitulo = $request->input('titulo');
        $create->GalResumo = $request->input('resumo');
        $create->GalLiberado = $request->input('liberado');

        $create->save();

        $this->save_log("C", $create->getTable(), $create);

        return redirect(getenv("PAINEL") . '/galerias')->with('success', 'Registro incluído com sucesso!');
    }

    public function update($id)
    {
        $item = Galerias::where('GalCodigo', '=', $id)
            ->firstOrFail();

        return view("painel.galerias.update", compact('item'));
    }

    public function update2(Request $request)
    {
        $id = $request->input('id');

        $update = Galerias::find($id);

        $update->GalData = implode("-", array_reverse(explode("/", $request->input('data'))));
        $update->GalTitulo = $request->input('titulo');
        $update->GalResumo = $request->input('resumo');
        $update->GalLiberado = $request->input('liberado');

        $update->save();

        $this->save_log("U", $update->getTable(), $update);

        return redirect(getenv("PAINEL") . '/galerias')->with('success', 'Registro alterado com sucesso!');
    }

    public function fotosupdate($id, $idfoto)
    {
        $item = GaleriasFotos::where('FotCodigo', '=', $idfoto)
            ->firstOrFail();

        return view("painel.galerias.fotosupdate", compact('item', 'id', 'idfoto'));
    }

    public function fotosupdate2(Request $request)
    {
        $id = $request->input('id');
        $idfoto = $request->input('idfoto');

        $update = GaleriasFotos::find($idfoto);
        $update->FotLegenda = $request->input('legenda');
        $update->save();

        $this->save_log("U", $update->getTable(), $update);

        return redirect(getenv("PAINEL") . '/galerias/' . $id)->with('success', 'Registro alterado com sucesso!');
    }


    public function destroy($id)
    {
        $itens = GaleriasFotos::where('FotGalCodigo', '=', $id)->get();

        foreach ($itens as $item) {
//            if (File::exists("upload/galerias/p_" . $item->FotCodigo . ".jpg")) {
//                File::delete("upload/galerias/p_" . $item->FotCodigo . ".jpg");
//            }
//            if (File::exists("upload/galerias/g_" . $item->FotCodigo . ".jpg")) {
//                File::delete("upload/galerias/g_" . $item->FotCodigo . ".jpg");
//            }

            GaleriasFotos::find($item->FotCodigo)->delete();

            $this->save_log("D", "tgaleriasfotos", $item->FotCodigo);
        }

//        if (File::exists(public_path() . "/upload/galerias/dest_" . $id . ".jpg")) {
//            File::delete(public_path() . "/upload/galerias/dest_" . $id . ".jpg");
//        }

        Galerias::find($id)->delete();

        $this->save_log("D", "tgalerias", $id);

        return redirect(getenv("PAINEL") . '/galerias')->with('success', 'Registro excluido com sucesso!');
    }

    public function fotosdestroy($id, $idfoto)
    {
//        if (File::exists("upload/galerias/p_" . $idfoto . ".jpg")) {
//            File::delete("upload/galerias/p_" . $idfoto . ".jpg");
//        }
//        if (File::exists("upload/galerias/g_" . $idfoto . ".jpg")) {
//            File::delete("upload/galerias/g_" . $idfoto . ".jpg");
//        }

        GaleriasFotos::find($idfoto)->delete();

        $this->save_log("D", "tGaleriasFotos", $idfoto);

        return redirect(getenv("PAINEL") . '/galerias/' . $id)->with('success', 'Registro excluido com sucesso!');
    }

    public function upload(Request $request, Upload $upload, $id)
    {
        if (Input::hasFile('file')) {

            $upload->resize($request->file("file"), public_path() . "/upload/galerias/dest_" . $id . ".jpg", getenv("TamGW"), getenv("TamGH"));

            $this->save_log("UP", "tgalerias", $id);

            return redirect(getenv("PAINEL") . '/galerias')->with("success", "Imagem enviada com sucesso!");
        } else {
            return redirect(getenv("PAINEL") . '/galerias')->with("error", "Imagem não enviada!");
        }
    }

    public function fotosupload(Request $request, Upload $upload, $id, $idfoto)
    {
        if (Input::hasFile('file')) {

            $upload->resize($request->file("file"), public_path() . "/upload/galerias/g_" . $idfoto . ".jpg", getenv("TamGW"), getenv("TamGH"));
            $upload->resize($request->file("file"), public_path() . "/upload/galerias/p_" . $idfoto . ".jpg", getenv("TamPW"), getenv("TamPH"));

            $this->save_log("UP", "tGaleriasFotos", $idfoto);

            return redirect(getenv("PAINEL") . '/galerias/' . $id)->with("success", "Imagem enviada com sucesso!");
        } else {
            return redirect(getenv("PAINEL") . '/galerias/' . $id)->with("error", "Imagem não enviada!");
        }
    }

    public function fotosmultiploupload(Request $request, Upload $upload, $id)
    {

        if (Input::hasFile('files')) {

            foreach (Input::file('files') as $file) {

                $foto = new GaleriasFotos;
                $foto->FotGalCodigo = $id;
                $foto->save();

                $codigo = $foto->FotCodigo;

                $this->save_log("C", "tGaleriasFotos", $codigo);

                $upload->resize($file, public_path() . "/upload/galerias/g_" . $codigo . ".jpg", getenv("TamGW"), getenv("TamGH"));
                $upload->resize($file, public_path() . "/upload/galerias/p_" . $codigo . ".jpg", getenv("TamPW"), getenv("TamPH"));

                $this->save_log("UP", "tGaleriasFotos", $codigo);
            }
            return redirect(getenv("PAINEL") . '/galerias/' . $id)->with("success", "Imagens enviadas com sucesso!");
        } else {
            return redirect(getenv("PAINEL") . '/galerias/' . $id)->with("error", "Imagens não enviadas!");
        }
    }

}
