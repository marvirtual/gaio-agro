<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProdutosFotos extends Model
{
    use SoftDeletes;

    protected $table = 'tprodutosfotos';
    protected $primaryKey = 'FotCodigo';
    protected $dates = ['deleted_at'];

//    protected $fillable = ['FotCodigo', 'FotProCodigo', 'FotLegenda'];
}
