<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Parceiros extends Model {

    use SoftDeletes;

    protected $table = 'tparceiros';
    protected $primaryKey = 'ParCodigo';
    protected $dates = ['deleted_at'];

    //protected $hidden = ['password', 'remember_token'];
//    protected $fillable = ['CatCodigo', 'CatTitulo', 'CatLiberado'];
}
