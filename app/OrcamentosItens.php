<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrcamentosItens extends Model {

    use SoftDeletes;

    protected $table = 'torcamentositens'; 
    protected $primaryKey = 'IteCodigo';
    protected $dates = ['deleted_at'];

//    protected $fillable = ['AgeCodigo', 'AgeDataInicio', 'AgeDataFim', 'AgeTitulo', 'AgeConteudo', 'AgeLiberado'];
    //protected $hidden = ['password', 'remember_token'];

}
