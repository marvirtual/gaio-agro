<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServicosFotos extends Model
{
    use SoftDeletes;

    protected $table = 'tservicosfotos';
    protected $primaryKey = 'FotCodigo';
    protected $dates = ['deleted_at'];

//    protected $fillable = ['FotCodigo', 'FotSerCodigo', 'FotLegenda'];
}
