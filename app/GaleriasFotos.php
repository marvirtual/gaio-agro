<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GaleriasFotos extends Model
{
    use SoftDeletes;

    protected $table = 'tgaleriasfotos';
    protected $primaryKey = 'FotCodigo';
    protected $dates = ['deleted_at'];

//    protected $fillable = ['FotCodigo', 'FotGalCodigo', 'FotLegenda'];
}
