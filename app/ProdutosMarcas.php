<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProdutosMarcas extends Model {

    use SoftDeletes;

    protected $table = 'tprodutosMarcas';
    protected $primaryKey = 'MarCodigo';
    protected $dates = ['deleted_at'];

    //protected $hidden = ['password', 'remember_token'];
//    protected $fillable = ['CatCodigo', 'CatTitulo', 'CatLiberado'];
}
