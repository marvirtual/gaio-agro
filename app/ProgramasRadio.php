<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProgramasRadio extends Model
{
    use SoftDeletes;

    protected $table = 'tprogramasradio';
    protected $primaryKey = 'ProCodigo';
    protected $dates = ['deleted_at'];

//    protected $fillable = ['ProCodigo', 'ProCatCodigo', 'ProTitulo', 'ProConteudo', 'ProLiberado'];
}
