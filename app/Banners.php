<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Banners extends Model {

    use SoftDeletes;

    protected $table = 'tbanners';
    protected $primaryKey = 'BanCodigo';
    protected $dates = ['deleted_at'];

//    protected $fillable = ['BanCodigo', 'BanTitulo', 'BanLink', 'BanTarget', 'BanLiberado'];
    //protected $hidden = ['password', 'remember_token'];

}
