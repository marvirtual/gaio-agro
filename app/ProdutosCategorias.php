<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProdutosCategorias extends Model {

    use SoftDeletes;

    protected $table = 'tprodutoscategorias';
    protected $primaryKey = 'CatCodigo';
    protected $dates = ['deleted_at'];

    //protected $hidden = ['password', 'remember_token'];
//    protected $fillable = ['CatCodigo', 'CatTitulo', 'CatLiberado'];
}
