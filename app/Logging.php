<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Logging extends Model
{

    protected $table = 'tlogs';
    //protected $fillable = ['Log', 'GalData', 'GalTitulo', 'GalResumo', 'GalLiberado'];
    protected $primaryKey = 'LogCodigo';

}
