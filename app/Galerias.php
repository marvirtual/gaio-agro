<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Galerias extends Model
{
    use SoftDeletes;

    protected $table = 'tgalerias';
    protected $primaryKey = 'GalCodigo';
    protected $dates = ['deleted_at'];

//    protected $fillable = ['GalCodigo', 'GalData', 'GalTitulo', 'GalResumo', 'GalLiberado'];
}
