<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProdutosArquivos extends Model
{
    use SoftDeletes;

    protected $table = 'tprodutosarquivos';
    protected $primaryKey = 'ArqCodigo';
    protected $dates = ['deleted_at'];

//    protected $fillable = ['ArqCodigo', 'ArqProCodigo', 'ArqTitulo'];
    //protected $hidden = ['password', 'remember_token'];

}
