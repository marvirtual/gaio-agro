<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Orcamentos extends Model {

    use SoftDeletes;

    protected $table = 'torcamentos';
    protected $primaryKey = 'OrcCodigo';
    protected $dates = ['deleted_at'];

    public function anexo(){
        return $this->hasOne('\App\OrcamentosArquivos', 'ArqOrcCodigo', 'OrcCodigo');
    }

//    protected $fillable = ['AgeCodigo', 'AgeDataInicio', 'AgeDataFim', 'AgeTitulo', 'AgeConteudo', 'AgeLiberado'];
    //protected $hidden = ['password', 'remember_token'];

}
