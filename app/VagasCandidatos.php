<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VagasCandidatos extends Model
{
    use SoftDeletes;

    protected $table = 'tvagascandidatos';
    protected $primaryKey = 'id';
    protected $dates = ['deleted_at'];

    public function vaga(){
        return $this->hasOne("\App\Vagas", "id", "vaga_id");
    }
}
