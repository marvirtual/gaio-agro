<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CotacoesAgricolas extends Model {

    use SoftDeletes;

    protected $table = 'tcotacoes';
    protected $primaryKey = 'CotCodigo';
    protected $dates = ['deleted_at'];

    //protected $hidden = ['password', 'remember_token'];
//    protected $fillable = ['CatCodigo', 'CatTitulo', 'CatLiberado'];
}
