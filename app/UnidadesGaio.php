<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnidadesGaio extends Model {

    use SoftDeletes;

    protected $table = 'tunidadesgaio';
    protected $primaryKey = 'UniCodigo';
    protected $dates = ['deleted_at'];

    //protected $hidden = ['password', 'remember_token'];
//    protected $fillable = ['CatCodigo', 'CatTitulo', 'CatLiberado'];
}
