$(window).ready(function(){

    $('.data-formulario').inputmask('99/99/9999');
    $('.hora-formulario').inputmask('99:99');

    $(".btn-upload-modal").on("click", function () {
        $("#uploadModal .modal-body .sec-secondary").hide();
        $("#uploadModal").modal("show");
        $("#uploadModal #uploadModalForm").attr("action", $(this).attr("data-href"));
    });


    $(".btn-upload-multiplo-modal").on("click", function () {
        $("#uploadMultiploModal .modal-body .sec-secondary").hide();
        $("#uploadMultiploModal").modal("show");
        $("#uploadMultiploModal #uploadModalForm").attr("action", $(this).attr("data-href"));
    });

    $("#uploadModal #uploadModalForm").on('submit', function () {
        $("#uploadModal .modal-body .sec-primary").hide();
        $("#uploadModal .modal-body .sec-secondary").show();
    });

    $("#uploadMultiploModal #uploadModalForm").on('submit', function () {
        $("#uploadMultiploModal .modal-body .sec-primary").hide();
        $("#uploadMultiploModal .modal-body .sec-secondary").show();
    });
    $(".btn-destroy").click(function(e){
       if(!confirm("Deseja excluir esse Registro")){
           e.preventDefault();
           return false;
       }
    });
});